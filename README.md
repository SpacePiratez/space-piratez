**Space Piratez** is a 2D side-scrolling shooter made by a small team of 6.

**Message to all other members** Please fill in the todo list as things come up

	Libraries used: 
		LibGDX 0.9.9
		Physics body Editor
		Universal Tween Engine
		Kryonet
		
**Versions & Progress**

Version 0.0-1
![Version0.0-1](https://bitbucket.org/SpacePiratez/space-piratez/raw/fe091ce4c57fcf2fc0ecfe687797ebe5fb9fdce4/images/Build%201.png)

Version 0.0-2
![Version0.0-2](https://www.dropbox.com/s/aas7yy6gzkpv7xk/build%202.mp4)

Version 0.1-3
![Version0.1-3](https://bitbucket.org/SpacePiratez/space-piratez/raw/fe091ce4c57fcf2fc0ecfe687797ebe5fb9fdce4/images/Build%203.jpg)

Version 0.1-4
	Forgotten image, sorry.

Version 0.1-5
![Version0.1-5_A](https://bitbucket.org/SpacePiratez/space-piratez/raw/fe091ce4c57fcf2fc0ecfe687797ebe5fb9fdce4/images/Build%205_A.png)
![Version0.1-5_B](https://bitbucket.org/SpacePiratez/space-piratez/raw/fe091ce4c57fcf2fc0ecfe687797ebe5fb9fdce4/images/Build%205_B.jpg)

Version 0.2-9
![Version0.2-9_A](https://dl-web.dropbox.com/get/PDW/Visual%20progress/Build_9.png?w=AADLMeWtxYjUNxu94N4BFQr2oAV5H062uSOX7ohlESGVYQ)
![Version0.2-9_B](https://dl-web.dropbox.com/get/PDW/Visual%20progress/Build_9_B.png?w=AAAdMymc194PIZKD-vOSW4UVeR5ZXpj_gmpN9oyeDYj_Wg)

Version 0.2-10
![Version0.2-10](https://bitbucket.org/SpacePiratez/space-piratez/raw/7f6b67ad692270fa72d4b134e126a506fc149501/images/Build_10.jpg)