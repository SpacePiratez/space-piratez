package net.spacepiratez.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

/**
 * Created by Guy on 09/03/14.
 */
public class TestStuff implements Screen {
    final SpacePiratez game;
    private SpriteBatch batch;
    private PerspectiveCamera cam;
    private Model model, caps;
    private ModelInstance modelInst, capsInst;
    private ModelBatch modBatch;
    private Environment env, env2;
    private CameraInputController camCont;
    private ModelBuilder modBuild;
    private ModelLoader modLoad;
    private float camPosX, camPosY, camPosZ;
    private int camSelect;
    private boolean camXUp = true, camYUp = true, camZUp = true;

    public TestStuff(final SpacePiratez gam) {
        this.game = gam;
        modBatch = new ModelBatch();
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(10f, 10f, 10f);
        camPosZ = 10f; camPosY = 10f; camPosZ = 10f;
        cam.lookAt(0, 0, 0);
        cam.near = 0.1f;
        cam.far = 300f;
        cam.update();

        env  = new Environment();
        env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        env.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        env2 = new Environment();
        env2.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        env2.add(new DirectionalLight().set(0.1f, 0.4f, 0.9f, -1, 0.8f, 0.2f));

        modBuild = new ModelBuilder();
        modLoad  = new ObjLoader();

        model = modBuild.createBox(5f, 5f, 5f,
                new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                Usage.Position | Usage.Normal);

        caps =	modLoad.loadModel(Gdx.files.internal("data/models/gal.obj"));

        modelInst = new ModelInstance(model);
        capsInst = new ModelInstance(caps);
        camCont = new CameraInputController(cam);
        Gdx.input.setInputProcessor(camCont);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);


        capsInst.transform.rotate(1, 1, 0, 0.1f);
        modBatch.begin(cam);
//			modBatch.render(modelInst, env);
        modBatch.render(capsInst, env2);
        modBatch.end();

        camCont.update();

        game.batch.begin();
        game.font.draw(game.batch, "Cam X: "+cam.position.x, 100, 100);
        game.font.draw(game.batch, "Cam Y: "+cam.position.y, 100, 80);
        game.font.draw(game.batch, "Cam Z: "+cam.position.z, 100, 60);
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        modBatch.dispose();
        model.dispose();
    }

}

