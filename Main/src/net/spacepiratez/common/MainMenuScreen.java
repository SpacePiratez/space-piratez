package net.spacepiratez.common;

/**
 * Created by Guy on 09/03/14.
 */
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import net.spacepiratez.common.game.GameScreenLvlOne;
import net.spacepiratez.common.game.GameScreenSurvival;
import net.spacepiratez.common.utilityScreen.ConfirmExitScreen;
import net.spacepiratez.utils.*;

import java.util.Scanner;

//import net.untitled.network.SPClient;
//import net.untitled.network.SPPacketManager.Packet2Message;
//import net.untitled.network.SPServer;
//import com.esotericsoftware.kryonet.Connection;

public class MainMenuScreen implements Screen {

    final SpacePiratez game;
    Skin skin;
    private Stage stage;
    private MenuNinePatch nineBlue, nineRed, nineGreen, nineCyan;
    private MenuNinePatch indiRed, indiYellow, indiGreen;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private BitmapFont font;
    //	public static Texture titleTex, titleImage, playImage, playerText, survivalText, helpText, diffText, customText;
    public  static  boolean isSurvival = false;
    public  static  boolean isMultiPlayer = false;
    public 	String  isMultiPlayerStr;
    public 	String  isSurvivalStr;
    public  static int difficulty;

    public  static boolean isConnected = false;
    public  String isConnectedTrans = "Error", connectedAddress = "127.0.0.1";
    public  InputListener listener = new InputListener();
    public  static String ipAddress;
    //	private Texture baseLaser;
//	private TextureRegion baseLaserReg;
//	private Sprite baseLaserSprite;
    private String multTrans = "", survTrans = "", diffTrans = "";
    private float multBoxX=75, survBoxX = 75, diffBoxX=75;
    private boolean helpScreenSwitch = false, playSwitch = false, customSwitch = false, exitSwitch = false;
    private int mousePosX = Gdx.input.getX(), mousePosY = Gdx.graphics.getHeight()-Gdx.input.getY();
    private int deltaCap = 10;
    private boolean menuOn = true, isModelSelected = true;
    private float alphaBatch = 1f, modelAlpha = 1f;
    private boolean isAlphaToggle = true, modelAlphaToggle = true;
    private int alphaToggleDelta = 0, modelAlphaDelta = 0;

    private PerspectiveCamera cam;
    private Model caps;
    private ModelInstance capsInst;
    private ModelBatch modBatch;
    private Environment env2;
    private CameraInputController camCont;
    private ModelLoader modLoad;

    private float bossRot = 0;
    private float bossX = 1000;
    private boolean bossRotClock = true, bossDirLeft = true;

    public boolean deltaCapToggle = true;
    private Scanner scan = new Scanner(System.in);
    private String scannedText = "";
    private int transScannedText = 10;

    public static int ControlsP1[] = new int[6];
    public static int ControlsP2[] = new int[6];
    public static int moveUpP1, moveDownP1, moveLeftP1, moveRightP1, actionShotP1, actionLazorP1;
    public static int moveUpP2, moveDownP2, moveLeftP2, moveRightP2, actionShotP2, actionLazorP2;
    public String moveUpP1Save = "1", moveDownP1Save = "1", moveLeftP1Save = "1", moveRightP1Save = "1", actionShotP1Save = "1", actionLazorP1Save = "1";
    public String moveUpP2Save = "1", moveDownP2Save = "1", moveLeftP2Save = "1", moveRightP2Save = "1", actionShotP2Save = "1", actionLazorP2Save = "1";

    private boolean isMultiPlayerToggle = true, isSurvivalToggle = true, diffToggle = true, serverToggle = true, windowToggle = true;
    private int multiDelta, survivalDelta, diffDelta, serverDelta, windowDelta, deltaCapDelta;

//	public static Music bgAudio = Gdx.audio.newMusic(Gdx.files.internal(Reference.AUDIO_MENU_BG_AUDIO));

    public MainMenuScreen(final SpacePiratez gam) {
        game = gam;

        XMLHandler.UpdateLeaderboard();

        ControlsP1[0] = moveUpP1;
        ControlsP1[1] = moveDownP1;
        ControlsP1[2] = moveLeftP1;
        ControlsP1[3] = moveRightP1;
        ControlsP1[4] = actionShotP1;
        ControlsP1[5] = actionLazorP1;

        ControlsP2[0] = moveUpP2;
        ControlsP2[1] = moveDownP2;
        ControlsP2[2] = moveLeftP2;
        ControlsP2[3] = moveRightP2;
        ControlsP2[4] = actionShotP2;
        ControlsP2[5] = actionLazorP2;

        XMLHandler.readXMLCont();
        //P1 Controls
        moveUpP1Save 		= XMLHandler.moveUpP1();
        moveDownP1Save		= XMLHandler.moveDownP1();
        moveLeftP1Save		= XMLHandler.moveLeftP1();
        moveRightP1Save		= XMLHandler.moveRightP1();
        actionShotP1Save	= XMLHandler.actionShotP1();
        actionLazorP1Save	= XMLHandler.actionLazorP1();

        moveUpP1			= Integer.valueOf(moveUpP1Save);
        moveDownP1			= Integer.valueOf(moveDownP1Save);
        moveLeftP1			= Integer.valueOf(moveLeftP1Save);
        moveRightP1			= Integer.valueOf(moveRightP1Save);
        actionShotP1		= Integer.valueOf(actionShotP1Save);
        actionLazorP1		= Integer.valueOf(actionLazorP1Save);

        //P2 Controls
        moveUpP2Save 		= XMLHandler.moveUpP2();
        moveDownP2Save		= XMLHandler.moveDownP2();
        moveLeftP2Save		= XMLHandler.moveLeftP2();
        moveRightP2Save		= XMLHandler.moveRightP2();
        actionShotP2Save	= XMLHandler.actionShotP2();
        actionLazorP2Save	= XMLHandler.actionLazorP2();

        moveUpP2			= Integer.valueOf(moveUpP2Save);
        moveDownP2			= Integer.valueOf(moveDownP2Save);
        moveLeftP2			= Integer.valueOf(moveLeftP2Save);
        moveRightP2			= Integer.valueOf(moveRightP2Save);
        actionShotP2		= Integer.valueOf(actionShotP2Save);
        actionLazorP2		= Integer.valueOf(actionLazorP2Save);

        difficulty = 1;

        nineBlue   = MenuNinePatch.getInstanceBlue();
        nineRed    = MenuNinePatch.getInstanceRed();
        nineGreen  = MenuNinePatch.getInstanceGreen();
        nineCyan   = MenuNinePatch.getInstanceCyan();

        indiRed    = MenuNinePatch.getIndicatorRed();
        indiYellow = MenuNinePatch.getIndicatorYellow();
        indiGreen  = MenuNinePatch.getIndicatorGreen();

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();
		camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

        modBatch = new ModelBatch();
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(10f, 10f, 10f);
        cam.lookAt(0, 0, 0);
        cam.near = 0.1f;
        cam.far = 300f;
        cam.update();

        env2 = new Environment();
        env2.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        env2.add(new DirectionalLight().set(0.1f, 0.4f, 0.9f, -1, 0.8f, 0.2f));

        modLoad  = new ObjLoader();
        caps =	modLoad.loadModel(Gdx.files.internal(Reference.Files.MODEL_GALAXY));

        capsInst = new ModelInstance(caps);
        capsInst.transform.scale(3, 3, 3);
//		camCont = new CameraInputController(cam);
//        Gdx.input.setInputProcessor(camCont);

//        Preloader.audioMenuBg.setLooping(true);
//        if(!Preloader.audioMenuBg.isPlaying()) Preloader.audioMenuBg.play();
        Preloader.bgAudioIndex = MathUtils.random(0, 1);
        Preloader.audioMenuBg[Preloader.bgAudioIndex].setLooping(true);
        if(Preloader.audioMenuBg[2].isPlaying()) Preloader.audioMenuBg[2].stop();
        if(Preloader.audioMenuBg[3].isPlaying()) Preloader.audioMenuBg[3].stop();
        if(!Preloader.audioMenuBg[0].isPlaying() && !Preloader.audioMenuBg[1].isPlaying()) Preloader.audioMenuBg[Preloader.bgAudioIndex].play();

//		titleTex = new Texture(Gdx.files.internal(Reference.MAIN_TITLE_TEX));
//		playImage = new Texture(Gdx.files.internal(Reference.MAIN_PLAY_IMAGE));
//		titleImage = new Texture(Gdx.files.internal(Reference.MAIN_TITLE_IMAGE));
//		playerText = new Texture(Gdx.files.internal(Reference.MAIN_PLAYER_TEXT));
//		survivalText = new Texture(Gdx.files.internal(Reference.MAIN_SURVIVAL_TEXT));
//		helpText = new Texture(Gdx.files.internal(Reference.MAIN_HELP_TEXT));
//		diffText = new Texture(Gdx.files.internal(Reference.MAIN_DIFF_TEXT));
//		customText = new Texture(Gdx.files.internal(Reference.MAIN_CUSTOM_TEXT));
//
//		titleTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		playImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		titleImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		playerText.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		survivalText.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		helpText.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		diffText.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		customText.setFilter(TextureFilter.Linear, TextureFilter.Linear);

    }

    public void create() {
        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        float time = Gdx.graphics.getFramesPerSecond();
        String fps = "FPS: " + time;

//		deltaCap = InputListener.inputNum;

//		batch.setProjectionMatrix(camera.combined);
        batch.begin();
//        batch.draw(Preloader.mainTitleImage, 0, 0);
        Preloader.mainTitleSprite[0].setPosition(0, 0);
        Preloader.mainTitleSprite[0].draw(batch, alphaBatch);
        batch.end();

        if(modelAlpha < 1) {
            capsInst.transform.rotate(1, 1, 0, 0.1f);
            modBatch.begin(cam);
            modBatch.render(capsInst, env2);
            modBatch.end();
            cam.update();
        }

        switch(String.valueOf(menuOn)) {
            case "true":  if(alphaBatch < 1) alphaBatch=alphaBatch+0.02f; if(alphaBatch > 1) alphaBatch = 1; break;
            case "false": if(alphaBatch > 0) alphaBatch=alphaBatch-0.02f; if(alphaBatch < 0) alphaBatch = 0; break;
        }

        switch(String.valueOf(isModelSelected)) {
            case "true":  if(modelAlpha < 1) modelAlpha=modelAlpha+0.02f; if(modelAlpha > 1) modelAlpha = 1; break;
            case "false": if(modelAlpha > 0) modelAlpha=modelAlpha-0.02f; if(modelAlpha < 0) modelAlpha = 0; break;
        }

        batch.begin();
        batch.setColor(1, 1, 1, 1);
        Preloader.mainTitleSprite[0].setPosition(0, 0);
        Preloader.mainTitleSprite[0].draw(batch, modelAlpha);
//		nineBlue.draw(batch, 320, 224, 384, 100); //Main options and selections
//		nineBlue.draw(batch, 384, -10, 256, 50); //Bottom frame w/ exit and FPS counter
//		nineBlue.draw(batch, 512-133, 65, 266, 138); //Play button border
        nineRed.draw(batch, 15, 225, 150, 125); //Options panel
        nineRed.draw(batch, 1024-(300+15), 95, 300, 255); //Chat room

        //switch(String.valueOf(isMultiPlayer)) {
        switch(isMultiPlayer+"") {
            case "true":  indiGreen.draw(batch, 125, 286, 25, 25); multTrans = "Multiplayer";  break;
            case "false": indiRed.draw(batch, 125, 286, 25, 25);   multTrans = "Singleplayer"; break;
        }

        switch(String.valueOf(isSurvival)) {
            case "true":  indiGreen.draw(batch, 125, 261, 25, 25); survTrans = "Survival"; break;
            case "false": indiRed.draw(batch, 125, 261, 25, 25);   survTrans = "Missions"; break;
        }

        switch(difficulty) {
            case 1: indiGreen.draw(batch, 125, 236, 25, 25);  diffTrans = "Easy";   break; //Easy
            case 2: indiYellow.draw(batch, 125, 236, 25, 25); diffTrans = "Normal"; break; //Normal
            case 3: indiRed.draw(batch, 125, 236, 25, 25);    diffTrans = "Hard";   break; //Hard
        }

//		batch.draw(playImage, 384, 70);
//        batch.draw(Preloader.mainTitleTex, 0, 0); //Title
        Preloader.mainTitleSprite[2].setPosition(0, 0);
        Preloader.mainTitleSprite[2].draw(batch);
        multBoxX = (game.font.getBounds(multTrans).width)+20;
        survBoxX = (game.font.getBounds(survTrans).width)+20;
        diffBoxX = (game.font.getBounds(diffTrans).width)+20;
        Shortcuts.Tooltip(batch, nineCyan, multBoxX, 30, 125, 150, 286, 311); //Multiplayer Tooltip
        Shortcuts.Tooltip(batch, nineCyan, survBoxX, 30, 125, 150, 261, 286); //Survival Tooltip
        Shortcuts.Tooltip(batch, nineCyan, diffBoxX, 30, 125, 150, 236, 261); //Difficulty Tooltip

        // offset from center: 6
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, alphaBatch, 0, 0.8f, 0.8f, alphaBatch, 323, 335, 183, 60); //Players Button
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, alphaBatch, 0, 0.8f, 0.8f, alphaBatch, 518, 335, 183, 60); //Survival Button
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, alphaBatch, 0, 0.8f, 0.8f, alphaBatch, 323, 270, 183, 60); //Help Button
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, alphaBatch, 0, 0.8f, 0.8f, alphaBatch, 518, 270, 183, 60); //Difficulty Button
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, alphaBatch, 0, 0.8f, 0.8f, alphaBatch, 323, 205, 378, 60); //Customise Button
        Shortcuts.Button(batch, 0, 0.5f, 0, alphaBatch, 0, 0.8f, 0, alphaBatch, 379, 56, 266, 138); 	  //Play button
        Shortcuts.Button(batch, 0.5f, 0, 0, alphaBatch, 0.8f, 0, 0, alphaBatch, 384, -10, 256, 50);		  //Exit button

        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[1], 0, 1, 1, alphaBatch, 384, 70, true); //Play Text Image
        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[3], 0.5f, 0, 0.8f, alphaBatch, 323, 335, true); //Players Text Image
        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[4], 0.5f, 0, 0.8f, alphaBatch, 518, 335, true); //Survival Text Image
        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[5], 0.5f, 0, 0.8f, alphaBatch, 323, 270, true); //Help Text Image
        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[6], 0.5f, 0, 0.8f, alphaBatch, 518, 270, true); //Difficulty Text Image
        Shortcuts.ColourSpriteF(batch, Preloader.mainTitleSprite[7], 0.5f, 0, 0.8f, alphaBatch, 323, 205, true); //Customise ship Text Image
//		Shortcuts.Button(batch, 0, 1, 1, 1, 0, 1, 100, 1        00, 150, 150); //Test

        batch.setColor(1, 1, 1, 1);
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 850, 20, 150, 30);
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 20, 20, 150, 30);
        batch.setColor(1, 1, 1, alphaBatch);

        Preloader.shapeSprite[0].setSize(6, 50);
        Preloader.shapeSprite[0].setOrigin(Preloader.shapeSprite[0].getWidth()/2, 0);
        Preloader.shapeSprite[0].rotate(-1);
        Shortcuts.SpriteColour(batch, Preloader.shapeSprite[0], 100, 100, 0, 0.8f, 0.8f, alphaBatch);
        Preloader.shapeSprite[1].setSize(32, 128);
        Preloader.shapeSprite[1].setOrigin(Preloader.shapeSprite[1].getWidth()/2, Preloader.shapeSprite[1].getHeight()/2);
        Preloader.shapeSprite[1].rotate(5);
        Shortcuts.SpriteColour(batch, Preloader.shapeSprite[1], 100, 100, 0.8f, 0, 0.2f, alphaBatch);
        Preloader.shapeSprite[2].setSize(128, 32);
        Preloader.shapeSprite[2].setOrigin(Preloader.shapeSprite[2].getWidth()/2, Preloader.shapeSprite[2].getHeight()/2);
        Preloader.shapeSprite[2].rotate(-5);
        Shortcuts.SpriteColour(batch, Preloader.shapeSprite[2], 100, 100, 0.2f, 0, 0.8f, alphaBatch);

        batch.setColor(1, 1, 1, 1);
        batch.end();

        isMultiPlayerStr = "" + isMultiPlayer;
        isSurvivalStr = "" + isSurvival;

//        isConnected = SPClient.isConnected();
//		connectedAddress = InputListener.getIp();

        game.batch.begin();
        game.batch.setColor(1, 1, 1, alphaBatch);

        //Tooltips
        if(menuOn) {
            Shortcuts.TooltipText(game.batch, game.font, Color.ORANGE, 30, 125, 150, 286, 311, multTrans); //Multiplayer text
            Shortcuts.TooltipText(game.batch, game.font, Color.ORANGE, 30, 125, 150, 261, 286, survTrans); //Survival text
            Shortcuts.TooltipText(game.batch, game.font, Color.ORANGE, 30, 125, 150, 236, 261, diffTrans); //Difficulty text
        }

//		title(Color.ORANGE, 400, 200, "______    __         __    __      __");
//		title(Color.ORANGE, 400, 185, "|          \\   |    |        /   \\   \\    \\    /   /");
//		title(Color.ORANGE, 400, 170, "|    D    |  |    |       /     \\   \\    \\  /   /");
//		title(Color.ORANGE, 400, 155, "|          /");

        //Right Pane

        //Options
        titlePlus(1, 0.78f, 0, alphaBatch, 55, 335, "OPTIONS");
        titlePlus(1, 0.78f, 0, alphaBatch, 55, 335, "________");
        titlePlus(1, 0.78f, 0, alphaBatch, 25, 305, "MULTIPLAYER");
        titlePlus(1, 0.78f, 0, alphaBatch, 53, 280, "SURVIVAL");
        titlePlus(1, 0.78f, 0, alphaBatch, 40, 255, "DIFFICULTY");
        titlePlus(1, 0.78f, 0, alphaBatch, 40, 450, "USER: "+XMLHandler.User);

        //Leaderboards
        titlePlus(1, 0.8f, 0.25f, alphaBatch, 800, 335, "LEADERBOARDS");
        for(int line = 0; line < 10; line++) {
            titlePlus(1, 0.8f, 0.25f, alphaBatch, 715, 320-(20*line), XMLHandler.Names[line]);
            titlePlus(1, 0.8f, 0.25f, alphaBatch, 800, 320-(20*line), XMLHandler.Score[line]);
            titlePlus(1, 0.8f, 0.25f, alphaBatch, 885, 320-(20*line), XMLHandler.Times[line]);
        }

        titlePlus(0, 1, 1, alphaBatch, 425, 22, "EXIT");
        titlePlus(1, 0.68f, 0.68f, alphaBatch, 525, 22, fps);
        game.batch.setColor(1, 1, 1, 1);
        game.batch.end();

        if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapModKey1)) {
            if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapModKey2)) {
                if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapAlter)) {
                    System.out.println("");
                    System.out.println("");
                    System.out.println("Default delay: 10 | Current Delay: " + deltaCap);
                    System.out.print("Please enter your desired button delay: ");
                    scannedText = scan.nextLine();
                    transScannedText = Integer.parseInt(scannedText);
                    if(transScannedText > 100) transScannedText = 100;
                    if(transScannedText < 1)   transScannedText = 1;
                    deltaCap = transScannedText;
                }
            }
        }

        if(Gdx.input.getX() >= 850 && Gdx.input.getX() <= 1000) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 20 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 50) {
                if(Gdx.input.isTouched()) {
                    if(isAlphaToggle) {
                        switch(String.valueOf(menuOn)) {
                            case "true":  menuOn = false; break;
                            case "false": menuOn = true;  break;
                        }
                        isAlphaToggle = false;
                    }
                }
            }
        }

        if(Gdx.input.getX() >= 20 && Gdx.input.getX() <= 170) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 20 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 50) {
                if(Gdx.input.isTouched()) {
                    if(modelAlphaToggle) {
                        switch(String.valueOf(isModelSelected)) {
                            case "true":  isModelSelected = false; break;
                            case "false": isModelSelected = true;  break;
                        }
                        modelAlphaToggle = false;
                    }
                }
            }

        }

        if(menuOn) {
            //Play
            if(Gdx.input.getX() >= 379 && Gdx.input.getX() <= 645) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 56 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 194) {
                    if(Gdx.input.isTouched()) {
                        if(!playSwitch) playSwitch = true;
                    }
                }
            }
            //Players
            if(Gdx.input.getX() >= 323 && Gdx.input.getX() <= 506) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 335 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 518) {
                    if(Gdx.input.isTouched()) {
                        if(isMultiPlayerToggle) {
                            switch(String.valueOf(isMultiPlayer)) {
                                case "true":  isMultiPlayer = false; break;
                                case "false": isMultiPlayer = true;  break;
                            }
                            isMultiPlayerToggle = false;
                        }
                    }
                }
            }
            //Survival
            if(Gdx.input.getX() >= 518 && Gdx.input.getX() <= 701) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 335 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 518) {
                    if(Gdx.input.isTouched()) {
                        if(isSurvivalToggle) {
                            switch(String.valueOf(isSurvival)) {
                                case "true":  isSurvival = false; break;
                                case "false": isSurvival = true;  break;
                            }
                            isSurvivalToggle = false;
                        }
                    }
                }
            }

            //Difficulty
            if(Gdx.input.getX() >= 518 && Gdx.input.getX() <= 701) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 270 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 330) {
                    if(Gdx.input.isTouched()) {
                        if(diffToggle) {
                            switch(difficulty) {
                                case 1:  difficulty = 2; break;
                                case 2:  difficulty = 3; break;
                                case 3:  difficulty = 1; break;
                                default: difficulty = 1; break;
                            }
                            diffToggle = false;
                        }
                    }
                }
            }
            //Help
            if(Gdx.input.getX() >= 323 && Gdx.input.getX() <= 506) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 270 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 330) {
                    if(Gdx.input.isTouched()) {
                        if(!helpScreenSwitch) helpScreenSwitch = true;
                    }
                }
            }
            //Customize
            if(Gdx.input.getX() >= 323 && Gdx.input.getX() <= 701) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 205 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 265) {
                    if(Gdx.input.isTouched()) {
                        if(!customSwitch) customSwitch = true;
                    }
                }
            }
            //Exit
            if(Gdx.input.getX() >= 384 && Gdx.input.getX() <= 640) {
                if(Gdx.graphics.getHeight()-Gdx.input.getY() >= -10 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 40) {
                    if(Gdx.input.isTouched()) {
                        if(!exitSwitch) exitSwitch = true;
                    }
                }
            }
        }
        if(helpScreenSwitch) { game.setScreen(new HelpScreen(game)); }
        if(playSwitch) {
            if(Preloader.audioMenuBg[0].isPlaying()) Preloader.audioMenuBg[0].stop();
            if(Preloader.audioMenuBg[1].isPlaying()) Preloader.audioMenuBg[1].stop();
            switch(String.valueOf(isSurvival)) {
                case "true":  game.setScreen(new GameScreenSurvival(game)); break;
                case "false": game.setScreen(new GameScreenLvlOne(game)); break;
            }
        }
        if(customSwitch)	 { game.setScreen(new CustomScreen(game)); }
//        if(customSwitch)     { game.setScreen(new PhysicGame(game)); }
        if(exitSwitch)		 { game.setScreen(new ConfirmExitScreen(game)); }

//		GameModeHandle();
//		DifficultyHandle();
//        PlayHandle();
        ToggleHandle();
        UtilHandle();

		/*
		 *
		public void DifficultyHandle() {
		if(diffToggle) {
			difficulty = difficulty + 1;
			diffToggle = false;
		}
		if(difficulty > 3) difficulty = 1;
	}
		 *
		 */

        if(Gdx.input.isKeyPressed(Keys.H)) {
            System.out.println("Test Environment");
            game.setScreen(new TestStuff(game));
        }
//		if(Gdx.input.isKeyPressed(Keys.C)) game.setScreen(new CustomScreen(game));
//		if(Gdx.input.isKeyPressed(Keys.NUM_0) || Gdx.input.isKeyPressed(Keys.NUMPAD_0)) game.setScreen(new ConfirmExitScreen(game));

    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }

    //Shortcuts and Handlers


//     @param Type:Colour  | Var:par1   | desc:Declares the colour of the text
//     @param Type:float   | Var:xCoord | desc:Declares the X-Coordinates of the text
//     @param Type:float   | Var:yCoord | desc:Declares the Y-Coordinates of the text
//     @param Type:String  | Var:par4	| desc:Declares the string to be printed

    public void title(Color par1, float xCoord, float yCoord, String par4) {
        game.font.setColor(par1);
        game.font.draw(game.batch, par4, xCoord, yCoord);
        game.font.setColor(Color.WHITE);
    }

    public void GameModeHandle() {
        if(Gdx.input.isKeyPressed(Keys.P)) {
            if(isMultiPlayerToggle) {
                switch(isMultiPlayerStr) {
                    case "true":
                        isMultiPlayer = false;
                        break;
                    case "false":
                        isMultiPlayer = true;
                        break;
                }
                isMultiPlayerToggle = false;
            }
        }

        if(Gdx.input.isKeyPressed(Keys.S)) {
            if(isSurvivalToggle) {
                switch(isSurvivalStr) {
                    case "true":
                        isSurvival = false;
                        break;
                    case "false":
                        isSurvival = true;
                        break;
                }
                isSurvivalToggle = false;
            }
        }
        if(Gdx.input.isKeyPressed(Keys.HOME)) {
            if(serverToggle) {
                switch(String.valueOf(isConnected)) {
                    case "false":
//                        SPServer.ActivateServer();
                        ipAddress = "127.0.0.1";
//                        SPClient.RunClient();
                        isConnectedTrans = "ONLINE: " + connectedAddress;
                        break;
                    case "true":
//                        SPServer.DeactivateServer();
                        isConnectedTrans = "OFFLINE";
                        break;
                }
                serverToggle = false;
            }
        }
    }

    public void DifficultyHandle() {
        if(diffToggle) {
            difficulty = difficulty + 1;
            diffToggle = false;
        }
        if(difficulty > 3) difficulty = 1;
    }

    public void PlayHandle() {
        if(Gdx.input.isKeyPressed(Keys.ENTER)) {
            Preloader.audioMenuBg[Preloader.bgAudioIndex].stop();
            game.setScreen(new GameScreenSurvival(game));
        }
    }

    public void ToggleHandle() {
        if(!isMultiPlayerToggle) {
            if(multiDelta > deltaCap) {
                isMultiPlayerToggle = true;
                multiDelta = 0;
            }
            multiDelta++;
        }
        if(!isSurvivalToggle) {
            if(survivalDelta > deltaCap) {
                isSurvivalToggle = true;
                survivalDelta = 0;
            }
            survivalDelta++;
        }
        if(!diffToggle) {
            if(diffDelta > deltaCap) {
                diffToggle = true;
                diffDelta = 0;
            }
            diffDelta++;
        }
        if(!deltaCapToggle) {
            if(deltaCapDelta > 30) {
                deltaCapToggle = true;
                deltaCapDelta = 0;
            }
            deltaCapDelta++;
        }
        if(!isAlphaToggle) {
            if(alphaToggleDelta > deltaCap) {
                isAlphaToggle = true;
                alphaToggleDelta = 0;
            }
            alphaToggleDelta++;
        }
        if(!modelAlphaToggle) {
            if(modelAlphaDelta > deltaCap) {
                modelAlphaToggle = true;
                modelAlphaDelta = 0;
            }
            modelAlphaDelta++;
        }
//		if(!serverToggle) {
//			if(serverDelta > 30) {
//				serverToggle = true;
//				serverDelta = 0;
//			}
//			serverDelta++;
//		}
//		if(!windowToggle) {
//			if(windowDelta > 30) {
//				windowToggle = true;
//				windowDelta = 0;
//			}
//			windowDelta++;
//		}
    }

    public void UtilHandle() {
        if(Gdx.input.isKeyPressed(Keys.T)) {
            if(windowToggle) {
                if(!isConnected) {
                    Gdx.input.getTextInput(listener, "Ip Address of server", "Put IP Here: ");
                }
                if(isConnected) {
                    Gdx.input.getTextInput(listener, "Chat Message", "Put Message Here:");
                }
                windowToggle = false;
            }
        }
    }

    public static boolean getMulti() {
        boolean multiState = isMultiPlayer;
        return multiState;
    }



//    public void received(Connection conn, Object obj) {
//        if(obj instanceof Packet2Message) {
//            Packet2Message messPack = new Packet2Message();
//            for(int i = 1; i < chatMessages.length; i++) {
//                chatMessages[i] = chatMessages[i-1];
//            }
//            chatMessages[10] = messPack + "";
//
//        }
//    }

    public void titlePlus(float r, float g, float b, float a, float xCoord, float yCoord, String par4) {
        game.font.setColor(r, g, b, a);
        game.font.draw(game.batch, par4, xCoord, yCoord);
        game.font.setColor(1, 1, 1, 1);
    }


}
