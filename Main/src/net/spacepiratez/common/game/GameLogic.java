package net.spacepiratez.common.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import net.spacepiratez.common.MainMenuScreen;
import net.spacepiratez.common.SpacePiratez;
import net.spacepiratez.common.utilityScreen.GameOverScreen;
import net.spacepiratez.handlers.LeaderboardHandler;
import net.spacepiratez.handlers.StatHandler;
import net.spacepiratez.utils.*;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by Guy on 16/04/2014.
 */
public class GameLogic {
    public static boolean gameOver = false;
    public static int bgGameAudioIndex;
    public static OrthographicCamera camera;
    public static SpriteBatch batch;
    public static Sprite craft[] = new Sprite[5];
    public static Sprite craft2[] = new Sprite[5];
    public static Rectangle craftShape, craftShape2, laserPlayer, laserPlayer2;
//    public static Rectangle laser, plasma, fastShip;
    public static Rectangle gameRect;
    public static String craftName[] = new String[5];
    public static int partName[] = new int[5], partName2[] = new int[5];
    public static long laserPlayerLastFire, laserPlayer2LastFire, laserLastFire, plasmaLastFire, shipLastFire, fastShipLastFire;
    public static long healthLastFire, speedLastFire, shieldLastFire, lazorLastFire, triLastFire, beamLastFire;
    public static int hitSound;
    public static float shieldHitSound;
    public static int score = 0;
    public static float healthP1 = 10, healthP2 = 10;
    public static float scrollTimer = 0.0f;
    public static int mastNumP1, hullNumP1, engineNumP1, shotNumP1, gunNumP1;
    public static int mastNumP2, hullNumP2, engineNumP2, shotNumP2, gunNumP2;
    public static boolean playerFire = true, playerFire2 = true;
    public static Rectangle ship, ship2, ship3;
    public static Array<Rectangle> arrayGame;
    public static Iterator<Rectangle> iterGame;
    public static MenuNinePatch hudBack, nineRed, nineBlue;
    public static float runTime = 0;
    public static int runTimeSec = 0, runTimeMin = 0, runTimeHour = 0;
    public static String mastNumP1Save = "1", hullNumP1Save = "1", engineNumP1Save = "1", shotNumP1Save = "1", gunNumP1Save = "1";
    public static String mastNumP2Save = "1", hullNumP2Save = "1", engineNumP2Save = "1", shotNumP2Save = "1", gunNumP2Save = "1";
    public static boolean isUp = true, isUp2 = true, isUp3 = true;
    public static Random rand = new Random();
    public static int startDir  = 0, startSpd  = 0;
    public static int startDir2 = 0;
    public static int startDir3 = 0;
    public static boolean isSpeed1 = false, isSpeed2 = false;
    public static float speed1 = 200, speed2 = 200;
    public static int speedTimer1, speedTimer2;
    public static boolean isShieldedP1 = false, isShieldedP2 = false;
    public static int isShieldedP1Timer = 0, isShieldedP2Timer = 0;
    public static boolean isTriShotP1 = false, isTriShotP2 = false;
    public static int isTriShotP1Timer = 0, isTriShotP2Timer = 0;
    public static int shipIterSelect = 0;
    public static boolean isLazorActiveP1 = false, isLazorActiveP2 = false;
    public static int isLazorP1Timer = 0, isLazorP2Timer = 0;
    public static long startTime, deltaTime;
    public static int target = 1;
    public static boolean lazorP1Fire = false, lazorP2Fire = false;
    public static float triShotProg1 = 0, speedProg1 = 0, shieldProg1 = 0, lazorProg1 = 0;
    public static float triShotProg2 = 0, speedProg2 = 0, shieldProg2 = 0, lazorProg2 = 0;
    public static boolean isBossPresentToggle = true;
    public static float isBossPresentDelta = 0;

    public static PerspectiveCamera cam;
    public static Environment env2;
    public static Model caps;
    public static ModelInstance capsInst;
    public static ModelBatch modBatch;
    public static ModelLoader modLoad;

    public static float bossTabSpeed = 3;
    public static boolean bossPresent = false;
    public static float bossTabHeight = 20;
    public static float bossX=1200, bossY=10;
    public static float bossVolume = 0;
    public static int bossHealth = 1000;
    public static float bossHealthTrans;
    public static int BossState = 0;
    public static boolean bossDrawn = true;
    public static boolean bossKilled = false;
    public static int BossMusic = 0;
    public static int bossTimer = MathUtils.random(15, 45);
    public static float craft1X, craft1Y, craft2X, craft2Y;

    public static String userName;

    public static boolean isMultiPlayer = MainMenuScreen.getMulti();

    public static void ScreenInit() {

        gameOver = false;
        score = 0;
        healthP1 = 10;  healthP2 = 10;
        scrollTimer = 0.0f;
        playerFire = true;  playerFire2 = true;
        runTime = 0; runTimeHour = 0; runTimeMin = 0; runTimeSec = 0;
        isUp = true; isUp2 = true; isUp3 = true;
        startSpd = 0;
        startDir = 0; startDir2 = 0; startDir3 = 0;
        isSpeed1 = false; isSpeed2 = false;
        speed1 = 200; speed2 = 200;
        isShieldedP1 = false; isShieldedP2 = false;
        isShieldedP1Timer = 0; isShieldedP2Timer = 0;
        isTriShotP1 = false; isTriShotP2 = false;
        isTriShotP1Timer = 0; isTriShotP2Timer = 0;
        shipIterSelect = 0;
        isLazorActiveP1 = false; isLazorActiveP2 = false;
        isLazorP1Timer = 0; isLazorP2Timer = 0;
        target = 1;
        lazorP1Fire = false; lazorP2Fire = false;
        triShotProg1 = 0; speedProg1 = 0; shieldProg1 = 0; lazorProg1 = 0;
        triShotProg2 = 0; speedProg2 = 0; shieldProg2 = 0; lazorProg2 = 0;
        isBossPresentToggle = true;
        isBossPresentDelta = 0;

        bossTabSpeed = 3; bossTabHeight = 20;
        bossPresent = false; bossDrawn = true; bossKilled = false;
        bossX = 1200; bossY = 10; bossVolume = 0;
        BossState = 0; BossMusic = 0; bossTimer = MathUtils.random(15, 45);

        String tempName;
        char[] tempNameArray;
        userName = "";
        char[] name = XMLHandler.User.toLowerCase().toCharArray();
        if(name.length != 0) {
            tempName = name[0] + "";
            System.out.println("net.spacepiratez.common.game.Game.ScreenInit | tempName: " + tempName);
            tempName = tempName.toUpperCase();
            System.out.println("net.spacepiratez.common.game.Game.ScreenInit | tempName: " + tempName);
            tempNameArray = tempName.toCharArray();
            System.out.println("net.spacepiratez.common.game.Game.ScreenInit | tempNameArray[0]: " + tempNameArray[0]);
            name[0] = tempNameArray[0];
            System.out.println("net.spacepiratez.common.game.Game.ScreenInit | name[0]: " + name[0]);
            for(int nameIndex = 0; nameIndex < name.length; nameIndex++) {
                userName = userName + name[nameIndex];
                System.out.println("net.spacepiratez.common.game.Game.ScreenInit | \u001B[31mnameIndex = " + nameIndex + " | \u001B[36muserName: " + userName + ".\u001B[0m");
            }
        }

        Preloader.gameSprite[8].setScale(0.5f);
        Preloader.gameSprite[9].setScale(0.5f);
        Preloader.gameSprite[10].setScale(0.5f);
        Preloader.gameSprite[11].setScale(0.5f);
        Preloader.gameSprite[12].setScale(0.5f);

        switchAllocation();

        craftName[0] = "mast";
        craftName[1] = "hull";
        craftName[2] = "engine";
        craftName[3] = "shot";
        craftName[4] = "gun";

        partName[0] = mastNumP1;
        partName[1] = hullNumP1;
        partName[2] = engineNumP1;
        partName[3] = shotNumP1;
        partName[4] = gunNumP1;

        partName2[0] = mastNumP2;
        partName2[1] = hullNumP2;
        partName2[2] = engineNumP2;
        partName2[3] = shotNumP2;
        partName2[4] = gunNumP2;

        bgGameAudioIndex = MathUtils.random(2, 3);
        Preloader.audioMenuBg[bgGameAudioIndex].setLooping(true);
        if(!Preloader.audioMenuBg[bgGameAudioIndex].isPlaying()) Preloader.audioMenuBg[bgGameAudioIndex].play();

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

        modBatch = new ModelBatch();
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(10f, 10f, 10f);
        cam.lookAt(0, 0, 0);
        cam.near = 0.1f;
        cam.far = 300f;
        cam.update();

        env2 = new Environment();
        env2.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        env2.add(new DirectionalLight().set(0.1f, 0.4f, 0.9f, -1, 0.8f, 0.2f));

        modLoad  = new ObjLoader();
        caps =	modLoad.loadModel(Gdx.files.internal(Reference.Files.MODEL_PLANET));

        capsInst = new ModelInstance(caps);
        capsInst.transform.scale(10, 10, 10);

        craftShape = new Rectangle();
        craftShape.x = 800 / 2 - 64 / 2;
        craftShape.y = 20;
        craftShape.width = 64;
        craftShape.height = 64;

        craft1X = craftShape.x;
        craft1Y = craftShape.y;

        if(isMultiPlayer) {
            craftShape2 = new Rectangle();
            craftShape2.x = 800 / 2 - 64 / 2;
            craftShape2.y = 20;
            craftShape2.width = 64;
            craftShape2.height = 64;

            craft2X = craftShape2.x;
            craft2Y = craftShape2.y;
        }

        hudBack  = MenuNinePatch.getHudBack();
        nineRed  = MenuNinePatch.getInstanceRed();
        nineBlue = MenuNinePatch.getInstanceBlue();

        healthP1 = 10;

        craft[0] = Preloader.shipMastSprite[mastNumP1-1];
        craft[1] = Preloader.shipHullSprite[hullNumP1-1];
        craft[2] = Preloader.shipEngineSprite[engineNumP1-1];
        craft[3] = Preloader.shipShotSprite[shotNumP1-1];
        craft[4] = Preloader.shipGunSprite[gunNumP1-1];

        StatHandler.StatModifier(true, mastNumP1 - 1, hullNumP1 - 1, engineNumP1 - 1, shotNumP1 - 1, gunNumP1 - 1);
        healthP1 =  StatHandler.craftHealth1;

        if(isMultiPlayer) {

            craft2[0] = Preloader.shipMastSprite[mastNumP2-1];
            craft2[1] = Preloader.shipHullSprite[hullNumP2-1];
            craft2[2] = Preloader.shipEngineSprite[engineNumP2-1];
            craft2[3] = Preloader.shipShotSprite[shotNumP2-1];
            craft2[4] = Preloader.shipGunSprite[gunNumP2-1];

            StatHandler.StatModifier(false, mastNumP2-1, hullNumP2-1, engineNumP2-1, shotNumP2-1, gunNumP2-1);
            healthP2 = StatHandler.craftHealth2;
        }

        arrayGame = new Array<Rectangle>();

        System.out.println("net.spacepiratez.common.game.Game.ScreenInit |  Player 1 Health: " + healthP1);
        System.out.println("net.spacepiratez.common.game.Game.ScreenInit |  Player 2 Health: " + healthP2);
    }

    public static void GameRender(byte level, float delta, SpacePiratez game){
        camera.update();
        scrollTimer += Gdx.graphics.getDeltaTime()/25;
        if(scrollTimer > 1.0f) scrollTimer = 0.0f;

        Preloader.gameSprite[0].setU(scrollTimer);
        Preloader.gameSprite[0].setU2(scrollTimer+1);

        batch.begin();
        Preloader.gameSprite[1].setPosition(0, 0);
        Preloader.gameSprite[1].draw(batch);
        Preloader.gameSprite[0].draw(batch);
        batch.end();

        capsInst.transform.rotate(1, 1, 0, 0.1f);
        modBatch.begin(cam);
        modBatch.render(capsInst, env2);
        modBatch.end();
        cam.update();

        batch.begin();
        hudBack.draw(batch, 2, 400, 1020, 110);
        nineBlue.draw(batch, 130, 455, 96, 50);
        if(isMultiPlayer) nineRed.draw(batch, 130, 405, 96, 50);
        for(int i = 0; i < craft.length; i++) {
            if(i != 3) {
//                batch.draw(craft[i], craftShape.x, craftShape.y);
                craft[i].setPosition(craft1X, craft1Y);
                craft[i].draw(batch);
                craft[i].setPosition(147, 466);
                craft[i].draw(batch);
                if(healthP1 <= 0) {
                    nineBlue.draw(batch, 130, 466, 96, 50);
                    Preloader.gameSprite[2].setPosition(163, 466);
                    Preloader.gameSprite[2].draw(batch);
                }
                if(isMultiPlayer) {
                    batch.draw(craft2[i], craft2X, craft2Y);
                    batch.draw(craft2[i], 147, 415);
                    if(healthP2 <= 0) {
                        nineRed.draw(batch, 130, 405, 96, 50);
                        Preloader.gameSprite[2].setPosition(163, 415);
                        Preloader.gameSprite[2].draw(batch);
                    }
                }
            }
        }
        if(isShieldedP1) {
//            batch.draw(Preloader.gameSprite[3], craftShape.x-6, craftShape.y-20);
            Preloader.gameSprite[3].setPosition(craftShape.x-6, craftShape.y-20);
            Preloader.gameSprite[3].draw(batch);
        }
        if(isMultiPlayer) if(isShieldedP2) {
//            batch.draw(Preloader.gameSprite[3], craftShape2.x-6, craftShape2.y-20);
            Preloader.gameSprite[3].setPosition(craftShape2.x-6, craftShape2.y-20);
            Preloader.gameSprite[3].draw(batch);
        }

        Preloader.partLazorEffect1.draw(batch);
        Preloader.partLazorEffect1.update(delta);
        if(isMultiPlayer) {
            Preloader.partLazorEffect2.draw(batch);
            Preloader.partLazorEffect2.update(delta);
        }
//        for(Rectangle laser: arrayGame)             Shortcuts.SpriteDraw(batch, new SPSprite(SPSprite.hostile.LASER), laser.x, laser.y);
//        for(Rectangle plasma: arrayGame)            Shortcuts.SpriteDraw(batch, new SPSprite(SPSprite.hostile.PLASMA), plasma.x, plasma.y);
//        for(Rectangle ship: arrayGame)              Shortcuts.SpriteDraw(batch, new SPSprite(SPSprite.hostile.SHIP), ship.x, ship.y);
//        for(Rectangle fastShip: arrayGame)          Shortcuts.SpriteDraw(batch, new SPSprite(SPSprite.hostile.FASTSHIP), fastShip.x, fastShip.y);
        for(Rectangle laserPlayer: arrayGame)       batch.draw(craft[3], laserPlayer.x, laserPlayer.y);
        if(isMultiPlayer) {
            for(Rectangle laserPlayer2: arrayGame)  batch.draw(craft2[3], laserPlayer2.x, laserPlayer2.y);
        }
        for(Rectangle health: arrayGame) 			Shortcuts.SpriteDraw(batch, Preloader.gameSprite[8], health.x, health.y);//batch.draw(Preloader.gameHealthImage, health.x, health.y);
        for(Rectangle speed:  arrayGame)			Shortcuts.SpriteDraw(batch, Preloader.gameSprite[9], speed.x, speed.y);//batch.draw(Preloader.gameSpeedImage, speed.x, speed.y);
        for(Rectangle shield: arrayGame) 			Shortcuts.SpriteDraw(batch, Preloader.gameSprite[10], shield.x, shield.y);//batch.draw(Preloader.gameShieldImage, shield.x, shield.y);
        for(Rectangle lazor: arrayGame)			    Shortcuts.SpriteDraw(batch, Preloader.gameSprite[12], lazor.x, lazor.y);//batch.draw(Preloader.gameLazorImage, lazor.x, lazor.y);
        for(Rectangle trishot: arrayGame)		    Shortcuts.SpriteDraw(batch, Preloader.gameSprite[11], trishot.x, trishot.y);//batch.draw(Preloader.gameTriImage, trishot.x, trishot.y);
        batch.end();

        if(isShieldedP1Timer >  0) isShieldedP1Timer--;
        if(isShieldedP1Timer <= 0) isShieldedP1 = false;
        if(isMultiPlayer) {
            if(isShieldedP2Timer >  0) isShieldedP2Timer--;
            if(isShieldedP2Timer <= 0) isShieldedP2 = false;
        }
        if(isTriShotP1Timer >  0) isTriShotP1Timer--;
        if(isTriShotP1Timer <= 0) isTriShotP1 = false;
        if(isMultiPlayer) {
            if(isTriShotP2Timer >  0) isTriShotP2Timer--;
            if(isTriShotP2Timer <= 0) isTriShotP2 = false;
        }

        if(isSpeed1)  speed1 = (StatHandler.craftSpeed1) * 1.5f;
        if(!isSpeed1) speed1 = (StatHandler.craftSpeed1);
        if(isMultiPlayer) {
            if(isSpeed2)  speed2 = (StatHandler.craftSpeed2) * 1.5f;
            if(!isSpeed2) speed2 = (StatHandler.craftSpeed2);
        }
        if(speedTimer1 > 0)  speedTimer1--;
        if(speedTimer1 <= 0) isSpeed1 = false;
        if(isMultiPlayer) {
            if(speedTimer2 > 0)  speedTimer2--;
            if(speedTimer2 <= 0) isSpeed2 = false;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.B)) spawnShield(); //Debug

        MovementHandler();

        if(craftShape.x < 0)   craftShape.x = 0;
        if(craftShape.x > 768)  craftShape.x = 768;
        if(craftShape.y < 0)   craftShape.y = 0;
        if(craftShape.y > 370)  craftShape.y = 370;

        if(isMultiPlayer) {
            if(craftShape2.x < 0)   craftShape2.x = 0;
            if(craftShape2.x > 768)  craftShape2.x = 768;
            if(craftShape2.y < 0)   craftShape2.y = 0;
            if(craftShape2.y > 370)  craftShape2.y = 370;
        }

        if(isLazorP1Timer >= 1) lazorP1Fire = true;
        if(isLazorP1Timer <= 0) lazorP1Fire = false;
        if(isLazorP2Timer >= 1) lazorP2Fire = true;
        if(isLazorP2Timer <= 0) lazorP2Fire = false;

        runTime = runTime + System.nanoTime();

        if(runTime >= 100) {
            runTimeSec = runTimeSec + 1;
            runTime = runTime - 100;
        }
        if(runTimeSec >= 60) {
            runTimeMin = runTimeMin + 1;
            runTimeSec = runTimeSec - 60;
        }
        if(runTimeMin >= 60) {
            runTimeHour = runTimeHour + 1;
            runTimeMin = runTimeMin - 60;
        }
        if(!bossPresent) {
            if(runTimeHour >= 1) {
                if(runTimeMin >= bossTimer) {
                    bossPresent = true;
                }
            }
        }

        String totalScore = "Score: " + score;
        String p1TotalHealth = "" + healthP1;
        String p2TotalHealth = "" + healthP2;
        String runTimeHourStr = runTimeHour+"";
        String runTimeMinStr  = String.format("%02d", runTimeMin);
        String runTimeSecStr  = String.format("%02d", runTimeSec);
        String runTimeStr = "Time: " + runTimeHourStr + ":" + runTimeMinStr + ":" + runTimeSecStr;

        if(healthP1 > 9000) {
            p1TotalHealth = "OVER 9000!";
        }else if(healthP1 <= 0) {
            p1TotalHealth = "Dead";
        }else{
            p1TotalHealth = "" + healthP1;
        }
        if(isMultiPlayer) {
            if(healthP2 > 9000) {
                p2TotalHealth = "OVER 9000!";
            }else if(healthP2 <= 0) {
                p2TotalHealth = "Dead";
            }else{
                p2TotalHealth = "" + healthP2;
            }
        }

        IteratorTimers();
        Iterators();
        SpecialIterators();

        if(!gameOver) {
            if(isMultiPlayer) {
                if(healthP1 <= 0 && healthP2 <= 0) {
                    gameOver = true;
                }
            }else{
                if(healthP1 <= 0) {
                    gameOver = true;
                }
            }
        }else{
            LeaderboardHandler.tempTimeFull = runTimeHourStr+":"+runTimeMinStr+":"+runTimeSecStr;
            Preloader.survivalTime = ((((runTimeHour*60)+runTimeMin)*60)+runTimeSec);
            Preloader.survivalScore = score;
            LeaderboardHandler.tempName  = XMLHandler.User;
            LeaderboardHandler.tempScore = Preloader.survivalScore+"";
            LeaderboardHandler.tempTime  = Preloader.survivalTime+"";
//            LeaderboardHandler.tempTimeFull = Preloader.survivalTime+"";
            LeaderboardHandler.orderBoard();
            Preloader.audioMenuBg[bgGameAudioIndex].stop();
            game.setScreen(new GameOverScreen(game));
        }

        triShotProg1	= 0; triShotProg2	= 0;
        speedProg1		= 0; speedProg2		= 0;
        shieldProg1		= 0; shieldProg2	= 0;
        lazorProg1		= 0; lazorProg2		= 0;
        if(isTriShotP1Timer != 0)   triShotProg1 	= (isTriShotP1Timer/6);
        if(speedTimer1 != 0)		speedProg1   	= (speedTimer1/6);
        if(isShieldedP1Timer != 0)	shieldProg1		= (isShieldedP1Timer/6);
        if(isLazorP1Timer != 0)		lazorProg1		= (isLazorP1Timer/6);

        if(isTriShotP2Timer != 0)   triShotProg2 	= (isTriShotP2Timer/6);
        if(speedTimer2 != 0)		speedProg2   	= (speedTimer2/6);
        if(isShieldedP2Timer != 0)	shieldProg2		= (isShieldedP2Timer/6);
        if(isLazorP2Timer != 0)		lazorProg2		= (isLazorP2Timer/6);

        batch.begin();
        //Trishot
        Shortcuts.ColourNinePatchGrad(batch, 0, 0.8f, 0.8f, 1, 400, 450, 106, 30);
        if(isTriShotP1Timer != 0 && isTriShotP1Timer < 600)  Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 403, 453, triShotProg1, 24);
        if(isTriShotP1Timer >= 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 403, 453, 100, 24);
        //Speed
        Shortcuts.ColourNinePatchGrad(batch, 0, 0.8f, 0.8f, 1, 550, 450, 106, 30);
        if(speedTimer1 != 0 && speedTimer1 < 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 553, 453, speedProg1, 24);
        if(speedTimer1 >= 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 553, 453, 100, 24);
        //Shield
        Shortcuts.ColourNinePatchGrad(batch, 0, 0.8f, 0.8f, 1, 700, 450, 106, 30);
        if(isShieldedP1Timer != 0 && isShieldedP1Timer < 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 703, 453, shieldProg1, 24);
        if(isShieldedP1Timer >= 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 703, 453, 100, 24);
        //Lazor
        Shortcuts.ColourNinePatchGrad(batch, 0, 0.8f, 0.8f, 1, 850, 450, 106, 30);
        if(isLazorP1Timer != 0 && isLazorP1Timer < 300) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 853, 453, lazorProg1, 24);
        if(isLazorP1Timer >= 300) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 853, 453, 100, 24);

        if(isMultiPlayer) {
            //Trishot
            Shortcuts.ColourNinePatchGrad(batch, 0.8f, 0.3f, 0, 1, 400, 410, 106, 30);
            if(isTriShotP2Timer != 0 && isTriShotP2Timer < 600)	Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 403, 413, triShotProg2, 24);
            if(isTriShotP2Timer >= 600)	Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 403, 413, 100, 24);
            //Speed
            Shortcuts.ColourNinePatchGrad(batch, 0.8f, 0.3f, 0, 1, 550, 410, 106, 30);
            if(speedTimer2 != 0 && speedTimer2 < 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 553, 413, speedProg2, 24);
            if(speedTimer2 >= 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 553, 413, 100, 24);
            //Shield
            Shortcuts.ColourNinePatchGrad(batch, 0.8f, 0.3f, 0, 1, 700, 410, 106, 30);
            if(isShieldedP2Timer != 0 && isShieldedP2Timer < 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 703, 413, shieldProg2, 24);
            if(isShieldedP2Timer >= 600) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 703, 413, 100, 24);
            //Lazor
            Shortcuts.ColourNinePatchGrad(batch, 0.8f, 0.3f, 0, 1, 850, 410, 106, 30);
            if(isLazorP2Timer != 0 && isLazorP2Timer < 300) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 853, 413, lazorProg2, 24);
            if(isLazorP2Timer >= 300) Shortcuts.ColourNinePatchTrans(batch, 1, 0, 1, 1, 853, 413, 100, 24);
        }
        batch.end();



        game.batch.begin();
        game.font.setColor(Color.BLUE);
        game.font.draw(game.batch, userName + "'s Health: ", 10, 490);
        game.font.draw(game.batch, p1TotalHealth, 10, 470);
        game.font.draw(game.batch, "Trishot", 400, 500);
        game.font.draw(game.batch, "Speed", 550, 500);
        game.font.draw(game.batch, "Shield", 700, 500);
        game.font.draw(game.batch, "Lazor", 850, 500);
        if(isMultiPlayer) {
            game.font.setColor(Color.RED);
            game.font.draw(game.batch, "Player 2's Health: ", 10, 450);
            game.font.draw(game.batch, p2TotalHealth, 10, 430);
        }
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, totalScore, 250, 490);
        game.font.draw(game.batch, runTimeStr, 250, 450);
        game.batch.end();
    }


    public static void switchAllocation() {

        XMLHandler.readXML();
        mastNumP1Save   = XMLHandler.mastP1();
        hullNumP1Save   = XMLHandler.hullP1();
        engineNumP1Save = XMLHandler.engineP1();
        shotNumP1Save   = XMLHandler.shotP1();
        gunNumP1Save    = XMLHandler.gunP1();

        mastNumP1   = Integer.valueOf(mastNumP1Save);
        hullNumP1   = Integer.valueOf(hullNumP1Save);
        engineNumP1 = Integer.valueOf(engineNumP1Save);
        shotNumP1   = Integer.valueOf(shotNumP1Save);
        gunNumP1    = Integer.valueOf(gunNumP1Save);

        if(isMultiPlayer) {
            mastNumP2Save   = XMLHandler.mastP2();
            hullNumP2Save   = XMLHandler.hullP2();
            engineNumP2Save = XMLHandler.engineP2();
            shotNumP2Save   = XMLHandler.shotP2();
            gunNumP2Save    = XMLHandler.gunP2();

            mastNumP2   = Integer.valueOf(mastNumP2Save);
            hullNumP2   = Integer.valueOf(hullNumP2Save);
            engineNumP2 = Integer.valueOf(engineNumP2Save);
            shotNumP2   = Integer.valueOf(shotNumP2Save);
            gunNumP2    = Integer.valueOf(gunNumP2Save);
        }
    }

    private static void spawnLaser() {
        System.out.println("Game.spawnLaser");
        Rectangle laser = new Rectangle();
        laser.x = 1024;
        laser.y = MathUtils.random(0, 400);
        laser.width = 16;
        laser.height = 16;
        arrayGame.add(laser);
        laserLastFire = TimeUtils.nanoTime();
    }
    private static void spawnShip() {
        System.out.println("Game.spawnShip");
        ship = new Rectangle();
        ship.x = 1024;
        ship.y = MathUtils.random(0, 350);
        ship.width = 64;
        ship.height = 64;
        arrayGame.add(ship);
        shipLastFire = TimeUtils.nanoTime();
    }
    private static void spawnFastShip() {
        System.out.println("Game.spawnFastShip1");
        Rectangle fastShip  = new Rectangle();
        fastShip.x = 1024;
        fastShip.y = MathUtils.random(0, 350);
        fastShip.width = 64;
        fastShip.height = 64;
        startDir = rand.nextInt(2);
        if(startDir == 0) isUp = true;
        if(startDir != 0) isUp = false;
        arrayGame.add(fastShip);
    }
    private static void spawnLaserAtShip(float xCoord, float yCoord) {
        System.out.println("Game.spawnLaserAtShip");
        Rectangle laser = new Rectangle();
        laser.x = xCoord;
        laser.y = yCoord;
        laser.width = 16;
        laser.height = 16;
        arrayGame.add(laser);
        laserLastFire = TimeUtils.nanoTime();
    }
    private static void spawnLaserAtPlayer(float xCoord, float yCoord) {
        System.out.println("Game.spawnLaserAtPlayer");
        Rectangle laserPlayer = new Rectangle();
        laserPlayer.x = xCoord;
        laserPlayer.y = yCoord;
        laserPlayer.width = 16;
        laserPlayer.height = 16;
        arrayGame.add(laserPlayer);
        laserPlayerLastFire = TimeUtils.nanoTime();
    }
    private static void spawnLaserAtPlayer2(float xCoord, float yCoord) {
        System.out.println("Game.spawnLaserAtPlayer2");
        Rectangle laserPlayer2 = new Rectangle();
        laserPlayer2.x = xCoord;
        laserPlayer2.y = yCoord;
        laserPlayer2.width = 16;
        laserPlayer2.height = 16;
        arrayGame.add(laserPlayer2);
        laserPlayer2LastFire = TimeUtils.nanoTime();
    }
    private static void spawnPlasma() {
        System.out.println("Game.spawnPlasma");
        Rectangle plasma = new Rectangle();
        plasma.x = 1024;
        plasma.y = MathUtils.random(0, 400);
        plasma.width = 16;
        plasma.height = 16;
        arrayGame.add(plasma);
        plasmaLastFire = TimeUtils.nanoTime();
    }
    private static void spawnHealth() {
        System.out.println("Game.spawnHealth");
        Rectangle health = new Rectangle();
        health.x = 1024;
        health.y = MathUtils.random(0, 400);
        health.width = 16;
        health.height = 16;
        arrayGame.add(health);
        healthLastFire = TimeUtils.nanoTime();
    }
    private static void spawnSpeed() {
        System.out.println("Game.spawnSpeed");
        Rectangle speed = new Rectangle();
        speed.x = 1024;
        speed.y = MathUtils.random(0, 400);
        speed.width = 30;
        speed.height = 22;
        arrayGame.add(speed);
        speedLastFire = TimeUtils.nanoTime();
    }
    private static void spawnShield() {
        System.out.println("Game.spawnShield");
        Rectangle shield = new Rectangle();
        shield.x = 1024;
        shield.y = MathUtils.random(0, 400);
        shield.width = 16;
        shield.height = 16;
        arrayGame.add(shield);
        shieldLastFire = TimeUtils.nanoTime();
    }
    private static void spawnLazor() {
        System.out.println("Game.spawnLazor");
        Rectangle lazor = new Rectangle();
        lazor.x = 1024;
        lazor.y = MathUtils.random(0, 400);
        lazor.width = 29;
        lazor.height = 22;
        arrayGame.add(lazor);
        lazorLastFire = TimeUtils.nanoTime();
    }
    private static void spawnTrishot() {
        System.out.println("Game.spawnTrishot");
        Rectangle trishot = new Rectangle();
        trishot.x = 1024;
        trishot.y = MathUtils.random(0, 400);
        trishot.width = 22;
        trishot.height = 29;
        arrayGame.add(trishot);
        triLastFire = TimeUtils.nanoTime();
    }
    private static void spawnLazorFire(float xCoord, float yCoord) {
        System.out.println("Game.spawnLazorFire");
        Rectangle lazorFire = new Rectangle();
        lazorFire.x = xCoord;
        lazorFire.y = yCoord;
        lazorFire.width = 64;
        lazorFire.height = 64;
        arrayGame.add(lazorFire);
    }
    private static void spawnLazorFireP2(float xCoord, float yCoord) {
        System.out.println("Game.spawnLazorFireP2");
        Rectangle lazorFire2 = new Rectangle();
        lazorFire2.x = xCoord;
        lazorFire2.y = yCoord;
        lazorFire2.width = 64;
        lazorFire2.height = 64;
        arrayGame.add(lazorFire2);
    }

    public static void MovementHandler() {
        if(isMultiPlayer) {
            if(!(healthP1 <= 0)) {
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveLeftP1))   craft1X -= speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveRightP1)) 	craft1X += speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveDownP1)) 	craft1Y -= speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveUpP1)) 	craft1Y += speed1 * Gdx.graphics.getDeltaTime();

                if(Gdx.input.isKeyPressed(MainMenuScreen.actionShotP1) && playerFire) {
                    spawnLaserAtPlayer(craftShape.x+56, craftShape.y-8);
                    if(isTriShotP1) {
                        spawnLaserAtPlayer(craftShape.x + 56, craftShape.y-32);
                        spawnLaserAtPlayer(craftShape.x + 56, craftShape.y+16);
                    }
                    playerFire = false;
                }
                if(Gdx.input.isKeyPressed(MainMenuScreen.actionLazorP1)) {
                    if(isLazorP1Timer > 0) {
                        Preloader.partLazorEffect1.setPosition(craftShape.x + 50, craftShape.y);
                        spawnLazorFire(craftShape.x + 50, craftShape.y-32);
                        isLazorP1Timer--;
                    }else Preloader.partLazorEffect1.setPosition(0, 600);
                }else Preloader.partLazorEffect1.setPosition(0, 600);
            }else Preloader.partLazorEffect1.setPosition(0, 600);
            if(!(healthP2 <= 0)) {
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveLeftP2))  craft2X -= speed2 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveRightP2)) craft2X += speed2 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveDownP2))  craft2Y -= speed2 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveUpP2))    craft2Y += speed2 * Gdx.graphics.getDeltaTime();

                if(Gdx.input.isKeyPressed(MainMenuScreen.actionShotP2) && playerFire2) {
                    spawnLaserAtPlayer2(craftShape2.x+56, craftShape2.y-8);
                    if(isTriShotP2) {
                        spawnLaserAtPlayer(craftShape2.x + 56, craftShape2.y-32);
                        spawnLaserAtPlayer(craftShape2.x + 56, craftShape2.y+16);
                    }
                    playerFire2 = false;
                }

                if(Gdx.input.isKeyPressed(MainMenuScreen.actionLazorP2)) {
                    if(isLazorP2Timer > 0) {
                        Preloader.partLazorEffect2.setPosition(craftShape2.x + 50, craftShape2.y);
                        spawnLazorFireP2(craftShape2.x + 50, craftShape2.y-32);
                        isLazorP2Timer--;
                    }else Preloader.partLazorEffect2.setPosition(0, 600);
                }else Preloader.partLazorEffect2.setPosition(0, 600);
            }else Preloader.partLazorEffect2.setPosition(0, 600);

        }else{
            if(!(healthP1 <= 0)) {
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveLeftP1)     || Gdx.input.isKeyPressed(MainMenuScreen.moveLeftP2))	    craft1X -= speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveRightP1)    || Gdx.input.isKeyPressed(MainMenuScreen.moveRightP2))     craft1X += speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveDownP1)     || Gdx.input.isKeyPressed(MainMenuScreen.moveDownP2))	    craft1Y -= speed1 * Gdx.graphics.getDeltaTime();
                if(Gdx.input.isKeyPressed(MainMenuScreen.moveUpP1)       || Gdx.input.isKeyPressed(MainMenuScreen.moveUpP2))	    craft1Y += speed1 * Gdx.graphics.getDeltaTime();

                if((Gdx.input.isKeyPressed(MainMenuScreen.actionShotP1) || Gdx.input.isKeyPressed(MainMenuScreen.actionShotP2)) && playerFire) {
                    spawnLaserAtPlayer(craftShape.x + 56, craftShape.y-8);
                    if(isTriShotP1) {
                        spawnLaserAtPlayer(craftShape.x + 56, craftShape.y-32);
                        spawnLaserAtPlayer(craftShape.x + 56, craftShape.y+16);
                    }
                    playerFire = false;
                }

                if(Gdx.input.isKeyPressed(MainMenuScreen.actionLazorP1) || Gdx.input.isKeyPressed(MainMenuScreen.actionLazorP2)) {
                    if(isLazorP1Timer > 0) {
                        Preloader.partLazorEffect1.setPosition(craftShape.x + 50, craftShape.y);
                        spawnLazorFire(craftShape.x + 50, craftShape.y-32);
                        isLazorP1Timer--;
                    }else Preloader.partLazorEffect1.setPosition(0, 600);
                }else Preloader.partLazorEffect1.setPosition(0, 600);
            }else Preloader.partLazorEffect1.setPosition(0, 600);
        }

        if(craft1X > 750)       craft1X = 750;
        if(craft1X < 0  )       craft1X = 0;
        if(craft1Y > 400)       craft1Y = 400;
        if(craft1Y < 0  )       craft1Y = 0;
        craftShape.x = craft1X;
        craftShape.y = craft1Y;
        if(isMultiPlayer) {
            if(craft2X > 750)   craft2X = 750;
            if(craft2X < 0  )   craft2X = 0;
            if(craft2Y > 400)   craft2Y = 400;
            if(craft2Y < 0  )   craft2Y = 0;
            craftShape2.x = craft2X;
            craftShape2.y = craft2Y;
        }
    }

    public static void IteratorTimers() {
        if(TimeUtils.nanoTime() - laserLastFire  > 1000000000) {
            int laserFire = MathUtils.random(0, 100);
            if(laserFire == 3) {
                spawnLaser();
            }
        }
        if(TimeUtils.nanoTime() - plasmaLastFire > 2147483647) {
            int plasmaFire = MathUtils.random(0, 500);
            if(plasmaFire == 125 || plasmaFire == 250 || plasmaFire == 375) {
                spawnPlasma();
                spawnPlasma();
                spawnPlasma();
            }
        }
        if(TimeUtils.nanoTime() - shipLastFire   > 2147483647) {
            int shipFire = MathUtils.random(0, 1000);
            if(shipFire == 50) spawnShip();
        }
        if(TimeUtils.nanoTime() - fastShipLastFire > 2147483647) {
            int fastShipFire = MathUtils.random(0, 1000);
            if(fastShipFire == 150 || fastShipFire == 300) spawnFastShip();
        }
        if(TimeUtils.nanoTime() - healthLastFire > 2000000000) {
            int healthFire = MathUtils.random(0, 5000);
            if(healthFire == 100) spawnHealth();
        }
        if(TimeUtils.nanoTime() - speedLastFire > 2000000000) {
            int speedFire = MathUtils.random(0, 7000);
            if(speedFire == 100) spawnSpeed();
        }
        if(TimeUtils.nanoTime() - shieldLastFire > 2000000000) {
            int shieldFire = MathUtils.random(0, 6500);
            if(shieldFire == 100) spawnShield();
        }
        if(TimeUtils.nanoTime() - lazorLastFire > 2147483647) {
            int lazorFire = MathUtils.random(0, 10000);
            if(lazorFire == 100) spawnLazor();
        }
        if(TimeUtils.nanoTime() - triLastFire > 2000000000) {
            int triFire = MathUtils.random(0, 2500);
            if(triFire == 100) spawnTrishot();
        }
        if(TimeUtils.nanoTime() - laserPlayerLastFire > 1000000000) playerFire = true;
        if(isMultiPlayer) {
            if(TimeUtils.nanoTime() - laserPlayer2LastFire > 1000000000) playerFire2 = true;
        }
    }

    public static void Iterators() {
        //MARK laser Iterator
        iterGame = arrayGame.iterator();
        while (iterGame.hasNext()) {
            try {
                gameRect = iterGame.next();
                gameRect.x -= 200 * Gdx.graphics.getDeltaTime();
                if (gameRect.x + 64 < 0) {
                    iterGame.remove();
                    score = score + 1;
                }
                IteratorCollide(gameRect, gameRect);
//                IteratorLazor2(laser, iterLaser);
//                IteratorPlayer1(laser, iterLaser);
//                IteratorPlayer2(laser, iterLaser);
            } catch (Exception e) {
                System.out.println("	Error: " + e);
            }
            if (gameRect.overlaps(craftShape) && !(healthP1 <= 0)) {
                if (isShieldedP1) {
                    shieldHitSound = MathUtils.random(-1, 1);
                    Preloader.audioImpactShield[0].play(1, 1, shieldHitSound);
                } else {
                    hitSound = MathUtils.random(0, Reference.IMPACT_SOUNDS);
                    Preloader.audioImpact[hitSound].play();
                }
//				System.out.println("impact sound: " + hitSound);
                iterGame.remove();
                if (!isShieldedP1) healthP1 = healthP1 - 1;
                System.out.println("Health: " + healthP1);
            }
            if (isMultiPlayer) {
                if (gameRect.overlaps(craftShape2) && !(healthP2 <= 0)) {
                    if (isShieldedP2) {
                        shieldHitSound = MathUtils.random(-1, 1);
                        Preloader.audioImpactShield[0].play(1, 1, shieldHitSound);
                    } else {
                        hitSound = MathUtils.random(0, Reference.IMPACT_SOUNDS);
                        Preloader.audioImpact[hitSound].play();
                    }
//					System.out.println("Impact Sound: " + hitSound);
                    iterGame.remove();
                    if (!isShieldedP2) healthP2 = healthP2 - 1;
                    System.out.println("Health: " + healthP2);
                }
            }
        }
    }
    private static void IteratorCollide(Rectangle Collider, Rectangle Collidee) {
        iterGame = arrayGame.iterator();
        while(iterGame.hasNext()) {
            try{
                Rectangle lazorFire = iterGame.next();
                lazorFire.x += (750) * Gdx.graphics.getDeltaTime();
                if(Collider.overlaps(Collidee)) {
                    try{
                        Collider.merge(Collidee);
                        iterGame.remove();
                    }catch (Exception e) {
                        System.out.println("	Error: " + e);
                    }
                }
                if(lazorFire.x > 1024) {
                    try{
                        iterGame.remove();
                    }catch (Exception e) {
                        System.out.println("	Error: " + e);
                    }
                }
            }catch (Exception e) {
                System.out.println("	Error: " + e);
            }
        }

    }

    public static void SpecialIterators() {
        //lazorFire 1
        iterGame = arrayGame.iterator();
        while(iterGame.hasNext()) {
            Rectangle lazorFire = iterGame.next();
            lazorFire.x += (750) * Gdx.graphics.getDeltaTime();
            if(lazorFire.x > 1024) iterGame.remove();
        }
    }
}
