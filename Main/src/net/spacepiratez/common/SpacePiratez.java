package net.spacepiratez.common;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.spacepiratez.utils.Preloader;
import net.spacepiratez.utils.XMLHandler;

/**
 * Created by Guy on 09/03/14.
 */
public class SpacePiratez extends Game {

    public SpriteBatch batch;
    public BitmapFont font;
    public static AssetManager manager = new AssetManager();
    public static String Build;

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        Preloader.loadAssets();
        XMLHandler.readXML();
        XMLHandler.readXMLCont();
//        XMLHandler.UpdateLeaderboard();
        this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
        super.render();
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
    }

}