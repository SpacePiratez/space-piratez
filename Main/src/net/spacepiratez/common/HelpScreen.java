package net.spacepiratez.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.spacepiratez.utils.Preloader;
import net.spacepiratez.utils.MenuNinePatch;
import net.spacepiratez.utils.Reference;
import net.spacepiratez.utils.Shortcuts;

/**
 * Created by Guy on 09/03/14.
 */
public class HelpScreen implements Screen, InputProcessor {

    final SpacePiratez game;
    private Stage stage;
    private MenuNinePatch nineBlue, nineRed, nineGreen, nineClear, nineBase;
    private MenuNinePatch indiRed, indiYellow, indiGreen;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private float healthTextY, shieldTextY, speedTextY, triTextY, lazorTextY;
    private float controlP1TextY, controlP2TextY;
    private float r=0, g=0, b=0;
    private boolean rUp = true, gUp = true, bUp = true;
    private boolean isReturnSwitch = false, isControlSwitch = false, isControlWindow = false;
    public int defaultControlsP1[] = new int[6];
    public int defaultControlsP2[] = new int[6];
    public boolean controlsOneToggle[] = new boolean[6];
    public boolean controlsTwoToggle[] = new boolean[6];
    public int controlsOneDelta[] = new int[6];
    public int controlsTwoDelta[] = new int[6];
    public boolean waitKeypress = false;
    public int isControlDelta = 0;
    public String ControlsP1Trans[] = new String[6];
    public String ControlsP2Trans[] = new String[6];
    public String defaultControlsP1Trans[] = new String[6];
    public String defaultControlsP2Trans[] = new String[6];

    public HelpScreen(final SpacePiratez gam) {
        game = gam;

        for(int i = 0; i < 6; i++) {
            controlsOneDelta[i] = 0;
            controlsTwoDelta[i] = 0;
            controlsOneToggle[i] = true;
            controlsTwoToggle[i] = true;
        }

        defaultControlsP1[0]  = Reference.Controls.moveUpP1;			defaultControlsP1Trans[0] = "W";
        defaultControlsP1[1]  = Reference.Controls.moveDownP1;		    defaultControlsP1Trans[1] = "S";
        defaultControlsP1[2]  = Reference.Controls.moveLeftP1;		    defaultControlsP1Trans[2] = "A";
        defaultControlsP1[3]  = Reference.Controls.moveRightP1;		    defaultControlsP1Trans[3] = "D";
        defaultControlsP1[4]  = Reference.Controls.actionFireP1;		defaultControlsP1Trans[4] = "F";
        defaultControlsP1[5]  = Reference.Controls.actionLazorP1;	    defaultControlsP1Trans[5] = "R";

        defaultControlsP2[0]  = Reference.Controls.moveUpP2;			defaultControlsP2Trans[0] = "Up";
        defaultControlsP2[1]  = Reference.Controls.moveDownP2;		    defaultControlsP2Trans[1] = "Down";
        defaultControlsP2[2]  = Reference.Controls.moveLeftP2;		    defaultControlsP2Trans[2] = "Left";
        defaultControlsP2[3]  = Reference.Controls.moveRightP2;		    defaultControlsP2Trans[3] = "Right";
        defaultControlsP2[4]  = Reference.Controls.actionFireP2;		defaultControlsP2Trans[4] = "Right Control";
        defaultControlsP2[5]  = Reference.Controls.actionLazorP2;	    defaultControlsP2Trans[5] = "Right Shift";

        nineBlue   	= MenuNinePatch.getInstanceBlue();
        nineRed    	= MenuNinePatch.getInstanceRed();
        nineGreen  	= MenuNinePatch.getInstanceGreen();
        nineClear  	= MenuNinePatch.getInstanceClear();
        nineBase	= MenuNinePatch.getInstanceBase();

        indiRed    	= MenuNinePatch.getIndicatorRed();
        indiYellow 	= MenuNinePatch.getIndicatorYellow();
        indiGreen  	= MenuNinePatch.getIndicatorGreen();

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

        if(!Preloader.audioMenuBg[0].isPlaying() && !Preloader.audioMenuBg[1].isPlaying()) Preloader.audioMenuBg[Preloader.bgAudioIndex].play();

    }

    public void create() {
        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
//        batch.draw(Preloader.mainTitleSprite[0], 0, 0);
        Preloader.mainTitleSprite[0].setPosition(0, 0);
        Preloader.mainTitleSprite[0].draw(batch);
        nineBlue.draw(batch, 25, 25, 974, 462);
        nineClear.draw(batch, 75, 395, 80, 80);
//        batch.draw(Preloader.gameHealthImage, 83, 403);
        Preloader.gameSprite[8].setPosition(83, 403);
        Preloader.gameSprite[8].draw(batch);
        nineClear.draw(batch, 75, 305, 80, 80);
//        batch.draw(Preloader.helpShieldImageLarge, 83, 313);
        Preloader.gameSprite[10].setPosition(83, 313);
        Preloader.gameSprite[10].draw(batch);
        nineClear.draw(batch, 75, 215, 80, 80);
//        batch.draw(Preloader.helpSpeedImageLarge, 85, 233);
        Preloader.gameSprite[9].setPosition(83, 233);
        Preloader.gameSprite[9].draw(batch);
        nineClear.draw(batch, 75, 125, 80, 80);
//        batch.draw(Preloader.helpTriImageLarge, 85, 133);
        Preloader.gameSprite[11].setPosition(83, 133);
        Preloader.gameSprite[11].draw(batch);
        nineClear.draw(batch, 75, 35, 80, 80);
//        batch.draw(Preloader.helpLazorImageLarge, 86, 53);
        Preloader.gameSprite[12].setPosition(83, 53);
        Preloader.gameSprite[12].draw(batch);

//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 455, 80, 16);
//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 435, 80, 16);
//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 415, 80, 16);
//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 395, 80, 16);
//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 375, 80, 16);
//		Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 355, 80, 16);
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 700, 400, 80, 32);

        Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 809, 35, 180, 90);
//        Shortcuts.ColourTexF(batch, Preloader.helpReturn, 0.5f, 0, 0.8f, 1, 809, 35);
        Preloader.helpReturnSprite.setColor(0.5f, 0, 0.8f, 1);
        Shortcuts.SpriteDraw(batch, Preloader.helpReturnSprite, 809, 35);

//		batch.draw(returnTxt, 809, 35);

//		switch(MathUtils.random(0, 2)) {
//		case 0: if(rUp) { r=r+0.01f; }else{ r=r-0.01f; } break;
//		case 1: if(gUp) { g=g+0.01f; }else{ g=g-0.01f; } break;
//		case 2: if(bUp) { b=b+0.01f; }else{ b=b-0.01f; } break;
//		}
//		if(r >= 1) { r=0.99f; rUp = false; }
//		if(r <= 0) { r=0.01f; rUp = true;  }
//		if(g >= 1) { g=0.99f; gUp = false; }
//		if(g <= 0) { g=0.01f; gUp = true;  }
//		if(b >= 1) { b=0.99f; bUp = false; }
//		if(b <= 0) { b=0.01f; bUp = true;  }
//
//		Shortcuts.ColourNinePatch(batch, r, g, b, 512, 100, 150, 225);

        batch.end();
        HelpArrays();
        keyPressTrans();
        game.batch.begin();
        game.font.setColor(Color.ORANGE);
        healthTextY = 475;
        shieldTextY = 385;
        speedTextY  = 275;
        triTextY    = 185;
        lazorTextY  = 110;
        controlP1TextY = 475;
        controlP2TextY = 275;
        for(int a = 0; a < 4; a++) title(Color.ORANGE, 175, healthTextY-(20*a), healthText[a]);
        for(int b = 0; b < 4; b++) title(Color.ORANGE, 175, shieldTextY-(20*b), shieldText[b]);
        for(int c = 0; c < 4; c++) title(Color.ORANGE, 175, speedTextY-(20*c),  speedText[c]);
        for(int d = 0; d < 4; d++) title(Color.ORANGE, 175, triTextY-(20*d),    trishotText[d]);
        for(int e = 0; e < 4; e++) title(Color.ORANGE, 175, lazorTextY-(20*e),  lazorText[e]);

        title(Color.BLUE, 800, 500, "Player 1 Controls");
        for(int f = 0; f < MainMenuScreen.ControlsP1.length; f++) {
            if(MainMenuScreen.ControlsP1[f] == defaultControlsP1[f]) {
                title(Color.ORANGE, 800, controlP1TextY-(20*f), ControlsP1Trans[f]+"	[Default]");
            }else if(MainMenuScreen.ControlsP1[f] == 0){
                title(Color.RED, 800, controlP1TextY-(20*f), defaultControlsP1Trans[f]+"	[Error]");
            }else{
                title(Color.GREEN, 800, controlP1TextY-(20*f), ControlsP1Trans[f]+"");
            }
        }

        title(Color.RED, 800, 300, "Player 2 Controls");
        for(int g = 0; g < MainMenuScreen.ControlsP2.length; g++) {
            if(MainMenuScreen.ControlsP2[g] == defaultControlsP2[g]) {
                title(Color.ORANGE, 800, controlP2TextY-(20*g), ControlsP2Trans[g]+"	[Default]");
            }else if(MainMenuScreen.ControlsP2[g] == 0) {
                title(Color.RED, 800, controlP2TextY-(20*g), defaultControlsP2Trans[g]+"	[Error]");
            }else{
                title(Color.GREEN, 800, controlP2TextY-(20*g), ControlsP2Trans[g]+"");
            }
        }

        game.batch.end();
        //Control Change
        if(Gdx.input.getX() >= 700 && Gdx.input.getX() <= 780) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 400 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 432) {
                if(Gdx.input.isTouched()) {
                    if(!isControlSwitch) {
                        isControlSwitch = true;
                        isControlDelta = 0;
                        isControlWindow = true;
                    }
                }
            }
        }
        //Return
        if(Gdx.input.getX() >= 809 && Gdx.input.getX() <= 989) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 35 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 125) {
                if(Gdx.input.isTouched()) {
                    if(!isReturnSwitch) isReturnSwitch = true;
                }
            }
        }

        if(Gdx.input.isKeyPressed(Keys.X)) game.setScreen(new MainMenuScreen(game));
        if(isReturnSwitch) game.setScreen(new MainMenuScreen(game));
//		if(isControlWindow) {
//			ProcessBuilder pb = new ProcessBuilder("server.bat");
//			pb.directory(new File(deployDir + "\\com\\project\\util"));
//			Process p = pb.start();
//			int exitStatus = p.waitFor();
//		}
        if(isControlSwitch) {
            if(isControlDelta > 10) {
                isControlSwitch = false;
                isControlDelta = 0;
            }
            isControlDelta++;
        }
    }

    public void resize(int width, int height) {

    }

    public void show() {

    }

    public void hide() {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }

    public void RegisterTexture(Texture varName, String filePath, String fileName, String fileExt) {
        varName = new Texture(Gdx.files.internal("data/" + filePath + "/" + fileName + "." + fileExt));
        varName.setFilter(TextureFilter.Linear, TextureFilter.Linear);
    }

    public void title(Color par1, float xCoord, float yCoord, String par4) {
        game.font.setColor(par1);
        game.font.draw(game.batch, par4, xCoord, yCoord);
        game.font.setColor(Color.WHITE);
    }

    public String[] healthText,  shieldText, speedText;
    public String[] trishotText, lazorText;

    public void HelpArrays() {
        healthText = new String[4];
        healthText[0] = "";
        healthText[1] = "Increases player health by 5.";
        healthText[2] = "Activates instantly.";
        healthText[3] = "";

        shieldText = new String[4];
        shieldText[0] = "";
        shieldText[1] = "Gives the player a damage shield.";
        shieldText[2] = "Lasts 10 seconds.";
        shieldText[3] = "";

        speedText = new String[4];
        speedText[0] = "Accelerates the players move speed";
        speedText[1] = "from 300 units to 500 units.";
        speedText[2] = "Lasts 10 seconds.";
        speedText[3] = "";

        trishotText = new String[4];
        trishotText[0] = "Allows the player to fire";
        trishotText[1] = "in parallel sets of three.";
        trishotText[2] = "Lasts 10 seconds.";
        trishotText[3] = "";

        lazorText = new String[4];
        lazorText[0] = "Allows the player to use the";
        lazorText[1] = "Super mega awesome death laser";
        lazorText[2] = "of rainbows and destruction.";
        lazorText[3] = "Can be fired for 5 seconds.";
    }

    public void keyPressTrans() {
        String temp = "";
        for(int l = 0; l < ControlsP1Trans.length; l++) ControlsP1Trans[l] = String.valueOf(MainMenuScreen.ControlsP1[l]);
        for(int n = 0; n < ControlsP2Trans.length; n++) ControlsP2Trans[n] = String.valueOf(MainMenuScreen.ControlsP2[n]);

        for(int m = 0; m < ControlsP1Trans.length; m++) {
            switch(ControlsP1Trans[m]) {
                case "7":   temp = "0"; break;
                case "8":   temp = "1"; break;
                case "9":   temp = "2"; break;
                case "10":  temp = "3"; break;
                case "11":  temp = "4"; break;
                case "12":	temp = "5"; break;
                case "13":	temp = "6"; break;
                case "14":	temp = "7"; break;
                case "15":	temp = "8"; break;
                case "16":	temp = "9"; break;
                case "29":	temp = "A"; break;
                case "57":	temp = "Left Alt"; break;
                case "58": 	temp = "Right Alt"; break;
                case "75":	temp = "Apostrophe"; break;
                case "77":	temp = "AT"; break;
                case "30":	temp = "B"; break;
                case "4":	temp = "Back"; break;
                case "73":	temp = "Back Slash"; break;
                case "31":	temp = "C"; break;
                case "5":	temp = "Call"; break;
                case "27":	temp = "Camera"; break;
                case "28":	temp = "Clear"; break;
                case "55":	temp = "Comma"; break;
                case "32":	temp = "D"; break;
                case "67":	temp = "Del/Backspace"; break;
                case "112":	temp = "Forward Del"; break;
                case "23": 	temp = "Center"; break;
                case "20":	temp = "Down"; break;
                case "21":	temp = "Left"; break;
                case "22":	temp = "Right"; break;
                case "19":	temp = "Up"; break;
                case "33":	temp = "E"; break;
                case "6":	temp = "End Call"; break;
                case "66":	temp = "Enter"; break;
                case "65":	temp = "Envelope"; break;
                case "70":	temp = "Equals"; break;
                case "64": 	temp = "Explorer"; break;
                case "34":	temp = "F"; break;
                case "80":	temp = "Focus"; break;
                case "35":	temp = "G"; break;
                case "68":	temp = "Grave"; break;
                case "36":	temp = "H"; break;
                case "79":	temp = "Headset Hook"; break;
                case "3":	temp = "Home"; break;
                case "37":	temp = "I"; break;
                case "38":	temp = "J"; break;
                case "39":	temp = "K"; break;
                case "40":	temp = "L"; break;
                case "71":	temp = "Left Bracket"; break;
                case "41":	temp = "M"; break;
                case "90":	temp = "Media Fast Forward"; break;
                case "87":	temp = "Media Next"; break;
                case "85":	temp = "Media Play Pause"; break;
                case "88":	temp = "Media Previous"; break;
                case "89":	temp = "Media Rewind"; break;
                case "86":	temp = "Media Stop"; break;
                case "82": 	temp = "Menu"; break;
                case "69": 	temp = "Minus"; break;
                case "91":	temp = "Mute"; break;
                case "42":	temp = "N"; break;
                case "83":	temp = "Notification"; break;
                case "78":	temp = "Num Lock"; break;
                case "43":	temp = "O"; break;
                case "44":	temp = "P"; break;
                case "56":	temp = "Period"; break;
                case "81":	temp = "Plus"; break;
                case "18":	temp = "Pound"; break;
                case "26":	temp = "Power"; break;
                case "45":	temp = "Q"; break;
                case "46":	temp = "R"; break;
                case "72":	temp = "Right Bracket"; break;
                case "47":	temp = "S"; break;
                case "84":	temp = "Search"; break;
                case "74":	temp = "Semi-Colon"; break;
                case "59":	temp = "Left Shift"; break;
                case "60":	temp = "Right Shift"; break;
                case "76":	temp = "Slash"; break;
                case "1":	temp = "Soft Left"; break;
                case "2":	temp = "Soft Right"; break;
                case "62":	temp = "Space"; break;
                case "17":	temp = "Star"; break;
                case "63":	temp = "Sym"; break;
                case "48":	temp = "T"; break;
                case "61":	temp = "Tab"; break;
                case "49":	temp = "U"; break;
                case "50":	temp = "V"; break;
                case "25":	temp = "Volume Down"; break;
                case "24":	temp = "Volume Up"; break;
                case "51":	temp = "W"; break;
                case "52":	temp = "X"; break;
                case "53":	temp = "Y"; break;
                case "54":	temp = "Z"; break;
                case "129":	temp = "Left Control"; break;
                case "130":	temp = "Right Control"; break;
                case "131":	temp = "Escape"; break;
                case "132":	temp = "End"; break;
                case "133":	temp = "Insert"; break;
                case "92":	temp = "Page Up"; break;
                case "93":	temp = "Page Down"; break;
                case "96":	temp = "A Button"; break;
                case "97":	temp = "B Button"; break;
                case "98":	temp = "C Button"; break;
                case "99":	temp = "X Button"; break;
                case "100":	temp = "Y Button"; break;
                case "101":	temp = "Z Button"; break;
                case "102":	temp = "L1 Button"; break;
                case "103":	temp = "R1 Button"; break;
                case "104":	temp = "L2 Button"; break;
                case "105":	temp = "R2 Button"; break;
                case "106":	temp = "Left Thumb Button"; break;
                case "107":	temp = "Right Thumb Button"; break;
                case "108":	temp = "Start Button"; break;
                case "109":	temp = "Select Button"; break;
                case "110":	temp = "Button Mode"; break;
                case "144":	temp = "Numpad 0"; break;
                case "145":	temp = "Numpad 1"; break;
                case "146": temp = "Numpad 2"; break;
                case "147":	temp = "Numpad 3"; break;
                case "148":	temp = "Numpad 4"; break;
                case "149":	temp = "Numpad 5"; break;
                case "150":	temp = "Numpad 6"; break;
                case "151":	temp = "Numpad 7"; break;
                case "152": temp = "Numpad 8"; break;
                case "153": temp = "Numpad 9"; break;
                case "243":	temp = "Colon"; break;
                case "244": temp = "F1"; break;
                case "245":	temp = "F2"; break;
                case "246":	temp = "F3"; break;
                case "247":	temp = "F4"; break;
                case "248":	temp = "F5"; break;
                case "249": temp = "F6"; break;
                case "250": temp = "F7"; break;
                case "251": temp = "F8"; break;
                case "252": temp = "F9"; break;
                case "253":	temp = "F10"; break;
                case "254":	temp = "F11"; break;
                case "255":	temp = "F12"; break;

                default: temp = "Unknown"; break;
            }
            ControlsP1Trans[m] = temp;
        }
        for(int o = 0; o < ControlsP2Trans.length; o++) {
            switch(ControlsP2Trans[o]) {
                case "7":   temp = "0"; break;
                case "8":   temp = "1"; break;
                case "9":   temp = "2"; break;
                case "10":  temp = "3"; break;
                case "11":  temp = "4"; break;
                case "12":	temp = "5"; break;
                case "13":	temp = "6"; break;
                case "14":	temp = "7"; break;
                case "15":	temp = "8"; break;
                case "16":	temp = "9"; break;
                case "29":	temp = "A"; break;
                case "57":	temp = "Left Alt"; break;
                case "58": 	temp = "Right Alt"; break;
                case "75":	temp = "Apostrophe"; break;
                case "77":	temp = "AT"; break;
                case "30":	temp = "B"; break;
                case "4":	temp = "Back"; break;
                case "73":	temp = "Back Slash"; break;
                case "31":	temp = "C"; break;
                case "5":	temp = "Call"; break;
                case "27":	temp = "Camera"; break;
                case "28":	temp = "Clear"; break;
                case "55":	temp = "Comma"; break;
                case "32":	temp = "D"; break;
                case "67":	temp = "Del/Backspace"; break;
                case "112":	temp = "Forward Del"; break;
                case "23": 	temp = "Center"; break;
                case "20":	temp = "Down"; break;
                case "21":	temp = "Left"; break;
                case "22":	temp = "Right"; break;
                case "19":	temp = "Up"; break;
                case "33":	temp = "E"; break;
                case "6":	temp = "End Call"; break;
                case "66":	temp = "Enter"; break;
                case "65":	temp = "Envelope"; break;
                case "70":	temp = "Equals"; break;
                case "64": 	temp = "Explorer"; break;
                case "34":	temp = "F"; break;
                case "80":	temp = "Focus"; break;
                case "35":	temp = "G"; break;
                case "68":	temp = "Grave"; break;
                case "36":	temp = "H"; break;
                case "79":	temp = "Headset Hook"; break;
                case "3":	temp = "Home"; break;
                case "37":	temp = "I"; break;
                case "38":	temp = "J"; break;
                case "39":	temp = "K"; break;
                case "40":	temp = "L"; break;
                case "71":	temp = "Left Bracket"; break;
                case "41":	temp = "M"; break;
                case "90":	temp = "Media Fast Forward"; break;
                case "87":	temp = "Media Next"; break;
                case "85":	temp = "Media Play Pause"; break;
                case "88":	temp = "Media Previous"; break;
                case "89":	temp = "Media Rewind"; break;
                case "86":	temp = "Media Stop"; break;
                case "82": 	temp = "Menu"; break;
                case "69": 	temp = "Minus"; break;
                case "91":	temp = "Mute"; break;
                case "42":	temp = "N"; break;
                case "83":	temp = "Notification"; break;
                case "78":	temp = "Num Lock"; break;
                case "43":	temp = "O"; break;
                case "44":	temp = "P"; break;
                case "56":	temp = "Period"; break;
                case "81":	temp = "Plus"; break;
                case "18":	temp = "Pound"; break;
                case "26":	temp = "Power"; break;
                case "45":	temp = "Q"; break;
                case "46":	temp = "R"; break;
                case "72":	temp = "Right Bracket"; break;
                case "47":	temp = "S"; break;
                case "84":	temp = "Search"; break;
                case "74":	temp = "Semi-Colon"; break;
                case "59":	temp = "Left Shift"; break;
                case "60":	temp = "Right Shift"; break;
                case "76":	temp = "Slash"; break;
                case "1":	temp = "Soft Left"; break;
                case "2":	temp = "Soft Right"; break;
                case "62":	temp = "Space"; break;
                case "17":	temp = "Star"; break;
                case "63":	temp = "Sym"; break;
                case "48":	temp = "T"; break;
                case "61":	temp = "Tab"; break;
                case "49":	temp = "U"; break;
                case "50":	temp = "V"; break;
                case "25":	temp = "Volume Down"; break;
                case "24":	temp = "Volume Up"; break;
                case "51":	temp = "W"; break;
                case "52":	temp = "X"; break;
                case "53":	temp = "Y"; break;
                case "54":	temp = "Z"; break;
                case "129":	temp = "Left Control"; break;
                case "130":	temp = "Right Control"; break;
                case "131":	temp = "Escape"; break;
                case "132":	temp = "End"; break;
                case "133":	temp = "Insert"; break;
                case "92":	temp = "Page Up"; break;
                case "93":	temp = "Page Down"; break;
                case "96":	temp = "A Button"; break;
                case "97":	temp = "B Button"; break;
                case "98":	temp = "C Button"; break;
                case "99":	temp = "X Button"; break;
                case "100":	temp = "Y Button"; break;
                case "101":	temp = "Z Button"; break;
                case "102":	temp = "L1 Button"; break;
                case "103":	temp = "R1 Button"; break;
                case "104":	temp = "L2 Button"; break;
                case "105":	temp = "R2 Button"; break;
                case "106":	temp = "Left Thumb Button"; break;
                case "107":	temp = "Right Thumb Button"; break;
                case "108":	temp = "Start Button"; break;
                case "109":	temp = "Select Button"; break;
                case "110":	temp = "Button Mode"; break;
                case "144":	temp = "Numpad 0"; break;
                case "145":	temp = "Numpad 1"; break;
                case "146": temp = "Numpad 2"; break;
                case "147":	temp = "Numpad 3"; break;
                case "148":	temp = "Numpad 4"; break;
                case "149":	temp = "Numpad 5"; break;
                case "150":	temp = "Numpad 6"; break;
                case "151":	temp = "Numpad 7"; break;
                case "152": temp = "Numpad 8"; break;
                case "153": temp = "Numpad 9"; break;
                case "243":	temp = "Colon"; break;
                case "244": temp = "F1"; break;
                case "245":	temp = "F2"; break;
                case "246":	temp = "F3"; break;
                case "247":	temp = "F4"; break;
                case "248":	temp = "F5"; break;
                case "249": temp = "F6"; break;
                case "250": temp = "F7"; break;
                case "251": temp = "F8"; break;
                case "252": temp = "F9"; break;
                case "253":	temp = "F10"; break;
                case "254":	temp = "F11"; break;
                case "255":	temp = "F12"; break;

                default: temp = "Unknown"; break;
            }
            ControlsP2Trans[o] = temp;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }

}

