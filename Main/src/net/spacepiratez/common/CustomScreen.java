package net.spacepiratez.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.spacepiratez.utils.*;

import java.util.Scanner;

/**
 * Created by Guy on 09/03/14.
 */
public class CustomScreen implements Screen {
    final SpacePiratez game;
    SpriteBatch batch;
    private Stage stage;
    OrthographicCamera camera;
    public String craftName[];
    public Sprite craft[] = new Sprite[5];
    public TextureRegion craftRegion[] = new TextureRegion[5];
    public Sprite craftSprite[] = new Sprite[5];
    private float menuBackTimer = 0.0f, menuBackTimer2 = 0.0f, bgBackTimer = 0.0f;
    int mastNumP1 = 1, hullNumP1 = 1, engineNumP1 = 1, gunNumP1 = 1, shotNumP1 = 1;
    int mastNumP2 = 1, hullNumP2 = 1, engineNumP2 = 1, gunNumP2 = 1, shotNumP2 = 1;
    float mastBarX1 = 750, hullBarX1 = 750, engineBarX1 = 750, gunBarX1 = 750, shotBarX1 = 750;
    float mastBarX2 = 750, hullBarX2 = 750, engineBarX2 = 750, gunBarX2 = 750, shotBarX2 = 750;
    public String mastNumP1Save = "1", hullNumP1Save = "1", engineNumP1Save = "1", gunNumP1Save = "1", shotNumP1Save = "1";
    public String mastNumP2Save = "1", hullNumP2Save = "1", engineNumP2Save = "1", gunNumP2Save = "1", shotNumP2Save = "1";
    public boolean mastNumP1Toggle = true, hullNumP1Toggle = true, engineNumP1Toggle = true, shotNumP1Toggle = true, gunNumP1Toggle = true;
    public int mastNumP1Delta = 0, hullNumP1Delta = 0, engineNumP1Delta = 0, shotNumP1Delta = 0, gunNumP1Delta = 0;
    public boolean mastNumP2Toggle = true, hullNumP2Toggle = true, engineNumP2Toggle = true, shotNumP2Toggle = true, gunNumP2Toggle = true;
    public int mastNumP2Delta = 0, hullNumP2Delta = 0, engineNumP2Delta = 0, shotNumP2Delta = 0, gunNumP2Delta = 0;
    public boolean activeScreenP1 = true;
    public int deltaCap = 5, deltaCapDelta;
    public boolean deltaCapToggle = true;
    private Scanner scan = new Scanner(System.in);
    private String scannedText = "";
    private int transScannedText = 10;

    public CustomScreen(SpacePiratez gam) {
        this.game = gam;
        XMLHandler.readXML();

        mastNumP1Save   = XMLHandler.mastP1();
        hullNumP1Save   = XMLHandler.hullP1();
        engineNumP1Save = XMLHandler.engineP1();
        shotNumP1Save   = XMLHandler.shotP1();
        gunNumP1Save    = XMLHandler.gunP1();

        mastNumP2Save   = XMLHandler.mastP2();
        hullNumP2Save   = XMLHandler.hullP2();
        engineNumP2Save = XMLHandler.engineP2();
        shotNumP2Save   = XMLHandler.shotP2();
        gunNumP2Save    = XMLHandler.gunP2();

        mastNumP1   = Integer.valueOf(mastNumP1Save);
        hullNumP1   = Integer.valueOf(hullNumP1Save);
        engineNumP1 = Integer.valueOf(engineNumP1Save);
        shotNumP1   = Integer.valueOf(shotNumP1Save);
        gunNumP1    = Integer.valueOf(gunNumP1Save);

        mastNumP2   = Integer.valueOf(mastNumP2Save);
        hullNumP2   = Integer.valueOf(hullNumP2Save);
        engineNumP2 = Integer.valueOf(engineNumP2Save);
        shotNumP2   = Integer.valueOf(shotNumP2Save);
        gunNumP2    = Integer.valueOf(gunNumP2Save);

        craftName = new String[5];
        craftName[0] = "mast";
        craftName[1] = "hull";
        craftName[2] = "engine";
        craftName[3] = "shot";
        craftName[4] = "gun";

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

//		menuBack = new Texture(Gdx.files.internal(Reference.CUSTOM_MENU_BACK));
//		menuBack2 = new Texture(Gdx.files.internal(Reference.CUSTOM_MENU_BACK_2));
//		bgBack = new Texture(Gdx.files.internal(Reference.CUSTOM_BG_BACK));
//		overlay = new Texture(Gdx.files.internal(Reference.CUSTOM_OVERLAY));
//		slider = new Texture(Gdx.files.internal(Reference.CUSTOM_SLIDER));
//		bar = new Texture(Gdx.files.internal(Reference.CUSTOM_BAR));
//
//		menuBack.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
//		menuBack2.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
//		bgBack.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
//		overlay.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		slider.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//		bar.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//
//		menuBackRegion = new TextureRegion(menuBack, 0, 0, 4096, 512);
//		menuBackRegion2 = new TextureRegion(menuBack2, 0, 0, 2048, 512);
//		bgBackRegion = new TextureRegion(bgBack, 0, 0, 8192, 512);
//
//		menuBackSprite = new Sprite(menuBackRegion);
//		menuBackSprite.setSize(4096, 512);
//
//		menuBackSprite2 = new Sprite(menuBackRegion2);
//		menuBackSprite2.setSize(2048, 512);
//
//		bgBackSprite = new Sprite(bgBackRegion);
//		bgBackSprite.setSize(8192, 512);

        craft[0] = Preloader.shipMastSprite[0];
        craft[1] = Preloader.shipHullSprite[0];
        craft[2] = Preloader.shipEngineSprite[0];
        craft[3] = Preloader.shipShotSprite[0];
        craft[4] = Preloader.shipGunSprite[0];

    }

    public void create() {
        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();

        if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapModKey1)) if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapModKey2)) if(Gdx.input.isKeyPressed(Reference.Controls.deltaCapAlter)) {
            scannedText = scan.nextLine();
            transScannedText = Integer.parseInt(scannedText);
            if(transScannedText > 100) transScannedText = 100;
            if(transScannedText < 1)   transScannedText = 1;
            deltaCap = transScannedText;
        }

        menuBackTimer += Gdx.graphics.getDeltaTime()/12;
        menuBackTimer2 += Gdx.graphics.getDeltaTime()/10;
        bgBackTimer += Gdx.graphics.getDeltaTime()/25;
        if(menuBackTimer > 1.0f) menuBackTimer = 0.0f;
        if(menuBackTimer2 > 1.0f) menuBackTimer2 = 0.0f;
        if(bgBackTimer > 1.0f) bgBackTimer = 0.0f;

        Preloader.customSprites[0].setU(menuBackTimer);
        Preloader.customSprites[1].setU(menuBackTimer2);
        Preloader.customSprites[2].setU(bgBackTimer);
        Preloader.customSprites[0].setU2(menuBackTimer+1);
        Preloader.customSprites[1].setU2(menuBackTimer2+1);
        Preloader.customSprites[2].setU2(bgBackTimer+1);

        deltaCap = InputListener.inputNum;

        if(activeScreenP1)  keyHandleP1();
        if(!activeScreenP1) keyHandleP2();

        if(activeScreenP1) {
            double mastCount1 = Math.floor(256/(Reference.Unlocks.maxMastNumP1-1));
            double hullCount1 = Math.floor(256/(Reference.Unlocks.maxHullNumP1-1));
            double engineCount1 = Math.floor(256/(Reference.Unlocks.maxEngineNumP1-1));
            double shotCount1 = Math.floor(256/(Reference.Unlocks.maxShotNumP1-1));
            double gunCount1 = Math.floor(256/(Reference.Unlocks.maxGunNumP1-1));

            if(mastNumP1 == 1) mastBarX1 = 750;
            if(mastNumP1 == Reference.Unlocks.maxMastNumP1) mastBarX1 = 750 + 256;
            if(mastNumP1 != 1 && mastNumP1 != Reference.Unlocks.maxMastNumP1) mastBarX1 = (float)(750 + (mastCount1*(mastNumP1-1)));

            if(hullNumP1 == 1) hullBarX1 = 750;
            if(hullNumP1 == Reference.Unlocks.maxHullNumP1) hullBarX1 = 750 + 256;
            if(hullNumP1 != 1 && hullNumP1 != Reference.Unlocks.maxHullNumP1) hullBarX1 = (float)(750 + (hullCount1*(hullNumP1-1)));

            if(engineNumP1 == 1) engineBarX1 = 750;
            if(engineNumP1 == Reference.Unlocks.maxEngineNumP1) engineBarX1 = 750 + 256;
            if(engineNumP1 != 1 && engineNumP1 != Reference.Unlocks.maxEngineNumP1) engineBarX1 = (float)(750 + (engineCount1*(engineNumP1-1)));

            if(shotNumP1 == 1) shotBarX1 = 750;
            if(shotNumP1 == Reference.Unlocks.maxShotNumP1) shotBarX1 = 750 + 256;
            if(shotNumP1 != 1 && shotNumP1 != Reference.Unlocks.maxShotNumP1) shotBarX1 = (float)(750 + (shotCount1*(shotNumP1-1)));

            if(gunNumP1 == 1) gunBarX1 = 750;
            if(gunNumP1 == Reference.Unlocks.maxGunNumP1) gunBarX1 = 750 + 256;
            if(gunNumP1 != 1 && gunNumP1 != Reference.Unlocks.maxGunNumP1) gunBarX1 = (float)(750 + (gunCount1*(gunNumP1-1)));

            craft[0] = Preloader.shipMastSprite[mastNumP1-1];
            craft[1] = Preloader.shipHullSprite[hullNumP1-1];
            craft[2] = Preloader.shipEngineSprite[engineNumP1-1];
            craft[3] = Preloader.shipShotSprite[shotNumP1-1];
            craft[4] = Preloader.shipGunSprite[gunNumP1-1];

        }
        if(!activeScreenP1) {
            double mastCount2 = Math.floor(256/(Reference.Unlocks.maxMastNumP2-1));
            double hullCount2 = Math.floor(256/(Reference.Unlocks.maxHullNumP2-1));
            double engineCount2 = Math.floor(256/(Reference.Unlocks.maxEngineNumP2-1));
            double shotCount2 = Math.floor(256/(Reference.Unlocks.maxShotNumP2-1));
            double gunCount2 = Math.floor(256/(Reference.Unlocks.maxGunNumP2-1));

            if(mastNumP2 == 1) mastBarX2 = 750;
            if(mastNumP2 == Reference.Unlocks.maxMastNumP2) mastBarX2 = 750 + 256;
            if(mastNumP2 != 1 && mastNumP2 != Reference.Unlocks.maxMastNumP2) mastBarX2 = (float)(750 + (mastCount2*(mastNumP2-1)));

            if(hullNumP2 == 1) hullBarX2 = 750;
            if(hullNumP2 == Reference.Unlocks.maxHullNumP2) hullBarX2 = 750 + 256;
            if(hullNumP2 != 1 && hullNumP2 != Reference.Unlocks.maxHullNumP2) hullBarX2 = (float)(750 + (hullCount2*(hullNumP2-1)));

            if(engineNumP2 == 1) engineBarX2 = 750;
            if(engineNumP2 == Reference.Unlocks.maxEngineNumP2) engineBarX2 = 750 + 256;
            if(engineNumP2 != 1 && engineNumP2 != Reference.Unlocks.maxEngineNumP2) engineBarX2 = (float)(750 + (engineCount2*(engineNumP2-1)));

            if(shotNumP2 == 1) shotBarX2 = 750;
            if(shotNumP2 == Reference.Unlocks.maxShotNumP2) shotBarX2 = 750 + 256;
            if(shotNumP2 != 1 && shotNumP2 != Reference.Unlocks.maxShotNumP2) shotBarX2 = (float)(750 + (shotCount2*(shotNumP2-1)));

            if(gunNumP2 == 1) gunBarX2 = 750;
            if(gunNumP2 == Reference.Unlocks.maxGunNumP2) gunBarX2 = 750 + 256;
            if(gunNumP2 != 1 && gunNumP2 != Reference.Unlocks.maxGunNumP2) gunBarX2 = (float)(750 + (gunCount2*(gunNumP2-1)));

            craft[0] = Preloader.shipMastSprite[mastNumP2-1];
            craft[1] = Preloader.shipHullSprite[hullNumP2-1];
            craft[2] = Preloader.shipEngineSprite[engineNumP2-1];
            craft[3] = Preloader.shipShotSprite[shotNumP2-1];
            craft[4] = Preloader.shipGunSprite[gunNumP2-1];
        }

        if(!Preloader.audioMenuBg[0].isPlaying() && !Preloader.audioMenuBg[1].isPlaying()) {
            Preloader.audioMenuBg[Preloader.bgAudioIndex].play();
        }

        batch.begin();
        Preloader.customSprites[2].draw(batch);
        Preloader.customSprites[0].draw(batch);
        Preloader.customSprites[1].draw(batch);
//		batch.draw(overlay, 0, 0);
        Shortcuts.ColourNinePatch(batch, 0.8f, 0, 0.5f, 1, 590, -10, 120, 600);
        Shortcuts.ColourNinePatch(batch, 0.8f, 0.5f, 0, 1, -10, -10, 300, 256);
        Shortcuts.ColourNinePatch(batch, 0.8f, 0.5f, 0, 1, 707, -10, 356, 600);
        for(int y = 0; y < craft.length; y++) {
            if(y == 3) {
                craft[y].setPosition(250, 75);
                craft[y].setScale(2.5f);
                craft[y].draw(batch);
//                batch.draw(craft[y], 175, 100);
            }else{
                craft[y].setPosition(100, 100);
                craft[y].setScale(2.5f);
                craft[y].draw(batch);
//                batch.draw(craft[y], 100, 100);
            }
        }
        button(600, 450); button(650, 450);
        button(600, 350); button(650, 350);
        button(600, 250); button(650, 250);
        button(600, 150); button(650, 150);
        button(600,  50); button(650,  50);

        Preloader.customSprites[3].setPosition(750,  450); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(750,  350); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(750,  250); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(750,  150); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(750,   50); Preloader.customSprites[3].draw(batch);

        Preloader.customSprites[3].setPosition(1006, 450); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(1006, 350); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(1006, 250); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(1006, 150); Preloader.customSprites[3].draw(batch);
        Preloader.customSprites[3].setPosition(1006,  50); Preloader.customSprites[3].draw(batch);

        if(activeScreenP1) {
//            batch.draw(Preloader.customSprite[4], mastBarX1, 450);
//            batch.draw(Preloader.customSprite[4], hullBarX1, 350);
//            batch.draw(Preloader.customSprite[4], engineBarX1, 250);
//            batch.draw(Preloader.customSprite[4], shotBarX1, 150);
//            batch.draw(Preloader.customSprite[4], gunBarX1, 50);
            Preloader.customSprites[4].setPosition(mastBarX1,   450); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(hullBarX1,   350); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(engineBarX1, 250); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(shotBarX1,   150); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(gunBarX1,     50); Preloader.customSprites[4].draw(batch);
        }
        if(!activeScreenP1) {
            Preloader.customSprites[4].setPosition(mastBarX2,   450); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(hullBarX2,   350); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(engineBarX2, 250); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(shotBarX2,   150); Preloader.customSprites[4].draw(batch);
            Preloader.customSprites[4].setPosition(gunBarX2,     50); Preloader.customSprites[4].draw(batch);
        }

        batch.end();

        game.batch.begin();
        title(Color.ORANGE, 750, 500, "[Y]  Mast  [U]");
        title(Color.ORANGE, 750, 400, "[H]  Hull  [J]");
        title(Color.ORANGE, 750, 300, "[N] Engine [M]");
        title(Color.ORANGE, 750, 200, "[ I ]  Shot  [O]");
        title(Color.ORANGE, 750, 100, "[K]  Guns  [L]");
        title(Color.ORANGE, 50, 50, "[Z] Return");
        if(activeScreenP1)  title(Color.ORANGE, 900, 500, "Player [2]");
        if(!activeScreenP1) title(Color.ORANGE, 50, 500, "Player [1]");
        game.batch.end();

        if(Gdx.input.isKeyPressed(Keys.NUM_2) || Gdx.input.isKeyPressed(Keys.NUMPAD_2)) {
            activeScreenP1 = false;
        }
        if(Gdx.input.isKeyPressed(Keys.NUM_1) || Gdx.input.isKeyPressed(Keys.NUMPAD_1)) {
            activeScreenP1 = true;
        }

        if(Gdx.input.isKeyPressed(Keys.Z)) {
            for(int i = 0; i < craft.length; i++) {
                craft[i].setScale(1);
            }

            XMLHandler.UpdateXML(mastNumP1, hullNumP1, engineNumP1, shotNumP1, gunNumP1,
                    mastNumP2, hullNumP2, engineNumP2, shotNumP2, gunNumP2);
            game.setScreen(new MainMenuScreen(game));
        }

//		if(Gdx.input.isKeyPressed(Reference.deltaCapAlter)) {
//			if(deltaCapToggle) {
//				if(listener == null) Gdx.input.getTextInput(listener, "Speed of which the buttons can be pressed (default: 10 | current: " + deltaCap + ")", "");
//			}
//		}

        keyHandleMisc();
    }

    @Override
    public void resize(int width, int height) {
        batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    public void title(Color par1, float xCoord, float yCoord, String par4) {
        game.font.setColor(par1);
        game.font.draw(game.batch, par4, xCoord, yCoord);
        game.font.setColor(Color.WHITE);
    }

    public void keyHandleP1() {

        //Toggles
        buttonClickRegP1();

        //Cyclic Restriction
        if(mastNumP1 < 1)   					            mastNumP1   = Reference.Unlocks.maxMastNumP1;
        if(mastNumP1 > Reference.Unlocks.maxMastNumP1)      mastNumP1   = 1;
        if(hullNumP1 < 1)   					            hullNumP1   = Reference.Unlocks.maxHullNumP1;
        if(hullNumP1 > Reference.Unlocks.maxHullNumP1)      hullNumP1   = 1;
        if(engineNumP1 < 1) 					            engineNumP1 = Reference.Unlocks.maxEngineNumP1;
        if(engineNumP1 > Reference.Unlocks.maxEngineNumP1)  engineNumP1 = 1;
        if(shotNumP1 < 1)   					            shotNumP1   = Reference.Unlocks.maxShotNumP1;
        if(shotNumP1 > Reference.Unlocks.maxShotNumP1)      shotNumP1   = 1;
        if(gunNumP1 < 1)    					            gunNumP1    = Reference.Unlocks.maxGunNumP1;
        if(gunNumP1 > Reference.Unlocks.maxGunNumP1)        gunNumP1    = 1;

        //Timers
        if(!mastNumP1Toggle) {
            if(mastNumP1Delta > deltaCap) {
                mastNumP1Toggle = true;
                mastNumP1Delta = 0;
            }
            mastNumP1Delta++;
        }
        if(!hullNumP1Toggle) {
            if(hullNumP1Delta > deltaCap) {
                hullNumP1Toggle = true;
                hullNumP1Delta = 0;
            }
            hullNumP1Delta++;
        }
        if(!engineNumP1Toggle) {
            if(engineNumP1Delta > deltaCap) {
                engineNumP1Toggle = true;
                engineNumP1Delta = 0;
            }
            engineNumP1Delta++;
        }
        if(!shotNumP1Toggle) {
            if(shotNumP1Delta > deltaCap) {
                shotNumP1Toggle = true;
                shotNumP1Delta = 0;
            }
            shotNumP1Delta++;
        }
        if(!gunNumP1Toggle) {
            if(gunNumP1Delta > deltaCap) {
                gunNumP1Toggle = true;
                gunNumP1Delta = 0;
            }
            gunNumP1Delta++;
        }
    }

    public void keyHandleP2() {

        //Toggles
        buttonClickRegP2();

        //Cyclic Restriction
        if(mastNumP2 < 1)   					            mastNumP2   = Reference.Unlocks.maxMastNumP2;
        if(mastNumP2 > Reference.Unlocks.maxMastNumP2)      mastNumP2   = 1;
        if(hullNumP2 < 1)   					            hullNumP2   = Reference.Unlocks.maxHullNumP2;
        if(hullNumP2 > Reference.Unlocks.maxHullNumP2)      hullNumP2   = 1;
        if(engineNumP2 < 1) 					            engineNumP2 = Reference.Unlocks.maxEngineNumP2;
        if(engineNumP2 > Reference.Unlocks.maxEngineNumP2)  engineNumP2 = 1;
        if(shotNumP2 < 1)   					            shotNumP2   = Reference.Unlocks.maxShotNumP2;
        if(shotNumP2 > Reference.Unlocks.maxShotNumP2)      shotNumP2   = 1;
        if(gunNumP2 < 1)    					            gunNumP2    = Reference.Unlocks.maxGunNumP2;
        if(gunNumP2 > Reference.Unlocks.maxGunNumP2)        gunNumP2    = 1;

        //Timers
        if(!mastNumP2Toggle) {
            if(mastNumP2Delta > deltaCap) {
                mastNumP2Toggle = true;
                mastNumP2Delta = 0;
            }
            mastNumP2Delta++;
        }
        if(!hullNumP2Toggle) {
            if(hullNumP2Delta > deltaCap) {
                hullNumP2Toggle = true;
                hullNumP2Delta = 0;
            }
            hullNumP2Delta++;
        }
        if(!engineNumP2Toggle) {
            if(engineNumP2Delta > deltaCap) {
                engineNumP2Toggle = true;
                engineNumP2Delta = 0;
            }
            engineNumP2Delta++;
        }
        if(!shotNumP2Toggle) {
            if(shotNumP2Delta > deltaCap) {
                shotNumP2Toggle = true;
                shotNumP2Delta = 0;
            }
            shotNumP2Delta++;
        }
        if(!gunNumP2Toggle) {
            if(gunNumP2Delta > deltaCap) {
                gunNumP2Toggle = true;
                gunNumP2Delta = 0;
            }
            gunNumP2Delta++;
        }
    }

    public void keyHandleMisc() {
        if(!deltaCapToggle) {
            if(deltaCapDelta > deltaCap) {
                deltaCapToggle = true;
                deltaCapDelta = 0;
            }
            deltaCapDelta++;
        }
    }

    public void button(float xPos, float yPos) {
        Shortcuts.Button(batch, 0.5f, 0.5f, 0, 1, 0.8f, 0.8f, 0, 1, xPos, yPos, 48, 48);
    }

    public void buttonClickRegP1() {

        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 450 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 498) {
                if(Gdx.input.isTouched()) {
                    if(mastNumP1Toggle) mastNumP1--;
                    mastNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 450 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 498) {
                if(Gdx.input.isTouched()) {
                    if(mastNumP1Toggle) mastNumP1++;
                    mastNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 350 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 398) {
                if(Gdx.input.isTouched()) {
                    if(hullNumP1Toggle) hullNumP1--;
                    hullNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 350 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 398) {
                if(Gdx.input.isTouched()) {
                    if(hullNumP1Toggle) hullNumP1++;
                    hullNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 250 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 298) {
                if(Gdx.input.isTouched()) {
                    if(engineNumP1Toggle) engineNumP1--;
                    engineNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 250 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 298) {
                if(Gdx.input.isTouched()) {
                    if(engineNumP1Toggle) engineNumP1++;
                    engineNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 150 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 198) {
                if(Gdx.input.isTouched()) {
                    if(shotNumP1Toggle) shotNumP1--;
                    shotNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 150 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 198) {
                if(Gdx.input.isTouched()) {
                    if(shotNumP1Toggle) shotNumP1++;
                    shotNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 50 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 98) {
                if(Gdx.input.isTouched()) {
                    if(gunNumP1Toggle) gunNumP1--;
                    gunNumP1Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 50 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 98) {
                if(Gdx.input.isTouched()) {
                    if(gunNumP1Toggle) gunNumP1++;
                    gunNumP1Toggle = false;
                }
            }
        }
    }

    public void buttonClickRegP2() {

        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 450 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 498) {
                if(Gdx.input.isTouched()) {
                    if(mastNumP2Toggle) mastNumP2--;
                    mastNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 450 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 498) {
                if(Gdx.input.isTouched()) {
                    if(mastNumP2Toggle) mastNumP2++;
                    mastNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 350 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 398) {
                if(Gdx.input.isTouched()) {
                    if(hullNumP2Toggle) hullNumP2--;
                    hullNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 350 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 398) {
                if(Gdx.input.isTouched()) {
                    if(hullNumP2Toggle) hullNumP2++;
                    hullNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 250 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 298) {
                if(Gdx.input.isTouched()) {
                    if(engineNumP2Toggle) engineNumP2--;
                    engineNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 250 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 298) {
                if(Gdx.input.isTouched()) {
                    if(engineNumP2Toggle) engineNumP2++;
                    engineNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 150 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 198) {
                if(Gdx.input.isTouched()) {
                    if(shotNumP2Toggle) shotNumP2--;
                    shotNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 150 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 198) {
                if(Gdx.input.isTouched()) {
                    if(shotNumP2Toggle) shotNumP2++;
                    shotNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 600 && Gdx.input.getX() <= 648) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 50 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 98) {
                if(Gdx.input.isTouched()) {
                    if(gunNumP2Toggle) gunNumP2--;
                    gunNumP2Toggle = false;
                }
            }
        }
        if(Gdx.input.getX() >= 650 && Gdx.input.getX() <= 698) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 50 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 98) {
                if(Gdx.input.isTouched()) {
                    if(gunNumP2Toggle) gunNumP2++;
                    gunNumP2Toggle = false;
                }
            }
        }
    }
}

