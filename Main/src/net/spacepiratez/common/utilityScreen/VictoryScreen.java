package net.spacepiratez.common.utilityScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import net.spacepiratez.common.MainMenuScreen;
import net.spacepiratez.common.SpacePiratez;
import net.spacepiratez.utils.Preloader;
import net.spacepiratez.utils.Shortcuts;

/**
 * Created by Guy on 14/03/14.
 */
public class VictoryScreen implements Screen {

    final SpacePiratez game;
    OrthographicCamera camera;
    SpriteBatch batch;
    boolean isReturnSwitch = false;
    int bgAudioIndex = 0;

    public VictoryScreen(final SpacePiratez gam) {
        this.game = gam;

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

        bgAudioIndex = MathUtils.random(0, 1);
        while(bgAudioIndex == Preloader.bgAudioIndex) {
            bgAudioIndex = MathUtils.random(0, 1);
        }
        System.out.println("Index: " + Preloader.bgAudioIndex);
        Preloader.audioMenuBg[Preloader.bgAudioIndex].setLooping(true);
        System.out.println("Looping: " + Preloader.audioMenuBg[Preloader.bgAudioIndex].isLooping());
        for(int i = 0; i < Preloader.audioMenuBg.length; i++) {
            if(Preloader.audioMenuBg[i].isPlaying()) Preloader.audioMenuBg[i].stop();
            System.out.println((i+1) + " Playing: " + Preloader.audioMenuBg[i].isPlaying());
        }
        Preloader.audioMenuBg[bgAudioIndex].play();
        System.out.println("Playing: " + Preloader.audioMenuBg[bgAudioIndex].isPlaying());

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();

        batch.begin();
        Shortcuts.SpriteColour(batch, Preloader.miscStuffSprite[6], 0, 0, 1, 1, 1, 1);
        Shortcuts.SpriteColour(batch, Preloader.miscStuffSprite[7], 0, 0, 0.5f, 0, 0.8f, 1);
        Shortcuts.Button(batch, 0, 0.5f, 0.5f, 1, 0, 0.8f, 0.8f, 1, 809, 35, 180, 90);
        Shortcuts.SpriteColour(batch, Preloader.helpReturnSprite, 809, 35, 0.5f, 0, 0.8f, 1);
        batch.end();

        if(Gdx.input.getX() >= 809 && Gdx.input.getX() <= 989) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 35 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 125) {
                if(Gdx.input.isTouched()) {
                    if(!isReturnSwitch) isReturnSwitch = true;
                }
            }
        }
        if(isReturnSwitch) game.setScreen(new MainMenuScreen(game));

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
