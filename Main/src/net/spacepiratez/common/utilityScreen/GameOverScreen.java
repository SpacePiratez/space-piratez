package net.spacepiratez.common.utilityScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.spacepiratez.common.MainMenuScreen;
import net.spacepiratez.common.SpacePiratez;
import net.spacepiratez.utils.Preloader;
import net.spacepiratez.utils.Shortcuts;

/**
 * Created by Guy on 09/03/14.
 */
public class GameOverScreen implements Screen {

    private Texture gameOverImage;
    final SpacePiratez game;
    OrthographicCamera camera;
    SpriteBatch batch;
    Stage stage;

    public GameOverScreen(SpacePiratez gam) {
        this.game = gam;

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

//		gameOverImage = new Texture(Gdx.files.internal("data/images/GameOver.png"));
//		gameOverImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);

    }

    public void create() {
        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        Shortcuts.SpriteDraw(batch, Preloader.miscStuffSprite[0], 0, 0);
        batch.end();

//        if(Gdx.input.isKeyPressed(Keys.ANY_KEY) || Gdx.input.isTouched()) game.setScreen(new M    ainMenuScreen(game));
        if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

}
