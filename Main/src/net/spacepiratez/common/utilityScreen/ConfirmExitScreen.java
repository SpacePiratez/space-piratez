package net.spacepiratez.common.utilityScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.spacepiratez.common.MainMenuScreen;
import net.spacepiratez.common.SpacePiratez;
import net.spacepiratez.utils.MenuNinePatch;
import net.spacepiratez.utils.Preloader;
import net.spacepiratez.utils.Shortcuts;

/**
 * Created by Guy on 09/03/14.
 */
public class ConfirmExitScreen implements Screen {
    final SpacePiratez game;
    OrthographicCamera camera;
    SpriteBatch batch;
    Stage stage;
    private MenuNinePatch indiRed, indiYellow, indiGreen;
    private boolean confirm = false, deny = false;

    public ConfirmExitScreen(final SpacePiratez gam) {
        this.game = gam;

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        indiRed    = MenuNinePatch.getIndicatorRed();
        indiYellow = MenuNinePatch.getIndicatorYellow();
        indiGreen  = MenuNinePatch.getIndicatorGreen();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        batch = new SpriteBatch();

    }

    public void create() {
        batch = new SpriteBatch();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        float time = Gdx.graphics.getFramesPerSecond();
        String fps = "FPS: " + time;

        camera.update();

        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        Shortcuts.SpriteDraw(batch, Preloader.miscStuffSprite[2], 0, 1);
        switch(String.valueOf(MainMenuScreen.isMultiPlayer)) {
            case "true":  indiGreen.draw(batch, 125, 286, 25, 25); break;
            case "false": indiRed.draw(batch, 125, 286, 25, 25);   break;
        }
        switch(String.valueOf(MainMenuScreen.isSurvival)) {
            case "true":  indiGreen.draw(batch, 125, 261, 25, 25); break;
            case "false": indiRed.draw(batch, 125, 261, 25, 25);   break;
        }
        switch(MainMenuScreen.difficulty) {
            case 1: indiGreen.draw(batch, 125, 236, 25, 25);  break; //Easy
            case 2: indiYellow.draw(batch, 125, 236, 25, 25); break; //Normal
            case 3: indiRed.draw(batch, 125, 236, 25, 25);    break; //Hard
        }
        batch.end();

        game.batch.begin();
        title(Color.PINK, 525, 22, fps);
        game.batch.end();

        batch.begin();
        Shortcuts.SpriteDraw(batch, Preloader.miscStuffSprite[3], 0, 0);
        Shortcuts.Button(batch, 0.5f, 0, 0, 1, 0.8f, 0, 0, 1, 313, 226, 183, 90); //Confirm exit
        Shortcuts.Button(batch, 0, 0.5f, 0, 1, 0, 0.8f, 0, 1, 528, 226, 183, 90); //Deny Exit
//        Shortcuts.ColourTexF(batch, confirmTxt, 0.5f, 0, 0.8f, 1, 313, 226);
//        Shortcuts.ColourTexF(batch, denyTxt, 0.5f, 0, 0.8f, 1, 528, 226);
        Shortcuts.SpriteColour(batch, Preloader.miscStuffSprite[4], 313, 226, 0.5f, 0, 0.8f, 1);
        Shortcuts.SpriteColour(batch, Preloader.miscStuffSprite[5], 528, 226, 0.5f, 0, 0.8f, 1);
        batch.end();

        game.batch.begin();
        title(Color.MAGENTA, 400, 400, "Are you sure you wish to quit?");
//		title(Color.GREEN, 300, 300, "[Y] Yes. Quit");
//		title(Color.RED, 450, 300, "[N] No. Dont Quit");
        game.batch.end();

        if(Gdx.input.getX() >= 313 && Gdx.input.getX() <= 496) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 226 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 316) {
                if(Gdx.input.isTouched()) {
                    if(!confirm) confirm = true;
                }
            }
        }
        if(Gdx.input.getX() >= 528 && Gdx.input.getX() <= 711) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= 226 && Gdx.graphics.getHeight()-Gdx.input.getY() <= 316) {
                if(Gdx.input.isTouched()) {
                    if(!deny) deny = true;
                }
            }
        }

        if(confirm) Gdx.app.exit();
        if(deny)	game.setScreen(new MainMenuScreen(game));

//		if(Gdx.input.isKeyPressed(Keys.Y)) Gdx.app.exit();
//		if(Gdx.input.isKeyPressed(Keys.N)) game.setScreen(new MainMenuScreen(game));

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    public void title(Color par1, float xCoord, float yCoord, String par4) {
        game.font.setColor(par1);
        game.font.draw(game.batch, par4, xCoord, yCoord);
        game.font.setColor(Color.WHITE);
    }



}

