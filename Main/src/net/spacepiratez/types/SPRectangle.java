package net.spacepiratez.types;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Guy on 16/04/2014.
 */
public class SPRectangle extends Rectangle {

    public boolean isMovingUp() {
        return movingUp;
    }

    public void setMovingUp(boolean movingUp) {
        this.movingUp = movingUp;
    }

    private boolean movingUp;

    public SPRectangle() {
        super();
    }

//    public boolean getMovingUp() {
//        return this.movingUp;
//    }
//
//    public void setMovingUp(boolean movingUp) {
//        this.movingUp = movingUp;
//    }

}
