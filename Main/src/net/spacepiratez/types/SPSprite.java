package net.spacepiratez.types;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SPSprite extends Sprite {

    static final int VERTEX_SIZE = 2 + 1 + 2;
    static final int SPRITE_SIZE = 4 * VERTEX_SIZE;

    private SpriteBatch batch;
    private boolean moveUp;
    public enum Hostile {
        SHIP, FASTSHIP, STRAFESHIP, LASER, PLASMA, HEALTH, SHIELD, SPEED,
        LAZOR, TRIPLE, PRIMLASER, SECLASER, PRIMLAZORFIRE, SECLAZORFIRE
    }

    private Hostile type;

    public SPSprite(TextureRegion region) {
        super();
        setRegion(region);
        setSize(getRegionWidth(), getRegionHeight());
        setColor(1, 1, 1, 1);
        setOrigin(getRegionWidth()/2, getRegionHeight()/2);
    }

    public SPSprite(TextureRegion region, Hostile type) {
        super();
        setRegion(region);
        setSize(getRegionWidth(), getRegionHeight());
        setOrigin(getRegionWidth()/2, getRegionHeight()/2);
        switch(type) {
            case LASER:
                setColor(0.8f, 0.1f, 0.14f, 1);
                break;
            case PLASMA:
                setColor(0.14f, 0.8f, 0.1f, 1);
                break;
        }
    }

    public void draw(SpriteBatch batch) {
        batch.draw(getTexture(), getVertices(), 0, SPRITE_SIZE);
    }

    public void setMoveUp(boolean moveUp) {
        this.moveUp = moveUp;
    }

    public boolean getMoveUp() {
        return this.moveUp;
    }

    public void setType(Hostile type) {
        this.type = type;
    }

    public Hostile getType() {
        return this.type;
    }
}