package net.spacepiratez.utils;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * Created by Guy on 09/03/14.
 */
@SuppressWarnings("all")
public class XMLHandler {

    static String mastNumP1Save, hullNumP1Save, engineNumP1Save, shotNumP1Save, gunNumP1Save;
    static String mastNumP2Save, hullNumP2Save, engineNumP2Save, shotNumP2Save, gunNumP2Save;
    static String moveUpP1Save, moveDownP1Save, moveLeftP1Save, moveRightP1Save, actionShotP1Save, actionLazorP1Save;
    static String moveUpP2Save, moveDownP2Save, moveLeftP2Save, moveRightP2Save, actionShotP2Save, actionLazorP2Save;
    public static String User;
    public static String[] Names, Score, Times;
    public static float[] Posit;

    //MARK Components XML

    public static void WriteXML() {
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SpacePiratez_Components");
            doc.appendChild(rootElement);

            Element player1 = doc.createElement("Player_1");
            rootElement.appendChild(player1);

            Element player2 = doc.createElement("Player_2");
            rootElement.appendChild(player2);

            Element MastNum1   = doc.createElement("MastNum");
            Element HullNum1   = doc.createElement("HullNum");
            Element EngineNum1 = doc.createElement("EngineNum");
            Element ShotNum1   = doc.createElement("ShotNum");
            Element GunNum1    = doc.createElement("GunNum");

            MastNum1.appendChild(doc.createTextNode("1"));
            HullNum1.appendChild(doc.createTextNode("1"));
            EngineNum1.appendChild(doc.createTextNode("1"));
            ShotNum1.appendChild(doc.createTextNode("1"));
            GunNum1.appendChild(doc.createTextNode("1"));

            player1.appendChild(MastNum1);
            player1.appendChild(HullNum1);
            player1.appendChild(EngineNum1);
            player1.appendChild(ShotNum1);
            player1.appendChild(GunNum1);

            Element MastNum2   = doc.createElement("MastNum");
            Element HullNum2   = doc.createElement("HullNum");
            Element EngineNum2 = doc.createElement("EngineNum");
            Element ShotNum2   = doc.createElement("ShotNum");
            Element GunNum2    = doc.createElement("GunNum");

            MastNum2.appendChild(doc.createTextNode("1"));
            HullNum2.appendChild(doc.createTextNode("1"));
            EngineNum2.appendChild(doc.createTextNode("1"));
            ShotNum2.appendChild(doc.createTextNode("1"));
            GunNum2.appendChild(doc.createTextNode("1"));

            player2.appendChild(MastNum2);
            player2.appendChild(HullNum2);
            player2.appendChild(EngineNum2);
            player2.appendChild(ShotNum2);
            player2.appendChild(GunNum2);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(Reference.Files.FILE_COMPONENTS));
//			StreamResult result = new StreamResult(new File("/media/peter/USB20FD/bin/components.xml"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public static void UpdateXML(int par11, int par12, int par13,
                                 int par14, int par15,
                                 int par21, int par22, int par23,
                                 int par24, int par25) {

        String par11S = String.valueOf(par11);
        String par12S = String.valueOf(par12);
        String par13S = String.valueOf(par13);
        String par14S = String.valueOf(par14);
        String par15S = String.valueOf(par15);
        String par21S = String.valueOf(par21);
        String par22S = String.valueOf(par22);
        String par23S = String.valueOf(par23);
        String par24S = String.valueOf(par24);
        String par25S = String.valueOf(par25);

        try{
            DocumentBuilderFactory docFac   = DocumentBuilderFactory.newInstance();
            DocumentBuilder        docBuild = docFac.newDocumentBuilder();
            Document 			   doc      = docBuild.parse(Reference.Files.FILE_COMPONENTS);

            Node SpacePiratez_Components = doc.getFirstChild();
            Node player1 = doc.getElementsByTagName("Player_1").item(0);
            Node player2 = doc.getElementsByTagName("Player_2").item(0);

            NamedNodeMap attr1 = player1.getAttributes();
            NamedNodeMap attr2 = player2.getAttributes();

            NodeList list1 = player1.getChildNodes();
            NodeList list2 = player2.getChildNodes();

            for(int i = 0; i < list1.getLength(); i++) {
                Node node = list1.item(i);
                if("MastNum".equals(node.getNodeName()))   node.setTextContent(par11S);
                if("HullNum".equals(node.getNodeName()))   node.setTextContent(par12S);
                if("EngineNum".equals(node.getNodeName())) node.setTextContent(par13S);
                if("ShotNum".equals(node.getNodeName()))   node.setTextContent(par14S);
                if("GunNum".equals(node.getNodeName()))    node.setTextContent(par15S);
            }
            for(int i = 0; i < list2.getLength(); i++) {
                Node node = list2.item(i);
                if("MastNum".equals(node.getNodeName()))   node.setTextContent(par22S);
                if("HullNum".equals(node.getNodeName()))   node.setTextContent(par22S);
                if("EngineNum".equals(node.getNodeName())) node.setTextContent(par23S);
                if("ShotNum".equals(node.getNodeName()))   node.setTextContent(par24S);
                if("GunNum".equals(node.getNodeName()))    node.setTextContent(par25S);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(Reference.Files.FILE_COMPONENTS));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
    }

    public static void readXML() {
        try{
            File XMLFile = new File(Reference.Files.FILE_COMPONENTS);
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuild = docFac.newDocumentBuilder();
            Document doc = docBuild.parse(XMLFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root Element: " + doc.getDocumentElement().getNodeName());

            NodeList nodeList1 = doc.getElementsByTagName("Player_1");
            NodeList nodeList2 = doc.getElementsByTagName("Player_2");

            System.out.println("----------------------------------");

            for(int temp = 0; temp < nodeList1.getLength(); temp++) {
                Node node1 = nodeList1.item(temp);
                System.out.println("\nCurrent Element: " + node1.getNodeName());

                if(node1.getNodeType() == Node.ELEMENT_NODE) {
                    Element element1 = (Element) node1;

                    System.out.println("Player 1");
                    System.out.println("Mast Num:   " + element1.getElementsByTagName("MastNum").item(0).getTextContent());
                    System.out.println("Hull Num:   " + element1.getElementsByTagName("HullNum").item(0).getTextContent());
                    System.out.println("Engine Num: " + element1.getElementsByTagName("EngineNum").item(0).getTextContent());
                    System.out.println("Shot Num:   " + element1.getElementsByTagName("ShotNum").item(0).getTextContent());
                    System.out.println("Gun Num:    " + element1.getElementsByTagName("GunNum").item(0).getTextContent());
                    mastNumP1Save   = element1.getElementsByTagName("MastNum").item(0).getTextContent();
                    hullNumP1Save   = element1.getElementsByTagName("HullNum").item(0).getTextContent();
                    engineNumP1Save = element1.getElementsByTagName("EngineNum").item(0).getTextContent();
                    shotNumP1Save   = element1.getElementsByTagName("ShotNum").item(0).getTextContent();
                    gunNumP1Save    = element1.getElementsByTagName("GunNum").item(0).getTextContent();
                }
            }

            System.out.println("----------------------------------");

            for(int temp = 0; temp < nodeList2.getLength(); temp++) {
                Node node2 = nodeList2.item(temp);
                System.out.println("\nCurrent Element: " + node2.getNodeName());

                if(node2.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element) node2;

                    System.out.println("Player 2");
                    System.out.println("Mast Num:   " + element2.getElementsByTagName("MastNum").item(0).getTextContent());
                    System.out.println("Hull Num:   " + element2.getElementsByTagName("HullNum").item(0).getTextContent());
                    System.out.println("Engine Num: " + element2.getElementsByTagName("EngineNum").item(0).getTextContent());
                    System.out.println("Shot Num:   " + element2.getElementsByTagName("ShotNum").item(0).getTextContent());
                    System.out.println("Gun Num:    " + element2.getElementsByTagName("GunNum").item(0).getTextContent());

                    mastNumP2Save = element2.getElementsByTagName("MastNum").item(0).getTextContent();
                    hullNumP2Save = element2.getElementsByTagName("HullNum").item(0).getTextContent();
                    engineNumP2Save = element2.getElementsByTagName("EngineNum").item(0).getTextContent();
                    shotNumP2Save = element2.getElementsByTagName("ShotNum").item(0).getTextContent();
                    gunNumP2Save = element2.getElementsByTagName("GunNum").item(0).getTextContent();
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    //MARK controls XML

    public static void WriteXMLCont() {
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SpacePiratez_Controls");
            doc.appendChild(rootElement);

            Element player1 = doc.createElement("Player_1");
            rootElement.appendChild(player1);

            Element player2 = doc.createElement("Player_2");
            rootElement.appendChild(player2);

            Element Up1      = doc.createElement("Up");
            Element Down1    = doc.createElement("Down");
            Element Left1    = doc.createElement("Left");
            Element Right1   = doc.createElement("Right");
            Element Shot1    = doc.createElement("Shot");
            Element Lazor1   = doc.createElement("Lazor");

            Up1.appendChild(doc.createTextNode(Reference.Controls.moveUpP1+""));
            Down1.appendChild(doc.createTextNode(Reference.Controls.moveDownP1+""));
            Left1.appendChild(doc.createTextNode(Reference.Controls.moveLeftP1+""));
            Right1.appendChild(doc.createTextNode(Reference.Controls.moveRightP1+""));
            Shot1.appendChild(doc.createTextNode(Reference.Controls.actionFireP1+""));
            Lazor1.appendChild(doc.createTextNode(Reference.Controls.actionLazorP1+""));

            player1.appendChild(Up1);
            player1.appendChild(Down1);
            player1.appendChild(Left1);
            player1.appendChild(Right1);
            player1.appendChild(Shot1);
            player1.appendChild(Lazor1);

            Element Up2      = doc.createElement("Up");
            Element Down2    = doc.createElement("Down");
            Element Left2    = doc.createElement("Left");
            Element Right2   = doc.createElement("Right");
            Element Shot2    = doc.createElement("Shot");
            Element Lazor2   = doc.createElement("Lazor");

            Up2.appendChild(doc.createTextNode(Reference.Controls.moveUpP2+""));
            Down2.appendChild(doc.createTextNode(Reference.Controls.moveDownP2+""));
            Left2.appendChild(doc.createTextNode(Reference.Controls.moveLeftP2+""));
            Right2.appendChild(doc.createTextNode(Reference.Controls.moveRightP2+""));
            Shot2.appendChild(doc.createTextNode(Reference.Controls.actionFireP2+""));
            Lazor2.appendChild(doc.createTextNode(Reference.Controls.actionLazorP2+""));

            player2.appendChild(Up2);
            player2.appendChild(Down2);
            player2.appendChild(Left2);
            player2.appendChild(Right2);
            player2.appendChild(Shot2);
            player2.appendChild(Lazor2);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(System.getProperty(Reference.Files.FILE_LOC) + "/Controls.xml"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public static void UpdateXMLCont(int par11, int par12, int par13,
                                     int par14, int par15, int par16,
                                     int par21, int par22, int par23,
                                     int par24, int par25, int par26) {

        String par11S = String.valueOf(par11);
        String par12S = String.valueOf(par12);
        String par13S = String.valueOf(par13);
        String par14S = String.valueOf(par14);
        String par15S = String.valueOf(par15);
        String par16S = String.valueOf(par16);
        String par21S = String.valueOf(par21);
        String par22S = String.valueOf(par22);
        String par23S = String.valueOf(par23);
        String par24S = String.valueOf(par24);
        String par25S = String.valueOf(par25);
        String par26S = String.valueOf(par26);

        try{
            DocumentBuilderFactory docFac   = DocumentBuilderFactory.newInstance();
            DocumentBuilder        docBuild = docFac.newDocumentBuilder();
            Document 			   doc      = docBuild.parse(Reference.Files.FILE_CONTROLS);

            Node SpacePiratez_Components = doc.getFirstChild();
            Node player1 = doc.getElementsByTagName("Player_1").item(0);
            Node player2 = doc.getElementsByTagName("Player_2").item(0);

            NamedNodeMap attr1 = player1.getAttributes();
            NamedNodeMap attr2 = player2.getAttributes();

            NodeList list1 = player1.getChildNodes();
            NodeList list2 = player2.getChildNodes();

            for(int i = 0; i < list1.getLength(); i++) {
                Node node = list1.item(i);
                if("Up".equals(node.getNodeName()))   	node.setTextContent(par11S);
                if("Down".equals(node.getNodeName()))   node.setTextContent(par12S);
                if("Left".equals(node.getNodeName()))   node.setTextContent(par13S);
                if("Right".equals(node.getNodeName()))  node.setTextContent(par14S);
                if("Shot".equals(node.getNodeName()))   node.setTextContent(par15S);
                if("Lazor".equals(node.getNodeName()))	node.setTextContent(par16S);
            }
            for(int i = 0; i < list2.getLength(); i++) {
                Node node = list2.item(i);
                if("Up".equals(node.getNodeName()))   	node.setTextContent(par22S);
                if("Down".equals(node.getNodeName()))   node.setTextContent(par22S);
                if("Left".equals(node.getNodeName())) 	node.setTextContent(par23S);
                if("Right".equals(node.getNodeName()))  node.setTextContent(par24S);
                if("Shot".equals(node.getNodeName()))   node.setTextContent(par25S);
                if("Lazor".equals(node.getNodeName()))	node.setTextContent(par26S);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(Reference.Files.FILE_CONTROLS));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
    }

    public static void readXMLCont() {
        try{
            File XMLFile = new File(System.getProperty(Reference.Files.FILE_LOC) + "/Controls.xml");
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuild = docFac.newDocumentBuilder();
            Document doc = docBuild.parse(XMLFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root Element: " + doc.getDocumentElement().getNodeName());

            NodeList nodeList1 = doc.getElementsByTagName("Player_1");
            NodeList nodeList2 = doc.getElementsByTagName("Player_2");

            System.out.println("----------------------------------");

            for(int temp = 0; temp < nodeList1.getLength(); temp++) {
                Node node1 = nodeList1.item(temp);
                System.out.println("\nCurrent Element: " + node1.getNodeName());

                if(node1.getNodeType() == Node.ELEMENT_NODE) {
                    Element element1 = (Element) node1;

                    System.out.println("Player 1");
                    System.out.println("Move Up:		" + element1.getElementsByTagName("Up").item(0).getTextContent());
                    System.out.println("Move Down:  	" + element1.getElementsByTagName("Down").item(0).getTextContent());
                    System.out.println("Move Left:  	" + element1.getElementsByTagName("Left").item(0).getTextContent());
                    System.out.println("Move Right: 	" + element1.getElementsByTagName("Right").item(0).getTextContent());
                    System.out.println("Action Shot:	" + element1.getElementsByTagName("Shot").item(0).getTextContent());
                    System.out.println("Action Lazor:	" + element1.getElementsByTagName("Lazor").item(0).getTextContent());
                    moveUpP1Save   		= element1.getElementsByTagName("Up").item(0).getTextContent();
                    moveDownP1Save   	= element1.getElementsByTagName("Down").item(0).getTextContent();
                    moveLeftP1Save 		= element1.getElementsByTagName("Left").item(0).getTextContent();
                    moveRightP1Save   	= element1.getElementsByTagName("Right").item(0).getTextContent();
                    actionShotP1Save    = element1.getElementsByTagName("Shot").item(0).getTextContent();
                    actionLazorP1Save	= element1.getElementsByTagName("Lazor").item(0).getTextContent();
                }
            }

            System.out.println("----------------------------------");

            for(int temp = 0; temp < nodeList2.getLength(); temp++) {
                Node node2 = nodeList2.item(temp);
                System.out.println("\nCurrent Element: " + node2.getNodeName());

                if(node2.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element) node2;

                    System.out.println("Player 2");
                    System.out.println("Move Up:		" + element2.getElementsByTagName("Up").item(0).getTextContent());
                    System.out.println("Move Down:  	" + element2.getElementsByTagName("Down").item(0).getTextContent());
                    System.out.println("Move Left:  	" + element2.getElementsByTagName("Left").item(0).getTextContent());
                    System.out.println("Move Right: 	" + element2.getElementsByTagName("Right").item(0).getTextContent());
                    System.out.println("Action Shot:	" + element2.getElementsByTagName("Shot").item(0).getTextContent());
                    System.out.println("Action Lazor:	" + element2.getElementsByTagName("Lazor").item(0).getTextContent());
                    moveUpP2Save   		= element2.getElementsByTagName("Up").item(0).getTextContent();
                    moveDownP2Save   	= element2.getElementsByTagName("Down").item(0).getTextContent();
                    moveLeftP2Save 		= element2.getElementsByTagName("Left").item(0).getTextContent();
                    moveRightP2Save   	= element2.getElementsByTagName("Right").item(0).getTextContent();
                    actionShotP2Save    = element2.getElementsByTagName("Shot").item(0).getTextContent();
                    actionLazorP2Save	= element2.getElementsByTagName("Lazor").item(0).getTextContent();
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void InitLeaderboard() {
        Names = new String[10];
        Score = new String[10];
        Times = new String[10];
        Posit = new float[10];

        for(int i = 0; i < 10; i++) {
            Names[i] = "";
            Score[i] = "";
            Times[i] = "";
            Posit[i] = 0;
        }

    }

    public static void WriteLeaderboard() {
        try{
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFac.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SpacePiratez_Leaderboards");
            doc.appendChild(rootElement);

            Element slotOne     = doc.createElement("Slot_1");
            Element slotTwo     = doc.createElement("Slot_2");
            Element slotThree   = doc.createElement("Slot_3");
            Element slotFour    = doc.createElement("Slot_4");
            Element slotFive    = doc.createElement("Slot_5");
            Element slotSix     = doc.createElement("Slot_6");
            Element slotSeven   = doc.createElement("Slot_7");
            Element slotEight   = doc.createElement("Slot_8");
            Element slotNine    = doc.createElement("Slot_9");
            Element slotTen     = doc.createElement("Slot_10");

            Attr[] attrName = new Attr[10], attrTime = new Attr[10], attrScore = new Attr[10], attrPosit = new Attr[10];

            for(int i = 0; i < 10; i++) {
                attrName[i]  = doc.createAttribute("Name");
                attrTime[i]  = doc.createAttribute("Time");
                attrScore[i] = doc.createAttribute("Score");
                attrPosit[i] = doc.createAttribute("Position");
            }

//            Attr attrName  = doc.createAttribute("Name");
//            Attr attrTime  = doc.createAttribute("Time");
//            Attr attrScore = doc.createAttribute("Score");
            
            rootElement.appendChild(slotOne);
            rootElement.appendChild(slotTwo);
            rootElement.appendChild(slotThree);
            rootElement.appendChild(slotFour);
            rootElement.appendChild(slotFive);
            rootElement.appendChild(slotSix);
            rootElement.appendChild(slotSeven);
            rootElement.appendChild(slotEight);
            rootElement.appendChild(slotNine);
            rootElement.appendChild(slotTen);

            slotOne.setAttributeNode(attrName[0]);
            slotTwo.setAttributeNode(attrName[1]);
            slotThree.setAttributeNode(attrName[2]);
            slotFour.setAttributeNode(attrName[3]);
            slotFive.setAttributeNode(attrName[4]);
            slotSix.setAttributeNode(attrName[5]);
            slotSeven.setAttributeNode(attrName[6]);
            slotEight.setAttributeNode(attrName[7]);
            slotNine.setAttributeNode(attrName[8]);
            slotTen.setAttributeNode(attrName[9]);

            slotOne.setAttributeNode(attrTime[0]);
            slotTwo.setAttributeNode(attrTime[1]);
            slotThree.setAttributeNode(attrTime[2]);
            slotFour.setAttributeNode(attrTime[3]);
            slotFive.setAttributeNode(attrTime[4]);
            slotSix.setAttributeNode(attrTime[5]);
            slotSeven.setAttributeNode(attrTime[6]);
            slotEight.setAttributeNode(attrTime[7]);
            slotNine.setAttributeNode(attrTime[8]);
            slotTen.setAttributeNode(attrTime[9]);

            slotOne.setAttributeNode(attrScore[0]);
            slotTwo.setAttributeNode(attrScore[1]);
            slotThree.setAttributeNode(attrScore[2]);
            slotFour.setAttributeNode(attrScore[3]);
            slotFive.setAttributeNode(attrScore[4]);
            slotSix.setAttributeNode(attrScore[5]);
            slotSeven.setAttributeNode(attrScore[6]);
            slotEight.setAttributeNode(attrScore[7]);
            slotNine.setAttributeNode(attrScore[8]);
            slotTen.setAttributeNode(attrScore[9]);

            slotOne.setAttributeNode(attrPosit[0]);
            slotTwo.setAttributeNode(attrPosit[1]);
            slotThree.setAttributeNode(attrPosit[2]);
            slotFour.setAttributeNode(attrPosit[3]);
            slotFive.setAttributeNode(attrPosit[4]);
            slotSix.setAttributeNode(attrPosit[5]);
            slotSeven.setAttributeNode(attrPosit[6]);
            slotEight.setAttributeNode(attrPosit[7]);
            slotNine.setAttributeNode(attrPosit[8]);
            slotTen.setAttributeNode(attrPosit[9]);

            TransformerFactory transformFac = TransformerFactory.newInstance();
            Transformer transformer = transformFac.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(Reference.Files.FILE_LEADERBOARD));

            transformer.transform(source, result);
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public static void UpdateLeaderboard() {
        try{
            DocumentBuilderFactory docFac   = DocumentBuilderFactory.newInstance();
            DocumentBuilder        docBuild = docFac.newDocumentBuilder();
            Document 			   doc      = docBuild.parse(Reference.Files.FILE_LEADERBOARD);

            Node spLead = doc.getFirstChild();
            Node[] slots = new Node[10];
            NamedNodeMap[] attrs = new NamedNodeMap[10];

            for(int p = 0; p < 10; p++) {
                slots[p] = doc.getElementsByTagName("Slot_"+(p+1)).item(0);
                attrs[p] = slots[p].getAttributes();

                attrs[p].item(0).setTextContent(Names[p]);
                attrs[p].item(2).setTextContent(Score[p]);
                attrs[p].item(3).setTextContent(Times[p]);
                attrs[p].item(1).setTextContent(Posit[p]+"");
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(Reference.Files.FILE_LEADERBOARD));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }

    }

    public static void ReadLeaderboard() {
        try{
            DocumentBuilderFactory  docFac   = DocumentBuilderFactory.newInstance();
            DocumentBuilder         docBuild = docFac.newDocumentBuilder();
            Document                doc      = docBuild.parse(Reference.Files.FILE_LEADERBOARD);

            Node spLead = doc.getFirstChild();
            Node[] slots = new Node[10];
            NamedNodeMap[] attrs = new NamedNodeMap[10];

            for(int l = 0; l < 10; l++) {
                slots[l] = doc.getElementsByTagName("Slot_"+(l+1)).item(0);
                attrs[l] = slots[l].getAttributes();

                Names[l] = attrs[l].item(0).getTextContent();
                Score[l] = attrs[l].item(2).getTextContent();
                Times[l] = attrs[l].item(3).getTextContent();
                Posit[l] = Float.parseFloat(attrs[l].item(1).getTextContent());

//                System.out.println("Name " + l + ": " + Names[l]);
//                System.out.println("Score " + l + ": " + Score[l]);
//                System.out.println("Time " + l + ": " + Times[l]);
//                System.out.println("Position " + l + ": " + Posit[l]);
                System.out.println(Colours.ANSI_BLUE+"Index "+l+": "+"Name: "+Names[l]+"\t\t|\tScore: "+Score[l]+"\t\t|\tTime: "+Times[l]+"\t\t|\tPosition: "+Posit[l]);
            }
            System.out.println(Colours.ANSI_CLEAR);

        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }catch(SAXException sae) {
            sae.printStackTrace();
        }
    }

    //Getters/Setters
    //MARK Component Setters
    public static String mastP1() { String mastP1 = mastNumP1Save; return mastP1; }
    public static String hullP1() { String hullP1 = hullNumP1Save; return hullP1; }
    public static String engineP1() { String engineP1 = engineNumP1Save; return engineP1; }
    public static String shotP1() { String shotP1 = shotNumP1Save; return shotP1; }
    public static String gunP1() { String gunP1 = gunNumP1Save; return gunP1; }
    public static String mastP2() { String mastP2 = mastNumP2Save; return mastP2; }
    public static String hullP2() { String hullP2 = hullNumP2Save; return hullP2; }
    public static String engineP2() { String engineP2 = engineNumP2Save; return engineP2; }
    public static String shotP2() { String shotP2 = shotNumP2Save; return shotP2; }
    public static String gunP2() { String gunP2 = gunNumP2Save; return gunP2; }
    //MARK Control Setters
    public static String moveUpP1() { String moveUpP1 = moveUpP1Save; return moveUpP1; }
    public static String moveDownP1() { String moveDownP1 = moveDownP1Save; return moveDownP1; }
    public static String moveLeftP1() { String moveLeftP1 = moveLeftP1Save; return moveLeftP1; }
    public static String moveRightP1() { String moveRightP1 = moveRightP1Save; return moveRightP1; }
    public static String actionShotP1() { String actionShotP1 = actionShotP1Save; return actionShotP1; }
    public static String actionLazorP1() { String actionLazorP1 = actionLazorP1Save; return actionLazorP1; }
    public static String moveUpP2() { String moveUpP2 = moveUpP2Save; return moveUpP2; }
    public static String moveDownP2() { String moveDownP2 = moveDownP2Save; return moveDownP2; }
    public static String moveLeftP2() { String moveLeftP2 = moveLeftP2Save; return moveLeftP2; }
    public static String moveRightP2() { String moveRightP2 = moveRightP2Save; return moveRightP2; }
    public static String actionShotP2() { String actionShotP2 = actionShotP2Save; return actionShotP2; }
    public static String actionLazorP2() { String actionLazorP2 = actionLazorP2Save; return actionLazorP2; }

}
