package net.spacepiratez.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.spacepiratez.handlers.AttributeHandler;
import net.spacepiratez.types.SPSprite;

import java.io.File;

/**
 * Created by Guy on 09/03/14.
 */
public class Preloader {
    //Other
    public static String[] names, scores, times;
    public static int bgAudioIndex;
    public static int survivalScore, survivalTime;

    //Audio
    public static Music[] audioMenuBg;
    public static Sound[] audioImpact;
    public static Sound[] audioImpactShield;

    //Textures
    //Main Menu
    public static Texture[] mainTitleTextures;// mainTitleTex, mainPlayImage, mainTitleImage, mainPlayerText, mainSurvivalText, mainHelpText, mainDiffText, mainCustomText;
    public static TextureRegion[] mainTitleTexturesReg;
    public static Sprite[] mainTitleSprite;
    //Custom Screen
    public static Texture[] customTextures; //customMenuBack, customMenuBack2, customBgBack, customSlider, customBar;
    public static TextureRegion[] customRegions; //customMenuBackRegion, customMenuBack2Region, customBgBackRegion, customSliderRegion, customBarRegion;
    public static Sprite[] customSprites; //customMenuBackSprite, customMenuBack2Sprite, customBgBackSprite, customSliderSprite, customBarSprite;
    //Game Screen
    public static Texture[] gameTextures;
    public static TextureRegion[] gameRegions;
    public static SPSprite[] gameSprite;
    //Help Screen
    public static Texture helpReturnTex;
    public static TextureRegion helpReturnRegion;
    public static Sprite helpReturnSprite;
    //Particles
    public static ParticleEffect partLazorEffect1, partLazorEffect2;
    //Files
    public static File xmlComp, xmlCont, xmlLead;
    //Components
    public static Texture[] shipMast, shipHull, shipEngine, shipShot, shipGun;
    public static TextureRegion[] shipMastReg, shipHullReg, shipEngineReg, shipShotReg, shipGunReg;
    public static Sprite[] shipMastSprite, shipHullSprite, shipEngineSprite, shipShotSprite, shipGunSprite;
    //Misc
    public static Texture[] miscStuffTextures;
    public static TextureRegion[] miscStuffRegions;
    public static Sprite[] miscStuffSprite;
    //Shapes
    public static Texture[] shapeTextures;
    public static TextureRegion[] shapeRegion;
    public static Sprite[] shapeSprite;
    //Bosses
    public static Texture[] BossTextures;
    public static TextureRegion[] BossRegions;
    public static Sprite[] BossSprites;
    public static Music[] BossMusic;
//    public static Sound[] BossSound;

    public static void loadAssets() {
        System.out.println("Loading Assets");

        System.out.println("Initializing attributes");
        AttributeHandler.AttributeSetters();
        System.out.println("Attributes Initialized");

        System.out.println("Loading Audio");
        try{
            loadAudio();
        }catch (Exception a) {
            System.out.println("Error while loading Audio");
            System.out.println(a.getStackTrace());
            System.out.println(a);
        }finally{
            System.out.println("Audio Loaded");
        }

        System.out.println("Loading Textures");
        try{
            loadTextures();
        }catch (Exception s) {
            System.out.println("Error while loading Textures");
            System.out.println(s.getStackTrace());
            System.out.println(s);
        }finally{
            System.out.println("Textures Loaded");
        }

        System.out.println("Loading Particles");
        try{
            loadParticles();
        }catch (Exception d) {
            System.out.println("Error while loading particles");
            System.out.println(d.getStackTrace());
            System.out.println(d);
        }finally{
            System.out.println("Particles Loaded");
        }

        System.out.println("Loading Files");
        try{
            loadFiles();
        }catch (Exception f) {
            System.out.println("Error while loading files");
            System.out.println(f.getStackTrace());
            System.out.println(f);
        }finally{
            System.out.println("Files Loaded");
        }

        System.out.println("Loading Components");
        try{
            loadComponents();
        }catch (Exception g) {
            System.out.println("Error while loading Components");
            System.out.println(g.getStackTrace());
            System.out.println(g);
        }finally{
            System.out.println("Components Loaded");
        }

        System.out.println("Loading Boss Assets");
        try{
            loadBosses();
        }catch (Exception h) {
            System.out.println("Error while Loading Boss Assets");
            System.out.println(h.getStackTrace());
            System.out.println(h);
        }finally{
            System.out.println("Boss Assets Loaded");
        }

        System.out.println("Loading Leaderboards");
        try{
            loadLeaderboard();
        }catch(Exception i) {
            System.out.println("Error while Loading Leaderboards");
            System.out.println(i.getStackTrace());
            System.out.println(i);
        }finally{
            System.out.println("Leaderboards Loaded");
        }

        System.out.println("Assets Loaded");
    }

    public static void loadAudio() {
        audioMenuBg             = new Music[4];
        audioImpact 			= new Sound[Reference.IMPACT_SOUNDS+1];
        audioImpactShield       = new Sound[1];

        audioMenuBg[0]          = Gdx.audio.newMusic(Gdx.files.internal(Reference.Files.AUDIO_MENU_BG_1));
        audioMenuBg[1]          = Gdx.audio.newMusic(Gdx.files.internal(Reference.Files.AUDIO_MENU_BG_2));
        audioMenuBg[2]          = Gdx.audio.newMusic(Gdx.files.internal(Reference.Files.AUDIO_GAME_BG_1));
        audioMenuBg[3]          = Gdx.audio.newMusic(Gdx.files.internal(Reference.Files.AUDIO_GAME_BG_2));

        audioImpact[0] 			= Gdx.audio.newSound(Gdx.files.internal(Reference.Files.AUDIO_IMPACT_1));
        audioImpact[1]			= Gdx.audio.newSound(Gdx.files.internal(Reference.Files.AUDIO_IMPACT_2));
        audioImpact[2]			= Gdx.audio.newSound(Gdx.files.internal(Reference.Files.AUDIO_IMPACT_3));
        audioImpact[3] 			= Gdx.audio.newSound(Gdx.files.internal(Reference.Files.AUDIO_IMPACT_4));

        audioImpactShield[0]    = Gdx.audio.newSound(Gdx.files.internal(Reference.Files.AUDIO_IMPACT_SHIELD_1));
    }

    public static void loadTextures() {
        //Main Screen
        mainTitleTextures       = new Texture[8];
        mainTitleTexturesReg    = new TextureRegion[mainTitleTextures.length];
        mainTitleSprite         = new Sprite[mainTitleTextures.length];

        mainTitleTextures[0]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_TITLE_IMAGE));
        mainTitleTextures[1]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_PLAY_IMAGE));
        mainTitleTextures[2]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_TITLE_TEX));
        mainTitleTextures[3]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_PLAYER_TEXT));
        mainTitleTextures[4]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_SURVIVAL_TEXT));
        mainTitleTextures[5]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_HELP_TEXT));
        mainTitleTextures[6]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_DIFF_TEXT));
        mainTitleTextures[7]    = new Texture(Gdx.files.internal(Reference.Files.MAIN_CUSTOM_TEXT));

        for(int main = 0; main < mainTitleTextures.length; main++) {
            mainTitleTextures[main].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            mainTitleTexturesReg[main] = new TextureRegion(mainTitleTextures[main]);
            mainTitleSprite[main] = new Sprite(mainTitleTexturesReg[main]);
        }

        //Custom Screen
        customTextures          = new Texture[5];
        customRegions           = new TextureRegion[customTextures.length];
        customSprites           = new Sprite[customTextures.length];
        customTextures[0]		= new Texture(Gdx.files.internal(Reference.Files.CUSTOM_MENU_BACK));
        customTextures[1]		= new Texture(Gdx.files.internal(Reference.Files.CUSTOM_MENU_BACK_2));
        customTextures[2]		= new Texture(Gdx.files.internal(Reference.Files.CUSTOM_BG_BACK));
        customTextures[3]		= new Texture(Gdx.files.internal(Reference.Files.CUSTOM_SLIDER));
        customTextures[4]		= new Texture(Gdx.files.internal(Reference.Files.CUSTOM_BAR));

        customTextures[0].setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        customTextures[1].setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        customTextures[2].setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        for(int custom = 0; custom < customTextures.length; custom++) {
            customTextures[custom].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            customRegions[custom]=new TextureRegion(customTextures[custom]);
            customSprites[custom]=new Sprite(customRegions[custom]);
        }

        customSprites[0].setSize(4096, 512);
        customSprites[1].setSize(2048, 512);
        customSprites[2].setSize(8192, 512);

        //Game Screen
        gameTextures            = new Texture[13];
        gameRegions             = new TextureRegion[gameTextures.length];
        gameSprite              = new SPSprite[gameTextures.length];

        gameTextures[0] 		= new Texture(Gdx.files.internal(Reference.Files.GAME_BG_IMAGE_SCROLL));
        gameTextures[1]			= new Texture(Gdx.files.internal(Reference.Files.GAME_BG_IMAGE));
        gameTextures[2]			= new Texture(Gdx.files.internal(Reference.Files.GAME_DEATH_IMAGE));
        gameTextures[3]     	= new Texture(Gdx.files.internal(Reference.Files.GAME_SHIELD_CRAFT_IMAGE));
        gameTextures[4]			= new Texture(Gdx.files.internal(Reference.Files.GAME_LASER_IMAGE));
        gameTextures[5]			= new Texture(Gdx.files.internal(Reference.Files.GAME_LASER_IMAGE));
        gameTextures[6]			= new Texture(Gdx.files.internal(Reference.Files.GAME_SHIP_IMAGE));
        gameTextures[7] 		= new Texture(Gdx.files.internal(Reference.Files.GAME_FAST_SHIP_IMAGE));
        gameTextures[8]			= new Texture(Gdx.files.internal(Reference.Files.GAME_HEALTH_IMAGE));
        gameTextures[9]			= new Texture(Gdx.files.internal(Reference.Files.HELP_SPEED_IMAGE_LARGE));
        gameTextures[10]		= new Texture(Gdx.files.internal(Reference.Files.HELP_SHIELD_IMAGE_LARGE));
        gameTextures[11]		= new Texture(Gdx.files.internal(Reference.Files.HELP_TRI_IMAGE_LARGE));
        gameTextures[12]		= new Texture(Gdx.files.internal(Reference.Files.HELP_LAZOR_IMAGE_LARGE));

        gameTextures[0].setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        for(int game = 0; game < gameTextures.length; game++) {
            gameTextures[game].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            gameRegions[game]   = new TextureRegion(gameTextures[game]);
//            gameSprite[game]    = new SPSprite(gameRegions[game]);
        }
        gameSprite[4]  = new SPSprite(gameRegions[4],  SPSprite.Hostile.LASER);
        gameSprite[5]  = new SPSprite(gameRegions[5],  SPSprite.Hostile.PLASMA);
        gameSprite[6]  = new SPSprite(gameRegions[6],  SPSprite.Hostile.SHIP);
        gameSprite[7]  = new SPSprite(gameRegions[7],  SPSprite.Hostile.FASTSHIP);
        gameSprite[8]  = new SPSprite(gameRegions[8],  SPSprite.Hostile.HEALTH);
        gameSprite[9]  = new SPSprite(gameRegions[9],  SPSprite.Hostile.SPEED);
        gameSprite[10] = new SPSprite(gameRegions[10], SPSprite.Hostile.SHIELD);
        gameSprite[11] = new SPSprite(gameRegions[11], SPSprite.Hostile.TRIPLE);
        gameSprite[12] = new SPSprite(gameRegions[12], SPSprite.Hostile.LAZOR);
        for(int game = 0; game < gameTextures.length; game++) {
            if(gameSprite[game] == null) {
                System.out.println("Preloader.loadTextures  index: "+game);
                gameSprite[game] = new SPSprite(gameRegions[game]);
            }
        }

        //Help Screen
        helpReturnTex			= new Texture(Gdx.files.internal(Reference.Files.HELP_RETURN_TEXT));
        helpReturnRegion        = new TextureRegion(helpReturnTex);
        helpReturnSprite        = new Sprite(helpReturnRegion);
        //Misc
        miscStuffTextures       = new Texture[8];
        miscStuffRegions        = new TextureRegion[miscStuffTextures.length];
        miscStuffSprite         = new Sprite[miscStuffTextures.length];
        miscStuffTextures[0]	= new Texture(Gdx.files.internal(Reference.Files.MISC_GAME_OVER));
        miscStuffTextures[1]    = new Texture(Gdx.files.internal(Reference.Files.MISC_CLOUD_BAR_LARGE));
        miscStuffTextures[2]    = new Texture(Gdx.files.internal(Reference.Files.MISC_EXIT_BG_IMAGE));
        miscStuffTextures[3]    = new Texture(Gdx.files.internal(Reference.Files.MISC_EXIT_BG_OVERLAY));
        miscStuffTextures[4]    = new Texture(Gdx.files.internal(Reference.Files.MISC_EXIT_CONFIRM_TXT));
        miscStuffTextures[5]    = new Texture(Gdx.files.internal(Reference.Files.MISC_EXIT_DENY_TXT));
        miscStuffTextures[6]    = new Texture(Gdx.files.internal(Reference.Files.MISC_VICTORY_BG));
        miscStuffTextures[7]    = new Texture(Gdx.files.internal(Reference.Files.MISC_VICTORY_TXT));

        for(int misc = 0; misc < miscStuffTextures.length; misc++) {
            miscStuffTextures[misc].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            miscStuffRegions[misc]  = new TextureRegion(miscStuffTextures[misc]);
            miscStuffSprite[misc]   = new Sprite(miscStuffRegions[misc]);
        }
        //Shapes
        shapeTextures           = new Texture[3];
        shapeRegion             = new TextureRegion[shapeTextures.length];
        shapeSprite             = new Sprite[shapeTextures.length];
        shapeTextures[0]        = new Texture(Gdx.files.internal(Reference.Files.SHAPE_LINE_PIX));
        shapeTextures[1]        = new Texture(Gdx.files.internal(Reference.Files.SHAPE_CIRCLE_PIX));
        shapeTextures[2]        = new Texture(Gdx.files.internal(Reference.Files.SHAPE_RING_PIX));
        for(int shape = 0; shape < shapeTextures.length; shape++) {
            shapeTextures[shape].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shapeRegion[shape]  = new TextureRegion(shapeTextures[shape]);
            shapeSprite[shape]  = new Sprite(shapeRegion[shape]);
        }
    }

    public static void loadParticles() {
        partLazorEffect1 		= new ParticleEffect();
        partLazorEffect2 		= new ParticleEffect();

        partLazorEffect1.load(Gdx.files.internal(Reference.Files.PART_LOG_LAZOR_EFFECT), Gdx.files.internal(Reference.Files.PART_TEX_LAZOR_EFFECT));
        partLazorEffect2.load(Gdx.files.internal(Reference.Files.PART_LOG_LAZOR_EFFECT), Gdx.files.internal(Reference.Files.PART_TEX_LAZOR_EFFECT));
    }

    public static void loadFiles() {
        xmlComp 				= new File(Reference.Files.FILE_COMPONENTS);
        xmlCont 				= new File(Reference.Files.FILE_CONTROLS);
        xmlLead                 = new File(Reference.Files.FILE_LEADERBOARD);

        if(!xmlComp.exists()) { XMLHandler.WriteXML(); 		    }else{ System.out.println(xmlComp + " already exists"); }
        if(!xmlCont.exists()) { XMLHandler.WriteXMLCont(); 	    }else{ System.out.println(xmlCont + " already exists"); }
        if(!xmlLead.exists()) { XMLHandler.WriteLeaderboard();  }else{ System.out.println(xmlLead + " already exists"); }
    }

    public static void loadComponents() {
        shipMast 				= new Texture[Reference.Unlocks.totalPossiblePartsMast];
        shipHull 				= new Texture[Reference.Unlocks.totalPossiblePartsHull];
        shipEngine	 			= new Texture[Reference.Unlocks.totalPossiblePartsEngine];
        shipShot 				= new Texture[Reference.Unlocks.totalPossiblePartsShot];
        shipGun 				= new Texture[Reference.Unlocks.totalPossiblePartsGun];

        shipMastReg             = new TextureRegion[Reference.Unlocks.totalPossiblePartsMast];
        shipHullReg             = new TextureRegion[Reference.Unlocks.totalPossiblePartsHull];
        shipEngineReg           = new TextureRegion[Reference.Unlocks.totalPossiblePartsEngine];
        shipShotReg             = new TextureRegion[Reference.Unlocks.totalPossiblePartsShot];
        shipGunReg              = new TextureRegion[Reference.Unlocks.totalPossiblePartsGun];

        shipMastSprite          = new Sprite[Reference.Unlocks.totalPossiblePartsMast];
        shipHullSprite          = new Sprite[Reference.Unlocks.totalPossiblePartsHull];
        shipEngineSprite        = new Sprite[Reference.Unlocks.totalPossiblePartsEngine];
        shipShotSprite          = new Sprite[Reference.Unlocks.totalPossiblePartsShot];
        shipGunSprite           = new Sprite[Reference.Unlocks.totalPossiblePartsGun];

        for(int q = 0; q < shipMast.length; q++) {
            shipMast[q]			= new Texture(Gdx.files.internal(Reference.Files.CRAFT_MAST_LOC + "mast" + (q+1) + ".png"));
            shipMast[q].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shipMastReg[q]      = new TextureRegion(shipMast[q]);
            shipMastSprite[q]   = new Sprite(shipMastReg[q]);
        }
        for(int w = 0; w < shipHull.length; w++) {
            shipHull[w] 		= new Texture(Gdx.files.internal(Reference.Files.CRAFT_HULL_LOC + "hull" + (w+1) + ".png"));
            shipHull[w].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shipHullReg[w]      = new TextureRegion(shipHull[w]);
            shipHullSprite[w]   = new Sprite(shipHullReg[w]);
        }
        for(int e = 0; e < shipEngine.length; e++)	{
            shipEngine[e]	 	= new Texture(Gdx.files.internal(Reference.Files.CRAFT_ENGINE_LOC + "engine" + (e+1) + ".png"));
            shipEngine[e].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shipEngineReg[e]    = new TextureRegion(shipEngine[e]);
            shipEngineSprite[e] = new Sprite(shipEngineReg[e]);
        }
        for(int r = 0; r < shipShot.length; r++) {
            shipShot[r]			= new Texture(Gdx.files.internal(Reference.Files.CRAFT_SHOT_LOC + "shot" + (r+1) + ".png"));
            shipShot[r].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shipShotReg[r]      = new TextureRegion(shipShot[r]);
            shipShotSprite[r]   = new Sprite(shipShotReg[r]);
        }
        for(int t = 0; t < shipGun.length; t++) {
            shipGun[t] 			= new Texture(Gdx.files.internal(Reference.Files.CRAFT_GUN_LOC + "gun" + (t+1) + ".png"));
            shipGun[t].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            shipGunReg[t]       = new TextureRegion(shipGun[t]);
            shipGunSprite[t]    = new Sprite(shipGunReg[t]);
        }
    }

    public static void loadBosses() {
        BossTextures            = new Texture[1];
        BossRegions             = new TextureRegion[BossTextures.length];
        BossSprites             = new Sprite[BossTextures.length];
        BossMusic               = new Music[1];

        BossTextures[0]         = new Texture(Gdx.files.internal(Reference.Files.BOSS_LEVEL_ONE));

        BossMusic[0]            = Gdx.audio.newMusic(Gdx.files.internal(Reference.Files.AUDIO_BOSS_LVL_ONE));

        for(int boss = 0; boss < BossTextures.length; boss++) {
            BossTextures[boss].setFilter(TextureFilter.Linear, TextureFilter.Linear);
            BossRegions[boss]   = new TextureRegion(BossTextures[boss]);
            BossSprites[boss]   = new Sprite(BossRegions[boss]);
        }
    }

    public static void loadLeaderboard() {
        XMLHandler.Names        = new String[10];
        XMLHandler.Score        = new String[10];
        XMLHandler.Times        = new String[10];
        XMLHandler.Posit        = new float[10];

        for(int board = 0; board < 10; board++) {
            XMLHandler.Names[board] = "";
            XMLHandler.Score[board] = "";
            XMLHandler.Times[board] = "";
            XMLHandler.Posit[board] = 0;
        }

        xmlLead = new File(Reference.Files.FILE_LEADERBOARD);

        if(xmlLead.exists()) {
            System.out.println(xmlLead + " already exists");
            XMLHandler.ReadLeaderboard();
        }else{
            System.out.println(xmlLead + " does not exist, creating...");
            XMLHandler.WriteLeaderboard();
            System.out.println("Leaderboard file created at " + System.getProperty(Reference.Files.FILE_LOC));
        }
    }
}

