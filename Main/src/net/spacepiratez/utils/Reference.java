package net.spacepiratez.utils;

import com.badlogic.gdx.Input.Keys;

/**
 * Created by Guy on 09/03/14.
 */

public class Reference {

    public static final int IMPACT_SOUNDS = 3; //Total available sounds - 1

    public static class Files {
        public static final String FILE_LOC                 = "user.dir";
        public static final String ROOT_ASSET_LOC           = "data";
        public static final String AUDIO_LOC                = ROOT_ASSET_LOC + "/audio/";
        public static final String CRAFT_MAST_LOC           = ROOT_ASSET_LOC + "/sprites/craft/parts/mast/";
        public static final String CRAFT_HULL_LOC           = ROOT_ASSET_LOC + "/sprites/craft/parts/hull/";
        public static final String CRAFT_ENGINE_LOC         = ROOT_ASSET_LOC + "/sprites/craft/parts/engine/";
        public static final String CRAFT_SHOT_LOC           = ROOT_ASSET_LOC + "/sprites/craft/parts/shot/";
        public static final String CRAFT_GUN_LOC            = ROOT_ASSET_LOC + "/sprites/craft/parts/gun/";
        public static final String MODEL_LOC                = ROOT_ASSET_LOC + "/models/";
        public static final String BOSS_LOC                 = ROOT_ASSET_LOC + "/sprites/Boss/";

        //Assets
        //Audio
        public static final String AUDIO_MENU_BG_1          = AUDIO_LOC + "/music/bgAudioMenu.wav";
        public static final String AUDIO_MENU_BG_2          = AUDIO_LOC + "/music/Clean Soul.mp3";
        public static final String AUDIO_GAME_BG_1          = AUDIO_LOC + "/music/bgAudioL1.mp3";
        public static final String AUDIO_GAME_BG_2          = AUDIO_LOC + "/music/Space Fighter Loop.mp3";
        public static final String AUDIO_IMPACT_1           = AUDIO_LOC + "/sounds/impact0.wav";
        public static final String AUDIO_IMPACT_2           = AUDIO_LOC + "/sounds/impact1.wav";
        public static final String AUDIO_IMPACT_3           = AUDIO_LOC + "/sounds/impact2.wav";
        public static final String AUDIO_IMPACT_4           = AUDIO_LOC + "/sounds/impact3.wav";
        public static final String AUDIO_IMPACT_SHIELD_1    = AUDIO_LOC + "/sounds/ShieldHit.wav";
        public static final String AUDIO_BOSS_LVL_ONE       = AUDIO_LOC + "/music/boss/bossLvlOne.wav";

        //Textures
        //Main Screen
        public static final String MAIN_TITLE_TEX           = ROOT_ASSET_LOC + "/utils/title.png";
        public static final String MAIN_PLAY_IMAGE          = ROOT_ASSET_LOC + "/buttons/PlayBtnBase.png";
        public static final String MAIN_TITLE_IMAGE         = ROOT_ASSET_LOC + "/images/bgImageMenu.jpg";
        public static final String MAIN_PLAYER_TEXT         = ROOT_ASSET_LOC + "/buttons/PlayersText.png";
        public static final String MAIN_SURVIVAL_TEXT       = ROOT_ASSET_LOC + "/buttons/SurvivalText.png";
        public static final String MAIN_HELP_TEXT           = ROOT_ASSET_LOC + "/buttons/HelpText.png";
        public static final String MAIN_DIFF_TEXT           = ROOT_ASSET_LOC + "/buttons/DifficultyText.png";
        public static final String MAIN_CUSTOM_TEXT         = ROOT_ASSET_LOC + "/buttons/CustomText.png";
        //Custom Screen
        public static final String CUSTOM_MENU_BACK         = ROOT_ASSET_LOC + "/images/menuBack.png";
        public static final String CUSTOM_MENU_BACK_2       = ROOT_ASSET_LOC + "/images/menuBack2.png";
        public static final String CUSTOM_BG_BACK           = ROOT_ASSET_LOC + "/images/bgBack.png";
        @Deprecated
        public static final String CUSTOM_OVERLAY           = ROOT_ASSET_LOC + "/images/overlay.png";
        public static final String CUSTOM_SLIDER            = ROOT_ASSET_LOC + "/sprites/slider.png";
        public static final String CUSTOM_BAR               = ROOT_ASSET_LOC + "/sprites/bar.png";
        //Game Screen
        public static final String GAME_BG_IMAGE_SCROLL     = ROOT_ASSET_LOC + "/images/bgImageScroll.png";
        public static final String GAME_BG_IMAGE            = ROOT_ASSET_LOC + "/images/bgImage2.jpg";
        public static final String GAME_DEATH_IMAGE         = ROOT_ASSET_LOC + "/images/Dead3.png";
        public static final String GAME_LAZOR_IMAGE         = ROOT_ASSET_LOC + "/pickups/lazor.png";
        public static final String GAME_SHIELD_CRAFT_IMAGE  = ROOT_ASSET_LOC + "/pickups/shield.png";
        public static final String GAME_LASER_IMAGE         = ROOT_ASSET_LOC + "/sprites/base/laserBase.png";
        public static final String GAME_PLASMA_IMAGE        = ROOT_ASSET_LOC + "/sprites/plasma.png";
        public static final String GAME_SHIP_IMAGE          = ROOT_ASSET_LOC + "/sprites/ship.png";
        public static final String GAME_FAST_SHIP_IMAGE     = ROOT_ASSET_LOC + "/sprites/fastShip.png";
        public static final String GAME_HEALTH_IMAGE        = ROOT_ASSET_LOC + "/pickups/health_01.png";
        public static final String GAME_SPEED_IMAGE         = ROOT_ASSET_LOC + "/pickups/speeds.png";
        public static final String GAME_SHIELD_IMAGE        = ROOT_ASSET_LOC + "/pickups/shields.png";
        public static final String GAME_TRI_IMAGE           = ROOT_ASSET_LOC + "/pickups/trishot.png";
        //Help Screen
        public static final String HELP_SHIELD_IMAGE_LARGE  = ROOT_ASSET_LOC + "/pickups/shieldLarge.png";
        public static final String HELP_SPEED_IMAGE_LARGE   = ROOT_ASSET_LOC + "/pickups/speedLarge.png";
        public static final String HELP_TRI_IMAGE_LARGE     = ROOT_ASSET_LOC + "/pickups/trishotLarge.png";
        public static final String HELP_LAZOR_IMAGE_LARGE   = ROOT_ASSET_LOC + "/pickups/lazorLarge.png";
        public static final String HELP_RETURN_TEXT         = ROOT_ASSET_LOC + "/buttons/ReturnText.png";
        //Misc
        public static final String MISC_GAME_OVER           = ROOT_ASSET_LOC + "/images/GameOver.png";
        public static final String MISC_CLOUD_BAR_LARGE     = ROOT_ASSET_LOC + "/utils/cloudBarLarge.png";
        public static final String MISC_EXIT_BG_IMAGE       = ROOT_ASSET_LOC + "/images/exitConfirmBG.png";
        public static final String MISC_EXIT_BG_OVERLAY     = ROOT_ASSET_LOC + "/overlays/overlayBlack.png";
        public static final String MISC_EXIT_CONFIRM_TXT    = ROOT_ASSET_LOC + "/buttons/ConfirmQuitText.png";
        public static final String MISC_EXIT_DENY_TXT       = ROOT_ASSET_LOC + "/buttons/DenyQuitText.png";
        public static final String MISC_VICTORY_BG          = ROOT_ASSET_LOC + "/images/BgVictory.png";
        public static final String MISC_VICTORY_TXT         = ROOT_ASSET_LOC + "/images/VictoryText.png";
        //Shapes
        public static final String SHAPE_LINE_PIX           = ROOT_ASSET_LOC + "/utils/shapes/Line.png";
        public static final String SHAPE_CIRCLE_PIX         = ROOT_ASSET_LOC + "/utils/shapes/Circle.png";
        public static final String SHAPE_RING_PIX           = ROOT_ASSET_LOC + "/utils/shapes/Ring.png";
        //Particles
        public static final String PART_LOG_LAZOR_EFFECT    = ROOT_ASSET_LOC + "/particles/lazor.p";
        ;
        public static final String PART_TEX_LAZOR_EFFECT    = ROOT_ASSET_LOC + "/particles";
        //Files
        public static final String FILE_COMPONENTS          = System.getProperty(FILE_LOC) + "\\components.xml";
        public static final String FILE_CONTROLS            = System.getProperty(FILE_LOC) + "\\controls.xml";
        public static final String FILE_LEADERBOARD         = System.getProperty(FILE_LOC) + "\\leaderboard.xml";
        public static final String FILE_USER_CFG            = System.getProperty(FILE_LOC) + "\\Users.txt";

        public static final String FILE_BUILD               = "https://www.dropbox.com/s/zws6aouxlolko1u/Version.txt";
        //Models
        public static final String MODEL_GALAXY             = MODEL_LOC + "/Galaxy/gal.obj";
        public static final String MODEL_PLANET             = MODEL_LOC + "/Planet/Planet.obj";

        //Bosses
        public static final String BOSS_LEVEL_ONE           = BOSS_LOC + "Boss1.png";
    }

    public static class Unlocks {
        //Unlock Progress
        public static final int totalPossiblePartsMast = 4;
        public static final int totalPossiblePartsHull = 4;
        public static final int totalPossiblePartsEngine = 4;
        public static final int totalPossiblePartsShot = 4;
        public static final int totalPossiblePartsGun = 4;
        //Player 1
        public static final int maxMastNumP1 = 4;
        public static final int maxHullNumP1 = 4;
        public static final int maxEngineNumP1 = 4;
        public static final int maxShotNumP1 = 4;
        public static final int maxGunNumP1 = 4;
        //Player 2
        public static final int maxMastNumP2 = 3;
        public static final int maxHullNumP2 = 3;
        public static final int maxEngineNumP2 = 3;
        public static final int maxShotNumP2 = 3;
        public static final int maxGunNumP2 = 3;
    }

    public static class Controls {
        //Keys
        //Player 1
        public static final int moveUpP1 = Keys.W;
        public static final int moveDownP1 = Keys.S;
        public static final int moveLeftP1 = Keys.A;
        public static final int moveRightP1 = Keys.D;
        public static final int actionFireP1 = Keys.F;
        public static final int actionLazorP1 = Keys.R;

        //Player 2
        public static final int moveUpP2 = Keys.UP;
        public static final int moveDownP2 = Keys.DOWN;
        public static final int moveLeftP2 = Keys.LEFT;
        public static final int moveRightP2 = Keys.RIGHT;
        public static final int actionFireP2 = Keys.CONTROL_RIGHT;
        public static final int actionLazorP2 = Keys.SHIFT_RIGHT;

        //Misc
        public static final int deltaCapAlter = Keys.D;
        public static final int deltaCapModKey1 = Keys.CONTROL_LEFT;
        public static final int deltaCapModKey2 = Keys.SHIFT_LEFT;
    }
}

