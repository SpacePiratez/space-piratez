package net.spacepiratez.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;

/**
 * Created by Guy on 09/03/14.
 */
public class MenuNinePatch extends NinePatch {
    private static MenuNinePatch instanceRed, instanceGreen, instanceBlue, instanceClear;
    private static MenuNinePatch indicatorRed, indicatorGreen, indicatorYellow;
    private static MenuNinePatch hudGray, instanceCyan, instanceBase, instanceGrad, instanceSolid, instanceTransGrad, instanceGradLarge;
    private static String colourPath = "menuskin";
    private static int dim = 8;

    private MenuNinePatch() {
        super(new Texture(Gdx.files.internal("data/buttons/" + colourPath + ".9.png")), dim, dim, dim, dim);
    }

    public static MenuNinePatch getInstanceTransGrad() {
        colourPath = "menuSkinTransGrad";
        dim = 1;
        if(instanceTransGrad == null) instanceTransGrad = new MenuNinePatch();
        return instanceTransGrad;
    }

    public static MenuNinePatch getInstanceGrad() {
        colourPath = "menuskinGrad";
        dim = 8;
        if(instanceGrad == null) instanceGrad = new MenuNinePatch();
        return instanceGrad;
    }

    public static MenuNinePatch getInstanceGradLarge() {
        colourPath = "menuskinGradLarge";
        dim = 8;
        if(instanceGradLarge == null) instanceGradLarge = new MenuNinePatch();
        return instanceGradLarge;
    }

    public static MenuNinePatch getInstanceSolid() {
        colourPath = "menuskinSolid";
        dim = 0;
        if(instanceSolid == null) instanceSolid = new MenuNinePatch();
        return instanceSolid;
    }

    public static MenuNinePatch getInstanceBase() {
        colourPath = "menuskinBase";
        dim = 8;
        if(instanceBase == null) instanceBase = new MenuNinePatch();
        return instanceBase;
    }

    public static MenuNinePatch getInstanceRed() {
        colourPath = "menuskinRed";
        dim = 8;
        if(instanceRed == null) instanceRed = new MenuNinePatch();
        return instanceRed;
    }

    public static MenuNinePatch getInstanceGreen() {
        colourPath = "menuSkinGreen";
        dim = 8;
        if(instanceGreen == null) instanceGreen = new MenuNinePatch();
        return instanceGreen;
    }

    public static MenuNinePatch getInstanceBlue() {
        colourPath = "menuskinBlue";
        dim = 8;
        if(instanceBlue == null) instanceBlue = new MenuNinePatch();
        return instanceBlue;
    }

    public static MenuNinePatch getInstanceClear() {
        colourPath = "menuskinClear";
        dim = 8;
        if(instanceClear == null) instanceClear = new MenuNinePatch();
        return instanceClear;
    }

    public static MenuNinePatch getIndicatorRed() {
        colourPath = "indicatorRed";
        dim = 8;
        if(indicatorRed == null) indicatorRed = new MenuNinePatch();
        return indicatorRed;
    }

    public static MenuNinePatch getIndicatorGreen() {
        colourPath = "indicatorGreen";
        dim = 8;
        if(indicatorGreen == null) indicatorGreen = new MenuNinePatch();
        return indicatorGreen;
    }

    public static MenuNinePatch getIndicatorYellow() {
        colourPath = "indicatorYellow";
        dim = 8;
        if(indicatorYellow == null) indicatorYellow = new MenuNinePatch();
        return indicatorYellow;
    }

    public static MenuNinePatch getHudBack() {
        colourPath = "hudBack";
        dim = 8;
        if(hudGray == null) hudGray = new MenuNinePatch();
        return hudGray;
    }

    public static MenuNinePatch getInstanceCyan() {
        colourPath = "menuskinCyan";
        dim = 8;
        if(instanceCyan == null) instanceCyan = new MenuNinePatch();
        return instanceCyan;
    }
}

