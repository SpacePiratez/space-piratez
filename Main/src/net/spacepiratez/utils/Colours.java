package net.spacepiratez.utils;

import java.awt.*;

/**
 * Created by Guy on 02/04/14.
 */
public class Colours {

    public static final String ANSI_CLEAR   = "\u001B[0m";
    public static final String ANSI_BLACK   = "\u001B[30m";
    public static final String ANSI_RED     = "\u001B[31m";
    public static final String ANSI_GREEN   = "\u001B[32m";
    public static final String ANSI_YELLOW  = "\u001B[33m";
    public static final String ANSI_BLUE    = "\u001B[34m";
    public static final String ANSI_PURPLE  = "\u001B[35m";
    public static final String ANSI_CYAN    = "\u001B[36m";
    public static final String ANSI_WHITE   = "\u001B[37m";

    public static final Color SILVER    = new Color(195, 195, 195);
    public static final Color RED       = new Color(195, 67,  48);
    public static final Color GREEN     = new Color(48,  195, 67);
    public static final Color BLUE      = new Color(67,  48,  195);
    public static final Color CYAN      = new Color(17, 176, 180);

}
