package net.spacepiratez.utils;

/**
 * Created by Guy on 09/03/14.
 */
public class HelpText {

    public static String[] healthText,  shieldText, speedText;
    public static String[] trishotText, lazorText;

    public void HelpArrays() {
        healthText = new String[3];
        healthText[0] = "";
        healthText[1] = "Increases player health by 5.";
        healthText[2] = "Activates instantly.";

        shieldText = new String[3];
        shieldText[0] = "";
        shieldText[1] = "Gives the player a damage shield.";
        shieldText[2] = "Lasts 10 seconds.";

        speedText = new String[3];
        speedText[0] = "Accelerates the players move speed";
        speedText[1] = "from 300 units to 500 units.";
        speedText[2] = "Lasts 10 seconds.";

        trishotText = new String[3];
        trishotText[0] = "Allows the player to fire";
        trishotText[1] = "in parallel sets of three.";
        trishotText[2] = "Lasts 10 seconds.";

        lazorText = new String[4];
        lazorText[0] = "Allows the player to use the Super mega";
        lazorText[1] = "awesome death laser of rainbows and destruction.";
        lazorText[2] = "Can be fired for 10 seconds.";
    }

}