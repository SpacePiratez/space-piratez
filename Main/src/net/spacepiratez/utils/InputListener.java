package net.spacepiratez.utils;

import com.badlogic.gdx.Input.TextInputListener;

/**
 * Created by Guy on 09/03/14.
 */
public class InputListener implements TextInputListener {

    public static int inputNum = 10;
    public int inputBuffer = 10;

    @Override
    public void input(String text) {
        try{
            inputBuffer = Integer.parseInt(text);
            if(inputBuffer < 1)   inputBuffer = 1;
            if(inputBuffer > 100) inputBuffer = 100;
            inputNum = inputBuffer;
        }catch(Exception e) {
            System.out.println("Error: " + e);
        }
    }

    @Override
    public void canceled() {

    }

    public static String getIp() {
        return "127.0.0.1";
    }
}
