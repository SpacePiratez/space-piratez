package net.spacepiratez.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import net.spacepiratez.types.SPSprite;

/**
 * Created by Guy on 09/03/14.
 */
public class Shortcuts {

    public static MenuNinePatch nineBase        = MenuNinePatch.getInstanceBase();
    public static MenuNinePatch nineGrad        = MenuNinePatch.getInstanceGrad();
    public static MenuNinePatch nineSolid       = MenuNinePatch.getInstanceSolid();
    public static MenuNinePatch nineTrans       = MenuNinePatch.getInstanceTransGrad();
    public static MenuNinePatch nineGradLarge   = MenuNinePatch.getInstanceGradLarge();

    /**This must be called within 'SpriteBatch.begin()' and 'SpriteBatch.end()'
     *
     * @param batch >> Type:SpriteBatch >> The SpriteBatch to draw this texture with
     * @param texture >> Type:Texture >> The variable name for the texture
     * @param r >> Type:float >> The redness of the desired colour
     * @param g	>> Type:float >> The greenness of the desired colour
     * @param b	>> Type:float >> The blueness of the desired colour
     * @param xPos >> Type:float >> The x-position of the texture
     * @param yPos >> Type:float >> The y-position of the texture
     */
    public static void ColourTexF(SpriteBatch batch, Texture texture, float r, float g, float b, float a, float xPos, float yPos) {
        batch.setColor(r, g, b, a);
        batch.draw(texture, xPos, yPos);
        batch.setColor(1, 1, 1, 1);
    }

    public static void ColourSpriteF(SpriteBatch batch, Sprite sprite, float r, float g, float b, float a, float xPos, float yPos, boolean alphaModulation) {
        sprite.setPosition(xPos, yPos);
        sprite.setColor(r, g, b, a);
        switch(alphaModulation+"") {
            case "true":  sprite.draw(batch, a); break;
            case "false": sprite.draw(batch);    break;
        }
        sprite.draw(batch);
    }
    public static void SpriteDraw(SpriteBatch batch, Sprite sprite, float xPos, float yPos) {
        sprite.setPosition(xPos, yPos);
        sprite.draw(batch);
    }

    public static void SpriteDraw(SpriteBatch batch, SPSprite sprite, float xPos, float yPos) {
        System.out.println("Shortcuts.SpriteDraw");
        sprite.setPosition(xPos, yPos);
        sprite.draw(batch);
    }

    /**
     *
     * @param batch
     * @param sprite
     * @param xPos
     * @param yPos
     * @param r
     * @param g
     * @param b
     * @param a
     */
    public static void SpriteColour(SpriteBatch batch, Sprite sprite, float xPos, float yPos, float r, float g, float b, float a) {
        sprite.setColor(r, g, b, a);
        sprite.setPosition(xPos, yPos);
        sprite.draw(batch);
    }

    /**
     * @param batch >> Type:SpriteBatch >> The SpriteBatch to draw this texture with
     * @param texture >> Type:Texture >> The variable name for the texture
     * @param r >> Type:byte >> The redness of the desired colour
     * @param g >> Type:byte >> The greenness of the desired colour
     * @param b >> Type:byte >> The blueness of the desired colour
     * @param xPos >> Type:float >> The x-position of the texture
     * @param yPos >> Type:float >> The y-position of the texture
     */
    public static void ColourTexRGB(SpriteBatch batch, Texture texture, byte r, byte g, byte b, byte a, float xPos, float yPos) {
        float rF = (r/255)*100;
        float gF = (g/255)*100;
        float bF = (b/255)*100;
        rF = MathUtils.ceil(rF);
        gF = MathUtils.ceil(gF);
        bF = MathUtils.ceil(bF);
        ColourTexF(batch, texture, rF/100, gF/100, bF/100, a, xPos, yPos);
    }

    /**
     * @param batch Type:SpriteBatch >> The SpriteBatch to draw this patch with
     * @param r >> Type:float >> The redness of the desired colour
     * @param g >> Type:float >> The greenness of the desired colour
     * @param b >> Type:float >> The blueness of the desired colour
     * @param xPos >> Type:float >> The x-position of the texture
     * @param yPos >> Type:float >> The y-position of the texture
     * @param width >> Type:float >> The width of the patch
     * @param height >> Type:float >> The height of the patch
     */
    public static void ColourNinePatch(SpriteBatch batch, float r, float g, float b, float a, float xPos, float yPos, float width, float height) {
        batch.setColor(r, g, b, a);
        nineBase.draw(batch, xPos, yPos, width, height);
        batch.setColor(1, 1, 1, 1);
    }

    public static void ColourNinePatchGrad(SpriteBatch batch, float r, float g, float b, float a, float xPos, float yPos, float width, float height) {
        batch.setColor(r, g, b , a);
        nineGrad.draw(batch, xPos, yPos, width, height);
        batch.setColor(1, 1, 1, 1);
    }

    public static void ColourNinePatchGradLarge(SpriteBatch batch, float r, float  g, float b, float a, float xPos, float yPos, float width, float height) {
        batch.setColor(r, g, b, a);
        nineGradLarge.draw(batch, xPos, yPos, width, height);
        batch.setColor(1, 1, 1, 1);
    }

    public static void ColourNinePatchSolid(SpriteBatch batch, float r, float g, float b, float a, float xPos, float yPos, float width, float height) {
        batch.setColor(r, g, b , a);
        nineSolid.draw(batch, xPos, yPos, width, height);
        batch.setColor(1, 1, 1, 1);
    }

    public static void ColourNinePatchTrans(SpriteBatch batch, float r, float g, float b, float a, float xPos, float yPos, float width, float height) {
        batch.setColor(r, g, b, a);
        nineTrans.draw(batch, xPos, yPos, width, height);
        batch.setColor(1, 1, 1, 1);
    }


    /**
     * @param batch Type:SpriteBatch >> The SpriteBatch to draw this patch with
     * @param r >> Type:byte >> The redness of the desired colour
     * @param g >> Type:byte >> The greenness of the desired colour
     * @param b >> Type:byte >> The blueness of the desired colour
     * @param xPos >> Type:float >> The x-position of the texture
     * @param yPos >> Type:float >> The y-position of the texture
     * @param width >> Type:float >> The width of the patch
     * @param height >> Type:float >> The height of the patch
     */
    public static void ColourNinePatchRGB(SpriteBatch batch, byte r, byte g, byte b, float xPos, float yPos, float width, float height) {
        float rF = (r/255)*100;
        float gF = (g/255)*100;
        float bF = (b/255)*100;
        rF = MathUtils.ceil(rF);
        gF = MathUtils.ceil(gF);
        bF = MathUtils.ceil(bF);
        ColourNinePatch(batch, rF/100, gF/100, bF/100, 1, xPos, yPos, width, height);
    }

    /** Draws the box
     *
     * @param batch >> Type:SpriteBatch >> The spriteBatch to draw this texture with
     * @param patch >> Type:MenuNinePatch >> patch instance
     * @param boxWidth >> Type:float >> width of the box
     * @param boxHeight >> Type:float >> height of the box
     * @param regionXBot >> Type:float >> left side of the region
     * @param regionXTop >> Type:float >> right side of the region
     * @param regionYBot >> Type:float >> bottom side of the region
     * @param regionYTop >> Type:float >> top side of the region
     */
    public static void Tooltip(SpriteBatch batch, MenuNinePatch patch, float boxWidth, float boxHeight,
                               float regionXBot, float regionXTop, float regionYBot, float regionYTop) {
        if(Gdx.input.getX() >= regionXBot && Gdx.input.getX() <= regionXTop) {
            if((Gdx.graphics.getHeight()-Gdx.input.getY()) > regionYBot && (Gdx.graphics.getHeight()-Gdx.input.getY() < regionYTop)) {
                patch.draw(batch, Gdx.input.getX(), (Gdx.graphics.getHeight()-Gdx.input.getY()), boxWidth, boxHeight);
            }
        }
    }

    /** Multi Line
     *
     * @param batch >> Type:SpriteBatch >> game.batch
     * @param font >> Type:BitmapFont >> game.font
     * @param colour >> Type:Color >> the colour of the text
     * @param ySize >> Type:float >> Height of the tooltip box
     * @param regionXBot >> Type:float >> Left side of the region
     * @param regionXTop >> Type:float >> right side of the region
     * @param regionYBot >> Type:float >> bottom side of the region
     * @param regionYTop >> Type:float >> top side of the region
     * @param text >> Type:String Array >> Array of the text to display
     */
    public static void TooltipText(SpriteBatch batch, BitmapFont font, Color colour, float ySize,
                                   float regionXBot, float regionXTop, float regionYBot, float regionYTop, String[] text) {
        if(Gdx.input.getX() >= regionXBot && Gdx.input.getX() <= regionXTop) {
            if((Gdx.graphics.getHeight()-Gdx.input.getY()) > regionYBot && (Gdx.graphics.getHeight()-Gdx.input.getY() < regionYTop)) {
                font.setColor(colour);
                for(int i = 0; i < text.length; i++) {
                    font.draw(batch, text[i], Gdx.input.getX()+10, (((Gdx.graphics.getHeight()-Gdx.input.getY())+ySize)-7)-(20*i));
                }
                font.setColor(Color.WHITE);
            }
        }
    }

    /** Single Line
     *
     * @param batch >> Type:SpriteBatch >> game.batch
     * @param font >> Type:BitmapFont >> game.font
     * @param colour >> Type:Color >> the colour of the text
     * @param ySize >> Type:float >> Height of the tooltip box
     * @param regionXBot >> Type:float >> Left side of the region
     * @param regionXTop >> Type:float >> right side of the region
     * @param regionYBot >> Type:float >> bottom side of the region
     * @param regionYTop >> Type:float >> top side of the region
     * @param text >> Type:String >> text to display
     */
    public static void TooltipText(SpriteBatch batch, BitmapFont font, Color colour, float ySize,
                                   float regionXBot, float regionXTop, float regionYBot, float regionYTop, String text) {
        if(Gdx.input.getX() >= regionXBot && Gdx.input.getX() <= regionXTop) {
            if((Gdx.graphics.getHeight()-Gdx.input.getY()) > regionYBot && (Gdx.graphics.getHeight()-Gdx.input.getY() < regionYTop)) {
                font.setColor(colour);
                font.draw(batch, text, Gdx.input.getX()+10, (((Gdx.graphics.getHeight()-Gdx.input.getY())+ySize)-7));
                font.setColor(Color.WHITE);
            }
        }
    }

    public static void Button(SpriteBatch batch, float r1, float g1, float b1, float a1, float r2, float g2, float b2, float a2, float xPos, float yPos, float width, float height) {
//		if(r1 > 255) r1 = 255; if(r1 < 0) r1 = 0; if(r2 > 255) r2 = 255; if(r2 < 0) r2 = 0;
//		if(g1 > 255) g1 = 255; if(g1 < 0) r1 = 0; if(g2 > 255) g2 = 255; if(g2 < 0) g2 = 0;
//		if(b1 > 255) b1 = 255; if(b1 < 0) r1 = 0; if(b2 > 255) b2 = 255; if(b2 < 0) b2 = 0;
        if(Gdx.input.getX() >= xPos && Gdx.input.getX() <= xPos+width) {
            if(Gdx.graphics.getHeight()-Gdx.input.getY() >= yPos && Gdx.graphics.getHeight()-Gdx.input.getY() <= yPos+height) {
                ColourNinePatch(batch, r2, g2, b2, a2, xPos, yPos, width, height);
            }else ColourNinePatch(batch, r1, g1, b1, a1, xPos, yPos, width, height);
        }else ColourNinePatch(batch, r1, g1, b1, a1, xPos, yPos, width, height);
    }

    public static void setImage(Body body, Sprite sprite, SpriteBatch batch) {
        sprite.setPosition(body.getPosition().x-(sprite.getWidth()/2), body.getPosition().y-(sprite.getHeight()/2));
        sprite.draw(batch);
    }

    public static void setCraftImage(Body body, Sprite[] craft, SpriteBatch batch, boolean rotate) {
        for(int i = 0; i < craft.length; i++) {
            if(i != 3) {
                if(rotate) craft[i].setRotation(body.getAngle());
                craft[i].setPosition(body.getPosition().x, body.getPosition().y);
                craft[i].draw(batch);
            }
        }
    }
}
