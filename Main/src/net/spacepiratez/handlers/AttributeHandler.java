package net.spacepiratez.handlers;

/**
 * Created by Guy on 20/03/14.
 */
public class AttributeHandler {

    public static float craftPartMods[][][];

    public static void AttributeSetters() {
        craftPartMods = new float[5][4][6];
        //[0][y][z] -> [4][y][z] Components
        //[x][0][z] -> [x][3][z] Component Parts
        //[x][y][0] -> [x][y][5] Modifiers

        // [0] Speed
        // [1] Health/Armour
        // [2] Evasion
        // [3] Fire Rate
        // [4] Pickup Duration
        // [5] Attack Damage

        //Mast 1
        craftPartMods[0][0][0] = 1;     // Speed
        craftPartMods[0][0][1] = 1;     // Health
        craftPartMods[0][0][2] = 1;     // Evasion
        craftPartMods[0][0][3] = 1;     // Fire Rate
        craftPartMods[0][0][4] = 1;     // Pickup Duration
        craftPartMods[0][0][5] = 1;     // Attack Damage
        //Mast 2
        craftPartMods[0][1][0] = 1.05f;  // Speed
        craftPartMods[0][1][1] = 50.95f;  // Health
        craftPartMods[0][1][2] = 1.05f;  // Evasion
        craftPartMods[0][1][3] = 1;     // Fire Rate
        craftPartMods[0][1][4] = 1.02f;  // Pickup Duration
        craftPartMods[0][1][5] = 1;     // Attack Damage
        //Mast 3
        craftPartMods[0][2][0] = 0.8f;  // Speed
        craftPartMods[0][2][1] = 1.05f;  // Health
        craftPartMods[0][2][2] = 0.7f;  // Evasion
        craftPartMods[0][2][3] = 1;     // Fire Rate
        craftPartMods[0][2][4] = 1.02f;  // Pickup Duration
        craftPartMods[0][2][5] = 1;     // Attack Damage
        //Mast 4
        craftPartMods[0][3][0] = 2;     // Speed
        craftPartMods[0][3][1] = 0.5f;  // Health
        craftPartMods[0][3][2] = 2;     // Evasion
        craftPartMods[0][3][3] = 1;     // Fire Rate
        craftPartMods[0][3][4] = 3;     // Pickup Duration
        craftPartMods[0][3][5] = 1;     // Attack Damage

        //Hull 1
        craftPartMods[1][0][0] = 1;     // Speed
        craftPartMods[1][0][1] = 1;     // Health
        craftPartMods[1][0][2] = 1;     // Evasion
        craftPartMods[1][0][3] = 1;     // Fire Rate
        craftPartMods[1][0][4] = 1;     // Pickup Duration
        craftPartMods[1][0][5] = 1;     // Attack Damage
        //Hull 2
        craftPartMods[1][1][0] = 0.9f;  // Speed
        craftPartMods[1][1][1] = 1.05f;  // Health
        craftPartMods[1][1][2] = 0.9f;  // Evasion
        craftPartMods[1][1][3] = 1;     // Fire Rate
        craftPartMods[1][1][4] = 1.02f;  // Pickup Duration
        craftPartMods[1][1][5] = 1;     // Attack Damage
        //Hull 3
        craftPartMods[1][2][0] = 0.8f;  // Speed
        craftPartMods[1][2][1] = 1.03f;  // Health
        craftPartMods[1][2][2] = 0.4f;  // Evasion
        craftPartMods[1][2][3] = 1;     // Fire Rate
        craftPartMods[1][2][4] = 0.9f;  // Pickup Duration
        craftPartMods[1][2][5] = 1;     // Attack Damage
        //Hull 4
        craftPartMods[1][3][0] = 1.25f; // Speed
        craftPartMods[1][3][1] = 0.5f;  // Health
        craftPartMods[1][3][2] = 1.25f; // Evasion
        craftPartMods[1][3][3] = 1;     // Fire Rate
        craftPartMods[1][3][4] = 3;     // Pickup Duration
        craftPartMods[1][3][5] = 1;     // Attack Damage

        //Engine 1
        craftPartMods[2][0][0] = 1;     // Speed
        craftPartMods[2][0][1] = 1;     // Health
        craftPartMods[2][0][2] = 1;     // Evasion
        craftPartMods[2][0][3] = 1;     // Fire Rate
        craftPartMods[2][0][4] = 1;     // Pickup Duration
        craftPartMods[2][0][5] = 1;     // Attack Damage
        //Engine 2
        craftPartMods[2][1][0] = 1.05f; // Speed
        craftPartMods[2][1][1] = 0.9f;  // Health
        craftPartMods[2][1][2] = 1.02f; // Evasion
        craftPartMods[2][1][3] = 1;     // Fire Rate
        craftPartMods[2][1][4] = 1.02f; // Pickup Duration
        craftPartMods[2][1][5] = 0.8f;  // Attack Damage
        //Engine 3
        craftPartMods[2][2][0] = 0.9f; // Speed
        craftPartMods[2][2][1] = 1.2f;  // Health
        craftPartMods[2][2][2] = 0.85f; // Evasion
        craftPartMods[2][2][3] = 0.8f;  // Fire Rate
        craftPartMods[2][2][4] = 1;     // Pickup Duration
        craftPartMods[2][2][5] = 1;     // Attack Damage
        //Engine 4
        craftPartMods[2][3][0] = 1.25f; // Speed
        craftPartMods[2][3][1] = 0.8f;  // Health
        craftPartMods[2][3][2] = 1.5f;  // Evasion
        craftPartMods[2][3][3] = 1;     // Fire Rate
        craftPartMods[2][3][4] = 1.3f;  // Pickup Duration
        craftPartMods[2][3][5] = 1.1f;  // Attack Damage

        //shots 1 -> 4
        for(int y = 0; y < 4; y++) {
            for(int z = 0; z < 6; z++) {
                craftPartMods[3][y][z] = 1;
            }
        }

        //Gun 1
        craftPartMods[4][0][0] = 1;     // Speed
        craftPartMods[4][0][1] = 1;     // Health
        craftPartMods[4][0][2] = 1;     // Evasion
        craftPartMods[4][0][3] = 1;     // Fire Rate
        craftPartMods[4][0][4] = 1;     // Pickup Duration
        craftPartMods[4][0][5] = 1;     // Attack Damage
        //Gun 2
        craftPartMods[4][1][0] = 1;     // Speed
        craftPartMods[4][1][1] = 1;     // Health
        craftPartMods[4][1][2] = 1;     // Evasion
        craftPartMods[4][1][3] = 0.8f;  // Fire Rate
        craftPartMods[4][1][4] = 1.1f;  // Pickup Duration
        craftPartMods[4][1][5] = 2;     // Attack Damage
        //Gun 3
        craftPartMods[4][2][0] = 1;     // Speed
        craftPartMods[4][2][1] = 1.1f;  // Health
        craftPartMods[4][2][2] = 1;     // Evasion
        craftPartMods[4][2][3] = 1.5f;  // Fire Rate
        craftPartMods[4][2][4] = 1;     // Pickup Duration
        craftPartMods[4][2][5] = 0.8f;  // Attack Damage
        //Gun 4
        craftPartMods[4][3][0] = 1;     // Speed
        craftPartMods[4][3][1] = 1.25f; // Health
        craftPartMods[4][3][2] = 1;     // Evasion
        craftPartMods[4][3][3] = 2;     // Fire Rate
        craftPartMods[4][3][4] = 1.3f;  // Pickup Duration
        craftPartMods[4][3][5] = 0.1f;  // Attack Damage

    }

}
