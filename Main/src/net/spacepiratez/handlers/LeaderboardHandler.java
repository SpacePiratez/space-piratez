package net.spacepiratez.handlers;

import net.spacepiratez.utils.XMLHandler;

/**
 * Created by Guy on 17/03/14.
 */
public class LeaderboardHandler {

    public static String tempTimeFull;
    public static String tempName, tempScore, tempTime;
    static int tempScoreInt, tempTimeInt;
    static float PosCalc;

    public static void orderBoard() {

        tempScoreInt = Integer.parseInt(tempScore);
        tempTimeInt  = Integer.parseInt(tempTime);

        PosCalc = (tempScoreInt + tempTimeInt)/100;

        if(PosCalc > XMLHandler.Posit[9]) {
            XMLHandler.Names[9] = tempName;
            XMLHandler.Score[9] = tempScore;
            XMLHandler.Times[9] = tempTimeFull;
            XMLHandler.Posit[9] = PosCalc;
            if(PosCalc > XMLHandler.Posit[8]) {
                XMLHandler.Names[9] = XMLHandler.Names[8];
                XMLHandler.Score[9] = XMLHandler.Score[8];
                XMLHandler.Times[9] = XMLHandler.Times[8];
                XMLHandler.Posit[9] = XMLHandler.Posit[8];
                XMLHandler.Names[8] = tempName;
                XMLHandler.Score[8] = tempScore;
                XMLHandler.Times[8] = tempTimeFull;
                XMLHandler.Posit[8] = PosCalc;
                if(PosCalc > XMLHandler.Posit[7]) {
                    XMLHandler.Names[8] = XMLHandler.Names[7];
                    XMLHandler.Score[8] = XMLHandler.Score[7];
                    XMLHandler.Times[8] = XMLHandler.Times[7];
                    XMLHandler.Posit[8] = XMLHandler.Posit[7];
                    XMLHandler.Names[7] = tempName;
                    XMLHandler.Score[7] = tempScore;
                    XMLHandler.Times[7] = tempTimeFull;
                    XMLHandler.Posit[7] = PosCalc;
                    if(PosCalc > XMLHandler.Posit[6]) {
                        XMLHandler.Names[7] = XMLHandler.Names[6];
                        XMLHandler.Score[7] = XMLHandler.Score[6];
                        XMLHandler.Times[7] = XMLHandler.Times[6];
                        XMLHandler.Posit[7] = XMLHandler.Posit[6];
                        XMLHandler.Names[6] = tempName;
                        XMLHandler.Score[6] = tempScore;
                        XMLHandler.Times[6] = tempTimeFull;
                        XMLHandler.Posit[6] = PosCalc;
                        if(PosCalc > XMLHandler.Posit[5]) {
                            XMLHandler.Names[6] = XMLHandler.Names[5];
                            XMLHandler.Score[6] = XMLHandler.Score[5];
                            XMLHandler.Times[6] = XMLHandler.Times[5];
                            XMLHandler.Posit[6] = XMLHandler.Posit[5];
                            XMLHandler.Names[5] = tempName;
                            XMLHandler.Score[5] = tempScore;
                            XMLHandler.Times[5] = tempTimeFull;
                            XMLHandler.Posit[5] = PosCalc;
                            if(PosCalc > XMLHandler.Posit[4]) {
                                XMLHandler.Names[5] = XMLHandler.Names[4];
                                XMLHandler.Score[5] = XMLHandler.Score[4];
                                XMLHandler.Times[5] = XMLHandler.Times[4];
                                XMLHandler.Posit[5] = XMLHandler.Posit[4];
                                XMLHandler.Names[4] = tempName;
                                XMLHandler.Score[4] = tempScore;
                                XMLHandler.Times[4] = tempTimeFull;
                                XMLHandler.Posit[4] = PosCalc;
                                if(PosCalc > XMLHandler.Posit[3]) {
                                    XMLHandler.Names[4] = XMLHandler.Names[3];
                                    XMLHandler.Score[4] = XMLHandler.Score[3];
                                    XMLHandler.Times[4] = XMLHandler.Times[3];
                                    XMLHandler.Posit[4] = XMLHandler.Posit[3];
                                    XMLHandler.Names[3] = tempName;
                                    XMLHandler.Score[3] = tempScore;
                                    XMLHandler.Times[3] = tempTimeFull;
                                    XMLHandler.Posit[3] = PosCalc;
                                    if(PosCalc > XMLHandler.Posit[2]) {
                                        XMLHandler.Names[3] = XMLHandler.Names[2];
                                        XMLHandler.Score[3] = XMLHandler.Score[2];
                                        XMLHandler.Times[3] = XMLHandler.Times[2];
                                        XMLHandler.Posit[3] = XMLHandler.Posit[2];
                                        XMLHandler.Names[2] = tempName;
                                        XMLHandler.Score[2] = tempScore;
                                        XMLHandler.Times[2] = tempTimeFull;
                                        XMLHandler.Posit[2] = PosCalc;
                                        if(PosCalc > XMLHandler.Posit[1]) {
                                            XMLHandler.Names[2] = XMLHandler.Names[1];
                                            XMLHandler.Score[2] = XMLHandler.Score[1];
                                            XMLHandler.Times[2] = XMLHandler.Times[1];
                                            XMLHandler.Posit[2] = XMLHandler.Posit[1];
                                            XMLHandler.Names[1] = tempName;
                                            XMLHandler.Score[1] = tempScore;
                                            XMLHandler.Times[1] = tempTimeFull;
                                            XMLHandler.Posit[1] = PosCalc;
                                            if(PosCalc > XMLHandler.Posit[0]) {
                                                XMLHandler.Names[1] = XMLHandler.Names[0];
                                                XMLHandler.Score[1] = XMLHandler.Score[0];
                                                XMLHandler.Times[1] = XMLHandler.Times[0];
                                                XMLHandler.Posit[1] = XMLHandler.Posit[0];
                                                XMLHandler.Names[0] = tempName;
                                                XMLHandler.Score[0] = tempScore;
                                                XMLHandler.Times[0] = tempTimeFull;
                                                XMLHandler.Posit[0] = PosCalc;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }

}
