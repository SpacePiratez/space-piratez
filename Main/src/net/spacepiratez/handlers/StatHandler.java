package net.spacepiratez.handlers;

/**
 * Created by Guy on 22/03/14.
 */
public class StatHandler {

    public static float craftSpeed1, craftSpeed2;
    public static float craftHealth1, craftHealth2;
    public static float craftEvasion1, craftEvasion2;
    public static float craftFireRate1, craftFireRate2;
    public static float craftPickupDuration1, craftPickupDuration2;
    public static float craftAttackDamage1, craftAttackDamage2;

    //Default values

    public static void DefaultStats(boolean player1) {
        switch(player1+""){
            case "true":
                craftSpeed1             = 200;
                craftHealth1            = 10;
                craftEvasion1           = 1;
                craftFireRate1          = 1;
                craftPickupDuration1    = 1;
                craftAttackDamage1      = 1;
                break;
            case "false":
                craftSpeed2             = 200;
                craftHealth2            = 10;
                craftEvasion2           = 1;
                craftFireRate2          = 1;
                craftPickupDuration2    = 1;
                craftAttackDamage2      = 1;
                break;
        }
    }

    public static void StatModifier(boolean player1, int mastNumber, int hullNumber, int engineNumber, int shotNumber, int gunNumber) {
        switch(player1+"") {
            case "true":
                DefaultStats(player1);
                craftSpeed1             = craftSpeed1           * AttributeHandler.craftPartMods[0][mastNumber][0];
                craftHealth1            = craftHealth1          * AttributeHandler.craftPartMods[0][mastNumber][1];
                craftEvasion1           = craftEvasion1         * AttributeHandler.craftPartMods[0][mastNumber][2];
                craftFireRate1          = craftFireRate1        * AttributeHandler.craftPartMods[0][mastNumber][3];
                craftPickupDuration1    = craftPickupDuration1  * AttributeHandler.craftPartMods[0][mastNumber][4];
                craftAttackDamage1      = craftAttackDamage1    * AttributeHandler.craftPartMods[0][mastNumber][5];

                craftSpeed1             = craftSpeed1           * AttributeHandler.craftPartMods[1][hullNumber][0];
                craftHealth1            = craftHealth1          * AttributeHandler.craftPartMods[1][hullNumber][1];
                craftEvasion1           = craftEvasion1         * AttributeHandler.craftPartMods[1][hullNumber][2];
                craftFireRate1          = craftFireRate1        * AttributeHandler.craftPartMods[1][hullNumber][3];
                craftPickupDuration1    = craftPickupDuration1  * AttributeHandler.craftPartMods[1][hullNumber][4];
                craftAttackDamage1      = craftAttackDamage1    * AttributeHandler.craftPartMods[1][hullNumber][5];

                craftSpeed1             = craftSpeed1           * AttributeHandler.craftPartMods[2][engineNumber][0];
                craftHealth1            = craftHealth1          * AttributeHandler.craftPartMods[2][engineNumber][1];
                craftEvasion1           = craftEvasion1         * AttributeHandler.craftPartMods[2][engineNumber][2];
                craftFireRate1          = craftFireRate1        * AttributeHandler.craftPartMods[2][engineNumber][3];
                craftPickupDuration1    = craftPickupDuration1  * AttributeHandler.craftPartMods[2][engineNumber][4];
                craftAttackDamage1      = craftAttackDamage1    * AttributeHandler.craftPartMods[2][engineNumber][5];

                craftSpeed1             = craftSpeed1           * AttributeHandler.craftPartMods[3][shotNumber][0];
                craftHealth1            = craftHealth1          * AttributeHandler.craftPartMods[3][shotNumber][1];
                craftEvasion1           = craftEvasion1         * AttributeHandler.craftPartMods[3][shotNumber][2];
                craftFireRate1          = craftFireRate1        * AttributeHandler.craftPartMods[3][shotNumber][3];
                craftPickupDuration1    = craftPickupDuration1  * AttributeHandler.craftPartMods[3][shotNumber][4];
                craftAttackDamage1      = craftAttackDamage1    * AttributeHandler.craftPartMods[3][shotNumber][5];

                craftSpeed1             = craftSpeed1           * AttributeHandler.craftPartMods[4][gunNumber][0];
                craftHealth1            = craftHealth1          * AttributeHandler.craftPartMods[4][gunNumber][1];
                craftEvasion1           = craftEvasion1         * AttributeHandler.craftPartMods[4][gunNumber][2];
                craftFireRate1          = craftFireRate1        * AttributeHandler.craftPartMods[4][gunNumber][3];
                craftPickupDuration1    = craftPickupDuration1  * AttributeHandler.craftPartMods[4][gunNumber][4];
                craftAttackDamage1      = craftAttackDamage1    * AttributeHandler.craftPartMods[4][gunNumber][5];
                break;
            case "false":
                DefaultStats(player1);
                craftSpeed2             = craftSpeed2           * AttributeHandler.craftPartMods[0][mastNumber][0];
                craftHealth2            = craftHealth2          * AttributeHandler.craftPartMods[0][mastNumber][1];
                craftEvasion2           = craftEvasion2         * AttributeHandler.craftPartMods[0][mastNumber][2];
                craftFireRate2          = craftFireRate2        * AttributeHandler.craftPartMods[0][mastNumber][3];
                craftPickupDuration2    = craftPickupDuration2  * AttributeHandler.craftPartMods[0][mastNumber][4];
                craftAttackDamage2      = craftAttackDamage2    * AttributeHandler.craftPartMods[0][mastNumber][5];

                craftSpeed2             = craftSpeed2           * AttributeHandler.craftPartMods[1][hullNumber][0];
                craftHealth2            = craftHealth2          * AttributeHandler.craftPartMods[1][hullNumber][1];
                craftEvasion2           = craftEvasion2         * AttributeHandler.craftPartMods[1][hullNumber][2];
                craftFireRate2          = craftFireRate2        * AttributeHandler.craftPartMods[1][hullNumber][3];
                craftPickupDuration2    = craftPickupDuration2  * AttributeHandler.craftPartMods[1][hullNumber][4];
                craftAttackDamage2      = craftAttackDamage2    * AttributeHandler.craftPartMods[1][hullNumber][5];

                craftSpeed2             = craftSpeed2           * AttributeHandler.craftPartMods[2][engineNumber][0];
                craftHealth2            = craftHealth2          * AttributeHandler.craftPartMods[2][engineNumber][1];
                craftEvasion2           = craftEvasion2         * AttributeHandler.craftPartMods[2][engineNumber][2];
                craftFireRate2          = craftFireRate2        * AttributeHandler.craftPartMods[2][engineNumber][3];
                craftPickupDuration2    = craftPickupDuration2  * AttributeHandler.craftPartMods[2][engineNumber][4];
                craftAttackDamage2      = craftAttackDamage2    * AttributeHandler.craftPartMods[2][engineNumber][5];

                craftSpeed2             = craftSpeed2           * AttributeHandler.craftPartMods[3][shotNumber][0];
                craftHealth2            = craftHealth2          * AttributeHandler.craftPartMods[3][shotNumber][1];
                craftEvasion2           = craftEvasion2         * AttributeHandler.craftPartMods[3][shotNumber][2];
                craftFireRate2          = craftFireRate2        * AttributeHandler.craftPartMods[3][shotNumber][3];
                craftPickupDuration2    = craftPickupDuration2  * AttributeHandler.craftPartMods[3][shotNumber][4];
                craftAttackDamage2      = craftAttackDamage2    * AttributeHandler.craftPartMods[3][shotNumber][5];

                craftSpeed2             = craftSpeed2           * AttributeHandler.craftPartMods[4][gunNumber][0];
                craftHealth2            = craftHealth2          * AttributeHandler.craftPartMods[4][gunNumber][1];
                craftEvasion2           = craftEvasion2         * AttributeHandler.craftPartMods[4][gunNumber][2];
                craftFireRate2          = craftFireRate2        * AttributeHandler.craftPartMods[4][gunNumber][3];
                craftPickupDuration2    = craftPickupDuration2  * AttributeHandler.craftPartMods[4][gunNumber][4];
                craftAttackDamage2      = craftAttackDamage2    * AttributeHandler.craftPartMods[4][gunNumber][5];
                break;
        }
    }
}
