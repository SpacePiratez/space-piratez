package net.spacepiratez.desktop;

/**
 * Created by Guy on 09/03/14.
 */
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.MathUtils;
import net.spacepiratez.utils.Reference;
import net.spacepiratez.utils.XMLHandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DesktopStarter {

    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();

        String AA=null;

        String[] titles;
        titles = new String[9];
        titles[0] = "An old hope";			titles[1] = "Fuck Gravity";		titles[2] = "One ship to rule them all";
        titles[3] = "The first frontier";	titles[4] = "With a Z";			titles[5] = "SPAAACCCCCE!";
        titles[6] = "An end to it all";		titles[7] = "Its just a game";	titles[8] = "Ooh, that thing has numbers on it!";

        int titleExtend = MathUtils.random(0, titles.length - 1);
        System.out.println("Title selected: " + titleExtend + "\t" + titles[titleExtend]);

        cfg.title = "Space Piratez: " + titles[titleExtend];

        BufferedReader br = null;
        System.out.println("Buffered reader initialized");
        try{
            br = new BufferedReader(new FileReader(Reference.Files.FILE_USER_CFG));
            System.out.println("File Reader Initialized");
//            while(AA != "false" && AA != "true") {
//                if(br != null) AA = br.readLine();
//                System.out.println("AA Set");
//            }
            XMLHandler.User = br.readLine();
            AA = br.readLine();
        }catch(IOException ioe) {}
        try{
            if(AA != null) {
                System.out.println("Setting AA samples");
                switch(AA) {
                    case "true":  cfg.samples = 8; break;
                    case "false": cfg.samples = 0; break;
                    default:      cfg.samples = 0; break;
                }
            }else {
                System.out.println("Default samples set");
                cfg.samples = 0;
            }
        }catch(Exception q) {
            cfg.samples = 8;
        }

        cfg.useGL20 = true;
        cfg.width = 1024;
        cfg.height = 512;
        cfg.resizable = false;
//		cfg.addIcon("data\\sprites\\ship.png", Files.FileType.Internal);

        new LwjglApplication(new net.spacepiratez.common.SpacePiratez(), cfg);
    }
}
